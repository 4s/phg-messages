/// <reference path="types/BluetoothDeviceControl_pb.d.ts" />
/// <reference path="types/Device_pb.d.ts" />
/// <reference path="types/DeviceControl_pb.d.ts" />
/// <reference path="types/DeviceDomain_pb.d.ts" />
/// <reference path="types/DeviceManagement_pb.d.ts" />
/// <reference path="types/Error_pb.d.ts" />
/// <reference path="types/FHIRMeasurement_pb.d.ts" />
/// <reference path="types/GATT_pb.d.ts" />
/// <reference path="types/Log_pb.d.ts" />
/// <reference path="types/Monica_pb.d.ts" />
/// <reference path="types/SerialPort_pb.d.ts" />
/// <reference path="types/Time_pb.d.ts" />
/// <reference path="types/ConfigSettings_pb.d.ts" />
/// <reference path="types/PersonalHealthDevice_pb.d.ts" />

/*
When adding new interfaces here, then remember to also add them to the
index.js file in the root.
That file is used to generate the web-packed messages.js
*/