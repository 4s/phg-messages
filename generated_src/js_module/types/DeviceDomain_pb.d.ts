// package: s4.messages.interfaces.device_domain
// file: DeviceDomain.proto

import * as jspb from "google-protobuf";
import * as Device_pb from "./Device_pb";

export class C2P extends jspb.Message {
  hasRequest(): boolean;
  clearRequest(): void;
  getRequest(): C2P.Request | undefined;
  setRequest(value?: C2P.Request): void;

  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): C2P.Event | undefined;
  setEvent(value?: C2P.Event): void;

  getPayloadCase(): C2P.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): C2P.AsObject;
  static toObject(includeInstance: boolean, msg: C2P): C2P.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: C2P, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): C2P;
  static deserializeBinaryFromReader(message: C2P, reader: jspb.BinaryReader): C2P;
}

export namespace C2P {
  export type AsObject = {
    request?: C2P.Request.AsObject,
    event?: C2P.Event.AsObject,
  }

  export class Request extends jspb.Message {
    hasConnectDevice(): boolean;
    clearConnectDevice(): void;
    getConnectDevice(): ConnectDevice | undefined;
    setConnectDevice(value?: ConnectDevice): void;

    hasDisconnectDevice(): boolean;
    clearDisconnectDevice(): void;
    getDisconnectDevice(): DisconnectDevice | undefined;
    setDisconnectDevice(value?: DisconnectDevice): void;

    getRequestCase(): Request.RequestCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Request.AsObject;
    static toObject(includeInstance: boolean, msg: Request): Request.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Request, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Request;
    static deserializeBinaryFromReader(message: Request, reader: jspb.BinaryReader): Request;
  }

  export namespace Request {
    export type AsObject = {
      connectDevice?: ConnectDevice.AsObject,
      disconnectDevice?: DisconnectDevice.AsObject,
    }

    export enum RequestCase {
      REQUEST_NOT_SET = 0,
      CONNECT_DEVICE = 1,
      DISCONNECT_DEVICE = 2,
    }
  }

  export class Event extends jspb.Message {
    hasSolicitDeviceDomain(): boolean;
    clearSolicitDeviceDomain(): void;
    getSolicitDeviceDomain(): SolicitDeviceDomain | undefined;
    setSolicitDeviceDomain(value?: SolicitDeviceDomain): void;

    hasSearch(): boolean;
    clearSearch(): void;
    getSearch(): Search | undefined;
    setSearch(value?: Search): void;

    hasStopSearch(): boolean;
    clearStopSearch(): void;
    getStopSearch(): StopSearch | undefined;
    setStopSearch(value?: StopSearch): void;

    hasDevice(): boolean;
    clearDevice(): void;
    getDevice(): Device_pb.Device | undefined;
    setDevice(value?: Device_pb.Device): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      solicitDeviceDomain?: SolicitDeviceDomain.AsObject,
      search?: Search.AsObject,
      stopSearch?: StopSearch.AsObject,
      device?: Device_pb.Device.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      SOLICIT_DEVICE_DOMAIN = 1,
      SEARCH = 2,
      STOP_SEARCH = 3,
      DEVICE = 4,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    REQUEST = 1,
    EVENT = 2,
  }
}

export class P2C extends jspb.Message {
  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): P2C.Event | undefined;
  setEvent(value?: P2C.Event): void;

  getPayloadCase(): P2C.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): P2C.AsObject;
  static toObject(includeInstance: boolean, msg: P2C): P2C.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: P2C, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): P2C;
  static deserializeBinaryFromReader(message: P2C, reader: jspb.BinaryReader): P2C;
}

export namespace P2C {
  export type AsObject = {
    event?: P2C.Event.AsObject,
  }

  export class Event extends jspb.Message {
    hasStoreDomainData(): boolean;
    clearStoreDomainData(): void;
    getStoreDomainData(): StoreDomainData | undefined;
    setStoreDomainData(value?: StoreDomainData): void;

    hasSearchResult(): boolean;
    clearSearchResult(): void;
    getSearchResult(): SearchResult | undefined;
    setSearchResult(value?: SearchResult): void;

    hasSolicitDevice(): boolean;
    clearSolicitDevice(): void;
    getSolicitDevice(): SolicitDevice | undefined;
    setSolicitDevice(value?: SolicitDevice): void;

    hasDeviceDomain(): boolean;
    clearDeviceDomain(): void;
    getDeviceDomain(): DeviceDomain | undefined;
    setDeviceDomain(value?: DeviceDomain): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      storeDomainData?: StoreDomainData.AsObject,
      searchResult?: SearchResult.AsObject,
      solicitDevice?: SolicitDevice.AsObject,
      deviceDomain?: DeviceDomain.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      STORE_DOMAIN_DATA = 1,
      SEARCH_RESULT = 2,
      SOLICIT_DEVICE = 3,
      DEVICE_DOMAIN = 4,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    EVENT = 2,
  }
}

export class ConnectDevice extends jspb.Message {
  getDeviceDomainHandle(): number;
  setDeviceDomainHandle(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ConnectDevice.AsObject;
  static toObject(includeInstance: boolean, msg: ConnectDevice): ConnectDevice.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ConnectDevice, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ConnectDevice;
  static deserializeBinaryFromReader(message: ConnectDevice, reader: jspb.BinaryReader): ConnectDevice;
}

export namespace ConnectDevice {
  export type AsObject = {
    deviceDomainHandle: number,
  }
}

export class ConnectDeviceSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ConnectDeviceSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: ConnectDeviceSuccess): ConnectDeviceSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ConnectDeviceSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ConnectDeviceSuccess;
  static deserializeBinaryFromReader(message: ConnectDeviceSuccess, reader: jspb.BinaryReader): ConnectDeviceSuccess;
}

export namespace ConnectDeviceSuccess {
  export type AsObject = {
  }
}

export class DisconnectDevice extends jspb.Message {
  getDeviceDomainHandle(): number;
  setDeviceDomainHandle(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DisconnectDevice.AsObject;
  static toObject(includeInstance: boolean, msg: DisconnectDevice): DisconnectDevice.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DisconnectDevice, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DisconnectDevice;
  static deserializeBinaryFromReader(message: DisconnectDevice, reader: jspb.BinaryReader): DisconnectDevice;
}

export namespace DisconnectDevice {
  export type AsObject = {
    deviceDomainHandle: number,
  }
}

export class DisconnectDeviceSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DisconnectDeviceSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: DisconnectDeviceSuccess): DisconnectDeviceSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DisconnectDeviceSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DisconnectDeviceSuccess;
  static deserializeBinaryFromReader(message: DisconnectDeviceSuccess, reader: jspb.BinaryReader): DisconnectDeviceSuccess;
}

export namespace DisconnectDeviceSuccess {
  export type AsObject = {
  }
}

export class SolicitDeviceDomain extends jspb.Message {
  hasDevice(): boolean;
  clearDevice(): void;
  getDevice(): Device_pb.Device | undefined;
  setDevice(value?: Device_pb.Device): void;

  getDeviceDomainHandlerTag(): string;
  setDeviceDomainHandlerTag(value: string): void;

  getData(): string;
  setData(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolicitDeviceDomain.AsObject;
  static toObject(includeInstance: boolean, msg: SolicitDeviceDomain): SolicitDeviceDomain.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SolicitDeviceDomain, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolicitDeviceDomain;
  static deserializeBinaryFromReader(message: SolicitDeviceDomain, reader: jspb.BinaryReader): SolicitDeviceDomain;
}

export namespace SolicitDeviceDomain {
  export type AsObject = {
    device?: Device_pb.Device.AsObject,
    deviceDomainHandlerTag: string,
    data: string,
  }
}

export class Search extends jspb.Message {
  getSearchId(): number;
  setSearchId(value: number): void;

  clearDeviceDomainPrototypesList(): void;
  getDeviceDomainPrototypesList(): Array<Device_pb.DeviceDomain>;
  setDeviceDomainPrototypesList(value: Array<Device_pb.DeviceDomain>): void;
  addDeviceDomainPrototypes(value?: Device_pb.DeviceDomain, index?: number): Device_pb.DeviceDomain;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Search.AsObject;
  static toObject(includeInstance: boolean, msg: Search): Search.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Search, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Search;
  static deserializeBinaryFromReader(message: Search, reader: jspb.BinaryReader): Search;
}

export namespace Search {
  export type AsObject = {
    searchId: number,
    deviceDomainPrototypesList: Array<Device_pb.DeviceDomain.AsObject>,
  }
}

export class StopSearch extends jspb.Message {
  getSearchId(): number;
  setSearchId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StopSearch.AsObject;
  static toObject(includeInstance: boolean, msg: StopSearch): StopSearch.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StopSearch, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StopSearch;
  static deserializeBinaryFromReader(message: StopSearch, reader: jspb.BinaryReader): StopSearch;
}

export namespace StopSearch {
  export type AsObject = {
    searchId: number,
  }
}

export class StoreDomainData extends jspb.Message {
  getDeviceHandle(): number;
  setDeviceHandle(value: number): void;

  getDeviceDomainHandle(): number;
  setDeviceDomainHandle(value: number): void;

  getDeviceDomainHandlerTag(): string;
  setDeviceDomainHandlerTag(value: string): void;

  getData(): string;
  setData(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StoreDomainData.AsObject;
  static toObject(includeInstance: boolean, msg: StoreDomainData): StoreDomainData.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StoreDomainData, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StoreDomainData;
  static deserializeBinaryFromReader(message: StoreDomainData, reader: jspb.BinaryReader): StoreDomainData;
}

export namespace StoreDomainData {
  export type AsObject = {
    deviceHandle: number,
    deviceDomainHandle: number,
    deviceDomainHandlerTag: string,
    data: string,
  }
}

export class SearchResult extends jspb.Message {
  getSearchId(): number;
  setSearchId(value: number): void;

  hasDeviceDomain(): boolean;
  clearDeviceDomain(): void;
  getDeviceDomain(): Device_pb.DeviceDomain | undefined;
  setDeviceDomain(value?: Device_pb.DeviceDomain): void;

  getDeviceTransportHandle(): number;
  setDeviceTransportHandle(value: number): void;

  getMatch(): boolean;
  setMatch(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SearchResult.AsObject;
  static toObject(includeInstance: boolean, msg: SearchResult): SearchResult.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SearchResult, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SearchResult;
  static deserializeBinaryFromReader(message: SearchResult, reader: jspb.BinaryReader): SearchResult;
}

export namespace SearchResult {
  export type AsObject = {
    searchId: number,
    deviceDomain?: Device_pb.DeviceDomain.AsObject,
    deviceTransportHandle: number,
    match: boolean,
  }
}

export class SolicitDevice extends jspb.Message {
  hasDeviceTransport(): boolean;
  clearDeviceTransport(): void;
  getDeviceTransport(): Device_pb.DeviceTransport | undefined;
  setDeviceTransport(value?: Device_pb.DeviceTransport): void;

  getTransportPersistentId(): string;
  setTransportPersistentId(value: string): void;

  hasDeviceDomain(): boolean;
  clearDeviceDomain(): void;
  getDeviceDomain(): Device_pb.DeviceDomain | undefined;
  setDeviceDomain(value?: Device_pb.DeviceDomain): void;

  getDeviceDomainHandlerTag(): string;
  setDeviceDomainHandlerTag(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolicitDevice.AsObject;
  static toObject(includeInstance: boolean, msg: SolicitDevice): SolicitDevice.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SolicitDevice, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolicitDevice;
  static deserializeBinaryFromReader(message: SolicitDevice, reader: jspb.BinaryReader): SolicitDevice;
}

export namespace SolicitDevice {
  export type AsObject = {
    deviceTransport?: Device_pb.DeviceTransport.AsObject,
    transportPersistentId: string,
    deviceDomain?: Device_pb.DeviceDomain.AsObject,
    deviceDomainHandlerTag: string,
  }
}

export class DeviceDomain extends jspb.Message {
  getDeviceHandle(): number;
  setDeviceHandle(value: number): void;

  getDeviceDomainHandlerTag(): string;
  setDeviceDomainHandlerTag(value: string): void;

  hasDeviceDomain(): boolean;
  clearDeviceDomain(): void;
  getDeviceDomain(): Device_pb.DeviceDomain | undefined;
  setDeviceDomain(value?: Device_pb.DeviceDomain): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeviceDomain.AsObject;
  static toObject(includeInstance: boolean, msg: DeviceDomain): DeviceDomain.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeviceDomain, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeviceDomain;
  static deserializeBinaryFromReader(message: DeviceDomain, reader: jspb.BinaryReader): DeviceDomain;
}

export namespace DeviceDomain {
  export type AsObject = {
    deviceHandle: number,
    deviceDomainHandlerTag: string,
    deviceDomain?: Device_pb.DeviceDomain.AsObject,
  }
}

