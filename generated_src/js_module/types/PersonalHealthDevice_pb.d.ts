// package: s4.messages.interfaces.personal_health_device
// file: PersonalHealthDevice.proto

import * as jspb from "google-protobuf";

export class C2P extends jspb.Message {
  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): C2P.Event | undefined;
  setEvent(value?: C2P.Event): void;

  getPayloadCase(): C2P.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): C2P.AsObject;
  static toObject(includeInstance: boolean, msg: C2P): C2P.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: C2P, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): C2P;
  static deserializeBinaryFromReader(message: C2P, reader: jspb.BinaryReader): C2P;
}

export namespace C2P {
  export type AsObject = {
    event?: C2P.Event.AsObject,
  }

  export class Event extends jspb.Message {
    hasSubscribeDeviceType(): boolean;
    clearSubscribeDeviceType(): void;
    getSubscribeDeviceType(): SubscribeDeviceType | undefined;
    setSubscribeDeviceType(value?: SubscribeDeviceType): void;

    hasUnsubscribeDeviceType(): boolean;
    clearUnsubscribeDeviceType(): void;
    getUnsubscribeDeviceType(): UnsubscribeDeviceType | undefined;
    setUnsubscribeDeviceType(value?: UnsubscribeDeviceType): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      subscribeDeviceType?: SubscribeDeviceType.AsObject,
      unsubscribeDeviceType?: UnsubscribeDeviceType.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      SUBSCRIBE_DEVICE_TYPE = 1,
      UNSUBSCRIBE_DEVICE_TYPE = 2,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    EVENT = 2,
  }
}

export class P2C extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): P2C.AsObject;
  static toObject(includeInstance: boolean, msg: P2C): P2C.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: P2C, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): P2C;
  static deserializeBinaryFromReader(message: P2C, reader: jspb.BinaryReader): P2C;
}

export namespace P2C {
  export type AsObject = {
  }
}

export class SubscribeDeviceType extends jspb.Message {
  getMdcCode(): number;
  setMdcCode(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SubscribeDeviceType.AsObject;
  static toObject(includeInstance: boolean, msg: SubscribeDeviceType): SubscribeDeviceType.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SubscribeDeviceType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SubscribeDeviceType;
  static deserializeBinaryFromReader(message: SubscribeDeviceType, reader: jspb.BinaryReader): SubscribeDeviceType;
}

export namespace SubscribeDeviceType {
  export type AsObject = {
    mdcCode: number,
  }
}

export class UnsubscribeDeviceType extends jspb.Message {
  getMdcCode(): number;
  setMdcCode(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UnsubscribeDeviceType.AsObject;
  static toObject(includeInstance: boolean, msg: UnsubscribeDeviceType): UnsubscribeDeviceType.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UnsubscribeDeviceType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UnsubscribeDeviceType;
  static deserializeBinaryFromReader(message: UnsubscribeDeviceType, reader: jspb.BinaryReader): UnsubscribeDeviceType;
}

export namespace UnsubscribeDeviceType {
  export type AsObject = {
    mdcCode: number,
  }
}

