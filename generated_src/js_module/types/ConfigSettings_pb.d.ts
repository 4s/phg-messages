// package: s4.messages.interfaces.config_settings
// file: ConfigSettings.proto

import * as jspb from "google-protobuf";

export class C2P extends jspb.Message {
  hasRequest(): boolean;
  clearRequest(): void;
  getRequest(): C2P.Request | undefined;
  setRequest(value?: C2P.Request): void;

  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): C2P.Event | undefined;
  setEvent(value?: C2P.Event): void;

  getPayloadCase(): C2P.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): C2P.AsObject;
  static toObject(includeInstance: boolean, msg: C2P): C2P.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: C2P, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): C2P;
  static deserializeBinaryFromReader(message: C2P, reader: jspb.BinaryReader): C2P;
}

export namespace C2P {
  export type AsObject = {
    request?: C2P.Request.AsObject,
    event?: C2P.Event.AsObject,
  }

  export class Request extends jspb.Message {
    hasReadConfiguration(): boolean;
    clearReadConfiguration(): void;
    getReadConfiguration(): ReadConfiguration | undefined;
    setReadConfiguration(value?: ReadConfiguration): void;

    hasWriteConfiguration(): boolean;
    clearWriteConfiguration(): void;
    getWriteConfiguration(): WriteConfiguration | undefined;
    setWriteConfiguration(value?: WriteConfiguration): void;

    getRequestCase(): Request.RequestCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Request.AsObject;
    static toObject(includeInstance: boolean, msg: Request): Request.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Request, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Request;
    static deserializeBinaryFromReader(message: Request, reader: jspb.BinaryReader): Request;
  }

  export namespace Request {
    export type AsObject = {
      readConfiguration?: ReadConfiguration.AsObject,
      writeConfiguration?: WriteConfiguration.AsObject,
    }

    export enum RequestCase {
      REQUEST_NOT_SET = 0,
      READ_CONFIGURATION = 1,
      WRITE_CONFIGURATION = 2,
    }
  }

  export class Event extends jspb.Message {
    hasSolicitStore(): boolean;
    clearSolicitStore(): void;
    getSolicitStore(): SolicitStore | undefined;
    setSolicitStore(value?: SolicitStore): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      solicitStore?: SolicitStore.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      SOLICIT_STORE = 1,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    REQUEST = 1,
    EVENT = 2,
  }
}

export class P2C extends jspb.Message {
  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): P2C.Event | undefined;
  setEvent(value?: P2C.Event): void;

  getPayloadCase(): P2C.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): P2C.AsObject;
  static toObject(includeInstance: boolean, msg: P2C): P2C.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: P2C, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): P2C;
  static deserializeBinaryFromReader(message: P2C, reader: jspb.BinaryReader): P2C;
}

export namespace P2C {
  export type AsObject = {
    event?: P2C.Event.AsObject,
  }

  export class Event extends jspb.Message {
    hasAnnounceStore(): boolean;
    clearAnnounceStore(): void;
    getAnnounceStore(): AnnounceStore | undefined;
    setAnnounceStore(value?: AnnounceStore): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      announceStore?: AnnounceStore.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      ANNOUNCE_STORE = 1,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    EVENT = 2,
  }
}

export class KeyValuePair extends jspb.Message {
  getKey(): string;
  setKey(value: string): void;

  getValue(): string;
  setValue(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): KeyValuePair.AsObject;
  static toObject(includeInstance: boolean, msg: KeyValuePair): KeyValuePair.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: KeyValuePair, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): KeyValuePair;
  static deserializeBinaryFromReader(message: KeyValuePair, reader: jspb.BinaryReader): KeyValuePair;
}

export namespace KeyValuePair {
  export type AsObject = {
    key: string,
    value: string,
  }
}

export class ReadConfiguration extends jspb.Message {
  getConfigurationName(): string;
  setConfigurationName(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReadConfiguration.AsObject;
  static toObject(includeInstance: boolean, msg: ReadConfiguration): ReadConfiguration.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ReadConfiguration, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReadConfiguration;
  static deserializeBinaryFromReader(message: ReadConfiguration, reader: jspb.BinaryReader): ReadConfiguration;
}

export namespace ReadConfiguration {
  export type AsObject = {
    configurationName: string,
  }
}

export class ReadConfigurationSuccess extends jspb.Message {
  clearConfigurationList(): void;
  getConfigurationList(): Array<KeyValuePair>;
  setConfigurationList(value: Array<KeyValuePair>): void;
  addConfiguration(value?: KeyValuePair, index?: number): KeyValuePair;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReadConfigurationSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: ReadConfigurationSuccess): ReadConfigurationSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ReadConfigurationSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReadConfigurationSuccess;
  static deserializeBinaryFromReader(message: ReadConfigurationSuccess, reader: jspb.BinaryReader): ReadConfigurationSuccess;
}

export namespace ReadConfigurationSuccess {
  export type AsObject = {
    configurationList: Array<KeyValuePair.AsObject>,
  }
}

export class WriteConfiguration extends jspb.Message {
  getConfigurationName(): string;
  setConfigurationName(value: string): void;

  clearConfigurationList(): void;
  getConfigurationList(): Array<KeyValuePair>;
  setConfigurationList(value: Array<KeyValuePair>): void;
  addConfiguration(value?: KeyValuePair, index?: number): KeyValuePair;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WriteConfiguration.AsObject;
  static toObject(includeInstance: boolean, msg: WriteConfiguration): WriteConfiguration.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: WriteConfiguration, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WriteConfiguration;
  static deserializeBinaryFromReader(message: WriteConfiguration, reader: jspb.BinaryReader): WriteConfiguration;
}

export namespace WriteConfiguration {
  export type AsObject = {
    configurationName: string,
    configurationList: Array<KeyValuePair.AsObject>,
  }
}

export class WriteConfigurationSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WriteConfigurationSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: WriteConfigurationSuccess): WriteConfigurationSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: WriteConfigurationSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WriteConfigurationSuccess;
  static deserializeBinaryFromReader(message: WriteConfigurationSuccess, reader: jspb.BinaryReader): WriteConfigurationSuccess;
}

export namespace WriteConfigurationSuccess {
  export type AsObject = {
  }
}

export class SolicitStore extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolicitStore.AsObject;
  static toObject(includeInstance: boolean, msg: SolicitStore): SolicitStore.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SolicitStore, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolicitStore;
  static deserializeBinaryFromReader(message: SolicitStore, reader: jspb.BinaryReader): SolicitStore;
}

export namespace SolicitStore {
  export type AsObject = {
  }
}

export class AnnounceStore extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AnnounceStore.AsObject;
  static toObject(includeInstance: boolean, msg: AnnounceStore): AnnounceStore.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AnnounceStore, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AnnounceStore;
  static deserializeBinaryFromReader(message: AnnounceStore, reader: jspb.BinaryReader): AnnounceStore;
}

export namespace AnnounceStore {
  export type AsObject = {
  }
}

