// package: s4.messages.classes.log
// file: Log.proto

import * as jspb from "google-protobuf";
import * as Error_pb from "./Error_pb";

export class Log extends jspb.Message {
  getModuleTag(): string;
  setModuleTag(value: string): void;

  getLogLevel(): LogLevelMap[keyof LogLevelMap];
  setLogLevel(value: LogLevelMap[keyof LogLevelMap]): void;

  getMessage(): string;
  setMessage(value: string): void;

  getFileName(): string;
  setFileName(value: string): void;

  getLineNumber(): number;
  setLineNumber(value: number): void;

  getClassName(): string;
  setClassName(value: string): void;

  getFunctionName(): string;
  setFunctionName(value: string): void;

  hasCausedBy(): boolean;
  clearCausedBy(): void;
  getCausedBy(): Error_pb.Error | undefined;
  setCausedBy(value?: Error_pb.Error): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Log.AsObject;
  static toObject(includeInstance: boolean, msg: Log): Log.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Log, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Log;
  static deserializeBinaryFromReader(message: Log, reader: jspb.BinaryReader): Log;
}

export namespace Log {
  export type AsObject = {
    moduleTag: string,
    logLevel: LogLevelMap[keyof LogLevelMap],
    message: string,
    fileName: string,
    lineNumber: number,
    className: string,
    functionName: string,
    causedBy?: Error_pb.Error.AsObject,
  }
}

export interface LogLevelMap {
  DEBUG: 0;
  INFO: 1;
  WARN: 2;
  ERROR: 3;
  FATAL: 4;
}

export const LogLevel: LogLevelMap;

