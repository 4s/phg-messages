// package: s4.messages.interfaces.monica
// file: Monica.proto

import * as jspb from "google-protobuf";

export class C2P extends jspb.Message {
  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): C2P.Event | undefined;
  setEvent(value?: C2P.Event): void;

  getPayloadCase(): C2P.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): C2P.AsObject;
  static toObject(includeInstance: boolean, msg: C2P): C2P.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: C2P, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): C2P;
  static deserializeBinaryFromReader(message: C2P, reader: jspb.BinaryReader): C2P;
}

export namespace C2P {
  export type AsObject = {
    event?: C2P.Event.AsObject,
  }

  export class Event extends jspb.Message {
    hasStart(): boolean;
    clearStart(): void;
    getStart(): Start | undefined;
    setStart(value?: Start): void;

    hasStop(): boolean;
    clearStop(): void;
    getStop(): Stop | undefined;
    setStop(value?: Stop): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      start?: Start.AsObject,
      stop?: Stop.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      START = 1,
      STOP = 2,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    EVENT = 2,
  }
}

export class P2C extends jspb.Message {
  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): P2C.Event | undefined;
  setEvent(value?: P2C.Event): void;

  getPayloadCase(): P2C.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): P2C.AsObject;
  static toObject(includeInstance: boolean, msg: P2C): P2C.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: P2C, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): P2C;
  static deserializeBinaryFromReader(message: P2C, reader: jspb.BinaryReader): P2C;
}

export namespace P2C {
  export type AsObject = {
    event?: P2C.Event.AsObject,
  }

  export class Event extends jspb.Message {
    hasCurrentStatus(): boolean;
    clearCurrentStatus(): void;
    getCurrentStatus(): CurrentStatus | undefined;
    setCurrentStatus(value?: CurrentStatus): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      currentStatus?: CurrentStatus.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      CURRENT_STATUS = 1,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    EVENT = 2,
  }
}

export class Start extends jspb.Message {
  clearSettingsList(): void;
  getSettingsList(): Array<Start.Property>;
  setSettingsList(value: Array<Start.Property>): void;
  addSettings(value?: Start.Property, index?: number): Start.Property;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Start.AsObject;
  static toObject(includeInstance: boolean, msg: Start): Start.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Start, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Start;
  static deserializeBinaryFromReader(message: Start, reader: jspb.BinaryReader): Start;
}

export namespace Start {
  export type AsObject = {
    settingsList: Array<Start.Property.AsObject>,
  }

  export class Property extends jspb.Message {
    getAttribute(): number;
    setAttribute(value: number): void;

    getValue(): number;
    setValue(value: number): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Property.AsObject;
    static toObject(includeInstance: boolean, msg: Property): Property.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Property, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Property;
    static deserializeBinaryFromReader(message: Property, reader: jspb.BinaryReader): Property;
  }

  export namespace Property {
    export type AsObject = {
      attribute: number,
      value: number,
    }
  }
}

export class Stop extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Stop.AsObject;
  static toObject(includeInstance: boolean, msg: Stop): Stop.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Stop, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Stop;
  static deserializeBinaryFromReader(message: Stop, reader: jspb.BinaryReader): Stop;
}

export namespace Stop {
  export type AsObject = {
  }
}

export class CurrentStatus extends jspb.Message {
  getCurrentStatus(): string;
  setCurrentStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CurrentStatus.AsObject;
  static toObject(includeInstance: boolean, msg: CurrentStatus): CurrentStatus.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CurrentStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CurrentStatus;
  static deserializeBinaryFromReader(message: CurrentStatus, reader: jspb.BinaryReader): CurrentStatus;
}

export namespace CurrentStatus {
  export type AsObject = {
    currentStatus: string,
  }
}

