// package: s4.messages.classes.device
// file: Device.proto

import * as jspb from "google-protobuf";

export class Device extends jspb.Message {
  getHandler(): number;
  setHandler(value: number): void;

  getHandle(): number;
  setHandle(value: number): void;

  getDevicePersistentId(): string;
  setDevicePersistentId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getConnected(): boolean;
  setConnected(value: boolean): void;

  getLinked(): boolean;
  setLinked(value: boolean): void;

  hasTransport(): boolean;
  clearTransport(): void;
  getTransport(): DeviceTransport | undefined;
  setTransport(value?: DeviceTransport): void;

  clearSupportedDomainsList(): void;
  getSupportedDomainsList(): Array<DeviceDomain>;
  setSupportedDomainsList(value: Array<DeviceDomain>): void;
  addSupportedDomains(value?: DeviceDomain, index?: number): DeviceDomain;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Device.AsObject;
  static toObject(includeInstance: boolean, msg: Device): Device.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Device, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Device;
  static deserializeBinaryFromReader(message: Device, reader: jspb.BinaryReader): Device;
}

export namespace Device {
  export type AsObject = {
    handler: number,
    handle: number,
    devicePersistentId: string,
    name: string,
    connected: boolean,
    linked: boolean,
    transport?: DeviceTransport.AsObject,
    supportedDomainsList: Array<DeviceDomain.AsObject>,
  }
}

export class DeviceTransport extends jspb.Message {
  getHandler(): number;
  setHandler(value: number): void;

  getHandle(): number;
  setHandle(value: number): void;

  getTransportLinkCapable(): boolean;
  setTransportLinkCapable(value: boolean): void;

  getTransportUnlinkCapable(): boolean;
  setTransportUnlinkCapable(value: boolean): void;

  hasBluetoothDevice(): boolean;
  clearBluetoothDevice(): void;
  getBluetoothDevice(): BluetoothDevice | undefined;
  setBluetoothDevice(value?: BluetoothDevice): void;

  getSubClassCase(): DeviceTransport.SubClassCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeviceTransport.AsObject;
  static toObject(includeInstance: boolean, msg: DeviceTransport): DeviceTransport.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeviceTransport, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeviceTransport;
  static deserializeBinaryFromReader(message: DeviceTransport, reader: jspb.BinaryReader): DeviceTransport;
}

export namespace DeviceTransport {
  export type AsObject = {
    handler: number,
    handle: number,
    transportLinkCapable: boolean,
    transportUnlinkCapable: boolean,
    bluetoothDevice?: BluetoothDevice.AsObject,
  }

  export enum SubClassCase {
    SUB_CLASS_NOT_SET = 0,
    BLUETOOTH_DEVICE = 5,
  }
}

export class BluetoothDevice extends jspb.Message {
  getBdAddr(): string;
  setBdAddr(value: string): void;

  getName(): string;
  setName(value: string): void;

  getClassic(): boolean;
  setClassic(value: boolean): void;

  getLe(): boolean;
  setLe(value: boolean): void;

  getBonded(): boolean;
  setBonded(value: boolean): void;

  clearBluetoothProfilesList(): void;
  getBluetoothProfilesList(): Array<BluetoothDevice.BluetoothProfile>;
  setBluetoothProfilesList(value: Array<BluetoothDevice.BluetoothProfile>): void;
  addBluetoothProfiles(value?: BluetoothDevice.BluetoothProfile, index?: number): BluetoothDevice.BluetoothProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BluetoothDevice.AsObject;
  static toObject(includeInstance: boolean, msg: BluetoothDevice): BluetoothDevice.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: BluetoothDevice, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BluetoothDevice;
  static deserializeBinaryFromReader(message: BluetoothDevice, reader: jspb.BinaryReader): BluetoothDevice;
}

export namespace BluetoothDevice {
  export type AsObject = {
    bdAddr: string,
    name: string,
    classic: boolean,
    le: boolean,
    bonded: boolean,
    bluetoothProfilesList: Array<BluetoothDevice.BluetoothProfile.AsObject>,
  }

  export class BluetoothProfile extends jspb.Message {
    getHandler(): number;
    setHandler(value: number): void;

    getHandle(): number;
    setHandle(value: number): void;

    getServiceName(): string;
    setServiceName(value: string): void;

    getServiceDescription(): string;
    setServiceDescription(value: string): void;

    getProviderName(): string;
    setProviderName(value: string): void;

    getProfile(): BluetoothDevice.BluetoothProfile.ProfileMap[keyof BluetoothDevice.BluetoothProfile.ProfileMap];
    setProfile(value: BluetoothDevice.BluetoothProfile.ProfileMap[keyof BluetoothDevice.BluetoothProfile.ProfileMap]): void;

    getAppearance(): number;
    setAppearance(value: number): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): BluetoothProfile.AsObject;
    static toObject(includeInstance: boolean, msg: BluetoothProfile): BluetoothProfile.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: BluetoothProfile, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): BluetoothProfile;
    static deserializeBinaryFromReader(message: BluetoothProfile, reader: jspb.BinaryReader): BluetoothProfile;
  }

  export namespace BluetoothProfile {
    export type AsObject = {
      handler: number,
      handle: number,
      serviceName: string,
      serviceDescription: string,
      providerName: string,
      profile: BluetoothDevice.BluetoothProfile.ProfileMap[keyof BluetoothDevice.BluetoothProfile.ProfileMap],
      appearance: number,
    }

    export interface ProfileMap {
      HEALTH_DEVICE_PROFILE: 0;
      SERIAL_PORT_PROFILE: 1;
      GENERIC_ATTRIBUTE_PROFILE: 2;
    }

    export const Profile: ProfileMap;
  }
}

export class DeviceDomain extends jspb.Message {
  getHandler(): number;
  setHandler(value: number): void;

  getHandle(): number;
  setHandle(value: number): void;

  getConnectable(): DeviceDomain.ConnectableMap[keyof DeviceDomain.ConnectableMap];
  setConnectable(value: DeviceDomain.ConnectableMap[keyof DeviceDomain.ConnectableMap]): void;

  getDisconnectable(): boolean;
  setDisconnectable(value: boolean): void;

  hasPersonalHealthDevice(): boolean;
  clearPersonalHealthDevice(): void;
  getPersonalHealthDevice(): PHDevice | undefined;
  setPersonalHealthDevice(value?: PHDevice): void;

  getSubClassCase(): DeviceDomain.SubClassCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeviceDomain.AsObject;
  static toObject(includeInstance: boolean, msg: DeviceDomain): DeviceDomain.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeviceDomain, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeviceDomain;
  static deserializeBinaryFromReader(message: DeviceDomain, reader: jspb.BinaryReader): DeviceDomain;
}

export namespace DeviceDomain {
  export type AsObject = {
    handler: number,
    handle: number,
    connectable: DeviceDomain.ConnectableMap[keyof DeviceDomain.ConnectableMap],
    disconnectable: boolean,
    personalHealthDevice?: PHDevice.AsObject,
  }

  export interface ConnectableMap {
    NEVER: 0;
    WHEN_LINKED: 1;
    ALWAYS: 2;
  }

  export const Connectable: ConnectableMap;

  export enum SubClassCase {
    SUB_CLASS_NOT_SET = 0,
    PERSONAL_HEALTH_DEVICE = 5,
  }
}

export class PHDevice extends jspb.Message {
  clearTypeSpecsList(): void;
  getTypeSpecsList(): Array<number>;
  setTypeSpecsList(value: Array<number>): void;
  addTypeSpecs(value: number, index?: number): number;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PHDevice.AsObject;
  static toObject(includeInstance: boolean, msg: PHDevice): PHDevice.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PHDevice, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PHDevice;
  static deserializeBinaryFromReader(message: PHDevice, reader: jspb.BinaryReader): PHDevice;
}

export namespace PHDevice {
  export type AsObject = {
    typeSpecsList: Array<number>,
  }
}

