// package: s4.messages.interfaces.fhir_measurement
// file: FHIRMeasurement.proto

import * as jspb from "google-protobuf";

export class C2P extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): C2P.AsObject;
  static toObject(includeInstance: boolean, msg: C2P): C2P.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: C2P, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): C2P;
  static deserializeBinaryFromReader(message: C2P, reader: jspb.BinaryReader): C2P;
}

export namespace C2P {
  export type AsObject = {
  }
}

export class P2C extends jspb.Message {
  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): P2C.Event | undefined;
  setEvent(value?: P2C.Event): void;

  getPayloadCase(): P2C.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): P2C.AsObject;
  static toObject(includeInstance: boolean, msg: P2C): P2C.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: P2C, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): P2C;
  static deserializeBinaryFromReader(message: P2C, reader: jspb.BinaryReader): P2C;
}

export namespace P2C {
  export type AsObject = {
    event?: P2C.Event.AsObject,
  }

  export class Event extends jspb.Message {
    hasMeasurementReceived(): boolean;
    clearMeasurementReceived(): void;
    getMeasurementReceived(): MeasurementReceived | undefined;
    setMeasurementReceived(value?: MeasurementReceived): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      measurementReceived?: MeasurementReceived.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      MEASUREMENT_RECEIVED = 1,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    EVENT = 2,
  }
}

export class MeasurementReceived extends jspb.Message {
  getDeviceType(): string;
  setDeviceType(value: string): void;

  getFhirObservation(): string;
  setFhirObservation(value: string): void;

  getFhirDevice(): string;
  setFhirDevice(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MeasurementReceived.AsObject;
  static toObject(includeInstance: boolean, msg: MeasurementReceived): MeasurementReceived.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MeasurementReceived, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MeasurementReceived;
  static deserializeBinaryFromReader(message: MeasurementReceived, reader: jspb.BinaryReader): MeasurementReceived;
}

export namespace MeasurementReceived {
  export type AsObject = {
    deviceType: string,
    fhirObservation: string,
    fhirDevice: string,
  }
}

