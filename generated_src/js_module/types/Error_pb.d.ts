// package: s4.messages.classes.error
// file: Error.proto

import * as jspb from "google-protobuf";

export class Error extends jspb.Message {
  getInterfaceName(): string;
  setInterfaceName(value: string): void;

  getErrorCode(): number;
  setErrorCode(value: number): void;

  getModuleTag(): string;
  setModuleTag(value: string): void;

  getMessage(): string;
  setMessage(value: string): void;

  getDetails(): string;
  setDetails(value: string): void;

  getFileName(): string;
  setFileName(value: string): void;

  getLineNumber(): number;
  setLineNumber(value: number): void;

  getClassName(): string;
  setClassName(value: string): void;

  getFunctionName(): string;
  setFunctionName(value: string): void;

  hasCausedBy(): boolean;
  clearCausedBy(): void;
  getCausedBy(): Error | undefined;
  setCausedBy(value?: Error): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Error.AsObject;
  static toObject(includeInstance: boolean, msg: Error): Error.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Error, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Error;
  static deserializeBinaryFromReader(message: Error, reader: jspb.BinaryReader): Error;
}

export namespace Error {
  export type AsObject = {
    interfaceName: string,
    errorCode: number,
    moduleTag: string,
    message: string,
    details: string,
    fileName: string,
    lineNumber: number,
    className: string,
    functionName: string,
    causedBy?: Error.AsObject,
  }
}

