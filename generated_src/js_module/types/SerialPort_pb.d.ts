// package: s4.messages.interfaces.serial_port
// file: SerialPort.proto

import * as jspb from "google-protobuf";

export class C2P extends jspb.Message {
  hasRequest(): boolean;
  clearRequest(): void;
  getRequest(): C2P.Request | undefined;
  setRequest(value?: C2P.Request): void;

  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): C2P.Event | undefined;
  setEvent(value?: C2P.Event): void;

  getPayloadCase(): C2P.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): C2P.AsObject;
  static toObject(includeInstance: boolean, msg: C2P): C2P.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: C2P, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): C2P;
  static deserializeBinaryFromReader(message: C2P, reader: jspb.BinaryReader): C2P;
}

export namespace C2P {
  export type AsObject = {
    request?: C2P.Request.AsObject,
    event?: C2P.Event.AsObject,
  }

  export class Request extends jspb.Message {
    hasOpen(): boolean;
    clearOpen(): void;
    getOpen(): Open | undefined;
    setOpen(value?: Open): void;

    hasWrite(): boolean;
    clearWrite(): void;
    getWrite(): Write | undefined;
    setWrite(value?: Write): void;

    getRequestCase(): Request.RequestCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Request.AsObject;
    static toObject(includeInstance: boolean, msg: Request): Request.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Request, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Request;
    static deserializeBinaryFromReader(message: Request, reader: jspb.BinaryReader): Request;
  }

  export namespace Request {
    export type AsObject = {
      open?: Open.AsObject,
      write?: Write.AsObject,
    }

    export enum RequestCase {
      REQUEST_NOT_SET = 0,
      OPEN = 1,
      WRITE = 2,
    }
  }

  export class Event extends jspb.Message {
    hasClose(): boolean;
    clearClose(): void;
    getClose(): Close | undefined;
    setClose(value?: Close): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      close?: Close.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      CLOSE = 1,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    REQUEST = 1,
    EVENT = 2,
  }
}

export class P2C extends jspb.Message {
  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): P2C.Event | undefined;
  setEvent(value?: P2C.Event): void;

  getPayloadCase(): P2C.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): P2C.AsObject;
  static toObject(includeInstance: boolean, msg: P2C): P2C.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: P2C, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): P2C;
  static deserializeBinaryFromReader(message: P2C, reader: jspb.BinaryReader): P2C;
}

export namespace P2C {
  export type AsObject = {
    event?: P2C.Event.AsObject,
  }

  export class Event extends jspb.Message {
    hasClosed(): boolean;
    clearClosed(): void;
    getClosed(): Closed | undefined;
    setClosed(value?: Closed): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      closed?: Closed.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      CLOSED = 1,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    EVENT = 2,
  }
}

export class Open extends jspb.Message {
  getSerialPortHandle(): number;
  setSerialPortHandle(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Open.AsObject;
  static toObject(includeInstance: boolean, msg: Open): Open.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Open, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Open;
  static deserializeBinaryFromReader(message: Open, reader: jspb.BinaryReader): Open;
}

export namespace Open {
  export type AsObject = {
    serialPortHandle: number,
  }
}

export class OpenSuccess extends jspb.Message {
  getMessage(): Uint8Array | string;
  getMessage_asU8(): Uint8Array;
  getMessage_asB64(): string;
  setMessage(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OpenSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: OpenSuccess): OpenSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OpenSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OpenSuccess;
  static deserializeBinaryFromReader(message: OpenSuccess, reader: jspb.BinaryReader): OpenSuccess;
}

export namespace OpenSuccess {
  export type AsObject = {
    message: Uint8Array | string,
  }
}

export class Close extends jspb.Message {
  getSerialPortHandle(): number;
  setSerialPortHandle(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Close.AsObject;
  static toObject(includeInstance: boolean, msg: Close): Close.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Close, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Close;
  static deserializeBinaryFromReader(message: Close, reader: jspb.BinaryReader): Close;
}

export namespace Close {
  export type AsObject = {
    serialPortHandle: number,
  }
}

export class Closed extends jspb.Message {
  getSerialPortHandle(): number;
  setSerialPortHandle(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Closed.AsObject;
  static toObject(includeInstance: boolean, msg: Closed): Closed.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Closed, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Closed;
  static deserializeBinaryFromReader(message: Closed, reader: jspb.BinaryReader): Closed;
}

export namespace Closed {
  export type AsObject = {
    serialPortHandle: number,
  }
}

export class Write extends jspb.Message {
  getSerialPortHandle(): number;
  setSerialPortHandle(value: number): void;

  getMessage(): Uint8Array | string;
  getMessage_asU8(): Uint8Array;
  getMessage_asB64(): string;
  setMessage(value: Uint8Array | string): void;

  getFlush(): boolean;
  setFlush(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Write.AsObject;
  static toObject(includeInstance: boolean, msg: Write): Write.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Write, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Write;
  static deserializeBinaryFromReader(message: Write, reader: jspb.BinaryReader): Write;
}

export namespace Write {
  export type AsObject = {
    serialPortHandle: number,
    message: Uint8Array | string,
    flush: boolean,
  }
}

export class WriteSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WriteSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: WriteSuccess): WriteSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: WriteSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WriteSuccess;
  static deserializeBinaryFromReader(message: WriteSuccess, reader: jspb.BinaryReader): WriteSuccess;
}

export namespace WriteSuccess {
  export type AsObject = {
  }
}

