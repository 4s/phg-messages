// package: s4.messages.interfaces.bluetooth_device_control
// file: BluetoothDeviceControl.proto

import * as jspb from "google-protobuf";

export class C2P extends jspb.Message {
  hasRequest(): boolean;
  clearRequest(): void;
  getRequest(): C2P.Request | undefined;
  setRequest(value?: C2P.Request): void;

  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): C2P.Event | undefined;
  setEvent(value?: C2P.Event): void;

  getPayloadCase(): C2P.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): C2P.AsObject;
  static toObject(includeInstance: boolean, msg: C2P): C2P.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: C2P, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): C2P;
  static deserializeBinaryFromReader(message: C2P, reader: jspb.BinaryReader): C2P;
}

export namespace C2P {
  export type AsObject = {
    request?: C2P.Request.AsObject,
    event?: C2P.Event.AsObject,
  }

  export class Request extends jspb.Message {
    hasBond(): boolean;
    clearBond(): void;
    getBond(): Bond | undefined;
    setBond(value?: Bond): void;

    hasUnbond(): boolean;
    clearUnbond(): void;
    getUnbond(): Unbond | undefined;
    setUnbond(value?: Unbond): void;

    getRequestCase(): Request.RequestCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Request.AsObject;
    static toObject(includeInstance: boolean, msg: Request): Request.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Request, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Request;
    static deserializeBinaryFromReader(message: Request, reader: jspb.BinaryReader): Request;
  }

  export namespace Request {
    export type AsObject = {
      bond?: Bond.AsObject,
      unbond?: Unbond.AsObject,
    }

    export enum RequestCase {
      REQUEST_NOT_SET = 0,
      BOND = 1,
      UNBOND = 2,
    }
  }

  export class Event extends jspb.Message {
    hasSetStandardPasskey(): boolean;
    clearSetStandardPasskey(): void;
    getSetStandardPasskey(): SetStandardPasskey | undefined;
    setSetStandardPasskey(value?: SetStandardPasskey): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      setStandardPasskey?: SetStandardPasskey.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      SET_STANDARD_PASSKEY = 1,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    REQUEST = 1,
    EVENT = 2,
  }
}

export class P2C extends jspb.Message {
  hasRequest(): boolean;
  clearRequest(): void;
  getRequest(): P2C.Request | undefined;
  setRequest(value?: P2C.Request): void;

  getPayloadCase(): P2C.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): P2C.AsObject;
  static toObject(includeInstance: boolean, msg: P2C): P2C.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: P2C, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): P2C;
  static deserializeBinaryFromReader(message: P2C, reader: jspb.BinaryReader): P2C;
}

export namespace P2C {
  export type AsObject = {
    request?: P2C.Request.AsObject,
  }

  export class Request extends jspb.Message {
    hasBluetoothPairing(): boolean;
    clearBluetoothPairing(): void;
    getBluetoothPairing(): BluetoothPairing | undefined;
    setBluetoothPairing(value?: BluetoothPairing): void;

    getRequestCase(): Request.RequestCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Request.AsObject;
    static toObject(includeInstance: boolean, msg: Request): Request.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Request, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Request;
    static deserializeBinaryFromReader(message: Request, reader: jspb.BinaryReader): Request;
  }

  export namespace Request {
    export type AsObject = {
      bluetoothPairing?: BluetoothPairing.AsObject,
    }

    export enum RequestCase {
      REQUEST_NOT_SET = 0,
      BLUETOOTH_PAIRING = 1,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    REQUEST = 1,
  }
}

export class Bond extends jspb.Message {
  getHandle(): number;
  setHandle(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Bond.AsObject;
  static toObject(includeInstance: boolean, msg: Bond): Bond.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Bond, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Bond;
  static deserializeBinaryFromReader(message: Bond, reader: jspb.BinaryReader): Bond;
}

export namespace Bond {
  export type AsObject = {
    handle: number,
  }
}

export class BondSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BondSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: BondSuccess): BondSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: BondSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BondSuccess;
  static deserializeBinaryFromReader(message: BondSuccess, reader: jspb.BinaryReader): BondSuccess;
}

export namespace BondSuccess {
  export type AsObject = {
  }
}

export class Unbond extends jspb.Message {
  getHandle(): number;
  setHandle(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Unbond.AsObject;
  static toObject(includeInstance: boolean, msg: Unbond): Unbond.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Unbond, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Unbond;
  static deserializeBinaryFromReader(message: Unbond, reader: jspb.BinaryReader): Unbond;
}

export namespace Unbond {
  export type AsObject = {
    handle: number,
  }
}

export class UnbondSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UnbondSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: UnbondSuccess): UnbondSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UnbondSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UnbondSuccess;
  static deserializeBinaryFromReader(message: UnbondSuccess, reader: jspb.BinaryReader): UnbondSuccess;
}

export namespace UnbondSuccess {
  export type AsObject = {
  }
}

export class SetStandardPasskey extends jspb.Message {
  getHandle(): number;
  setHandle(value: number): void;

  getPasskey(): string;
  setPasskey(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetStandardPasskey.AsObject;
  static toObject(includeInstance: boolean, msg: SetStandardPasskey): SetStandardPasskey.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetStandardPasskey, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetStandardPasskey;
  static deserializeBinaryFromReader(message: SetStandardPasskey, reader: jspb.BinaryReader): SetStandardPasskey;
}

export namespace SetStandardPasskey {
  export type AsObject = {
    handle: number,
    passkey: string,
  }
}

export class BluetoothPairing extends jspb.Message {
  getHandle(): number;
  setHandle(value: number): void;

  getMethod(): BluetoothPairing.MethodMap[keyof BluetoothPairing.MethodMap];
  setMethod(value: BluetoothPairing.MethodMap[keyof BluetoothPairing.MethodMap]): void;

  getNumericComparisonValue(): number;
  setNumericComparisonValue(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BluetoothPairing.AsObject;
  static toObject(includeInstance: boolean, msg: BluetoothPairing): BluetoothPairing.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: BluetoothPairing, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BluetoothPairing;
  static deserializeBinaryFromReader(message: BluetoothPairing, reader: jspb.BinaryReader): BluetoothPairing;
}

export namespace BluetoothPairing {
  export type AsObject = {
    handle: number,
    method: BluetoothPairing.MethodMap[keyof BluetoothPairing.MethodMap],
    numericComparisonValue: number,
  }

  export interface MethodMap {
    LEGACY: 0;
    JUST_WORKS: 1;
    NUMERIC_COMPARISON: 2;
    PASSKEY: 3;
  }

  export const Method: MethodMap;
}

export class BluetoothPairingSuccess extends jspb.Message {
  hasLegacy(): boolean;
  clearLegacy(): void;
  getLegacy(): string;
  setLegacy(value: string): void;

  hasAccept(): boolean;
  clearAccept(): void;
  getAccept(): boolean;
  setAccept(value: boolean): void;

  hasPasskey(): boolean;
  clearPasskey(): void;
  getPasskey(): number;
  setPasskey(value: number): void;

  getMethodCase(): BluetoothPairingSuccess.MethodCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BluetoothPairingSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: BluetoothPairingSuccess): BluetoothPairingSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: BluetoothPairingSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BluetoothPairingSuccess;
  static deserializeBinaryFromReader(message: BluetoothPairingSuccess, reader: jspb.BinaryReader): BluetoothPairingSuccess;
}

export namespace BluetoothPairingSuccess {
  export type AsObject = {
    legacy: string,
    accept: boolean,
    passkey: number,
  }

  export enum MethodCase {
    METHOD_NOT_SET = 0,
    LEGACY = 1,
    ACCEPT = 2,
    PASSKEY = 3,
  }
}

