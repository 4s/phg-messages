// package: s4.messages.interfaces.time
// file: Time.proto

import * as jspb from "google-protobuf";

export class C2P extends jspb.Message {
  hasRequest(): boolean;
  clearRequest(): void;
  getRequest(): C2P.Request | undefined;
  setRequest(value?: C2P.Request): void;

  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): C2P.Event | undefined;
  setEvent(value?: C2P.Event): void;

  getPayloadCase(): C2P.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): C2P.AsObject;
  static toObject(includeInstance: boolean, msg: C2P): C2P.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: C2P, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): C2P;
  static deserializeBinaryFromReader(message: C2P, reader: jspb.BinaryReader): C2P;
}

export namespace C2P {
  export type AsObject = {
    request?: C2P.Request.AsObject,
    event?: C2P.Event.AsObject,
  }

  export class Request extends jspb.Message {
    hasReadTime(): boolean;
    clearReadTime(): void;
    getReadTime(): ReadTime | undefined;
    setReadTime(value?: ReadTime): void;

    hasStartTimer(): boolean;
    clearStartTimer(): void;
    getStartTimer(): StartTimer | undefined;
    setStartTimer(value?: StartTimer): void;

    hasStopTimer(): boolean;
    clearStopTimer(): void;
    getStopTimer(): StopTimer | undefined;
    setStopTimer(value?: StopTimer): void;

    getRequestCase(): Request.RequestCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Request.AsObject;
    static toObject(includeInstance: boolean, msg: Request): Request.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Request, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Request;
    static deserializeBinaryFromReader(message: Request, reader: jspb.BinaryReader): Request;
  }

  export namespace Request {
    export type AsObject = {
      readTime?: ReadTime.AsObject,
      startTimer?: StartTimer.AsObject,
      stopTimer?: StopTimer.AsObject,
    }

    export enum RequestCase {
      REQUEST_NOT_SET = 0,
      READ_TIME = 1,
      START_TIMER = 2,
      STOP_TIMER = 3,
    }
  }

  export class Event extends jspb.Message {
    hasSolicitClock(): boolean;
    clearSolicitClock(): void;
    getSolicitClock(): SolicitClock | undefined;
    setSolicitClock(value?: SolicitClock): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      solicitClock?: SolicitClock.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      SOLICIT_CLOCK = 1,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    REQUEST = 1,
    EVENT = 2,
  }
}

export class P2C extends jspb.Message {
  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): P2C.Event | undefined;
  setEvent(value?: P2C.Event): void;

  getPayloadCase(): P2C.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): P2C.AsObject;
  static toObject(includeInstance: boolean, msg: P2C): P2C.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: P2C, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): P2C;
  static deserializeBinaryFromReader(message: P2C, reader: jspb.BinaryReader): P2C;
}

export namespace P2C {
  export type AsObject = {
    event?: P2C.Event.AsObject,
  }

  export class Event extends jspb.Message {
    hasAnnounceClock(): boolean;
    clearAnnounceClock(): void;
    getAnnounceClock(): AnnounceClock | undefined;
    setAnnounceClock(value?: AnnounceClock): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      announceClock?: AnnounceClock.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      ANNOUNCE_CLOCK = 1,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    EVENT = 2,
  }
}

export class ReadTime extends jspb.Message {
  getUtc(): boolean;
  setUtc(value: boolean): void;

  getFormat(): ReadTime.FormatMap[keyof ReadTime.FormatMap];
  setFormat(value: ReadTime.FormatMap[keyof ReadTime.FormatMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReadTime.AsObject;
  static toObject(includeInstance: boolean, msg: ReadTime): ReadTime.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ReadTime, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReadTime;
  static deserializeBinaryFromReader(message: ReadTime, reader: jspb.BinaryReader): ReadTime;
}

export namespace ReadTime {
  export type AsObject = {
    utc: boolean,
    format: ReadTime.FormatMap[keyof ReadTime.FormatMap],
  }

  export interface FormatMap {
    PARTS: 0;
    ISO8601EXT: 1;
  }

  export const Format: FormatMap;
}

export class ReadTimeSuccess extends jspb.Message {
  getUtc(): boolean;
  setUtc(value: boolean): void;

  hasParts(): boolean;
  clearParts(): void;
  getParts(): ReadTimeSuccess.TimeParts | undefined;
  setParts(value?: ReadTimeSuccess.TimeParts): void;

  hasIso8601ext(): boolean;
  clearIso8601ext(): void;
  getIso8601ext(): string;
  setIso8601ext(value: string): void;

  getFormatCase(): ReadTimeSuccess.FormatCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReadTimeSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: ReadTimeSuccess): ReadTimeSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ReadTimeSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReadTimeSuccess;
  static deserializeBinaryFromReader(message: ReadTimeSuccess, reader: jspb.BinaryReader): ReadTimeSuccess;
}

export namespace ReadTimeSuccess {
  export type AsObject = {
    utc: boolean,
    parts?: ReadTimeSuccess.TimeParts.AsObject,
    iso8601ext: string,
  }

  export class TimeParts extends jspb.Message {
    getYear(): number;
    setYear(value: number): void;

    getMonth(): number;
    setMonth(value: number): void;

    getDay(): number;
    setDay(value: number): void;

    getHour(): number;
    setHour(value: number): void;

    getMin(): number;
    setMin(value: number): void;

    getSec(): number;
    setSec(value: number): void;

    getMs(): number;
    setMs(value: number): void;

    getTimezone(): number;
    setTimezone(value: number): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): TimeParts.AsObject;
    static toObject(includeInstance: boolean, msg: TimeParts): TimeParts.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: TimeParts, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): TimeParts;
    static deserializeBinaryFromReader(message: TimeParts, reader: jspb.BinaryReader): TimeParts;
  }

  export namespace TimeParts {
    export type AsObject = {
      year: number,
      month: number,
      day: number,
      hour: number,
      min: number,
      sec: number,
      ms: number,
      timezone: number,
    }
  }

  export enum FormatCase {
    FORMAT_NOT_SET = 0,
    PARTS = 2,
    ISO8601EXT = 3,
  }
}

export class StartTimer extends jspb.Message {
  getDelayMillis(): number;
  setDelayMillis(value: number): void;

  getRepeat(): boolean;
  setRepeat(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StartTimer.AsObject;
  static toObject(includeInstance: boolean, msg: StartTimer): StartTimer.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StartTimer, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StartTimer;
  static deserializeBinaryFromReader(message: StartTimer, reader: jspb.BinaryReader): StartTimer;
}

export namespace StartTimer {
  export type AsObject = {
    delayMillis: number,
    repeat: boolean,
  }
}

export class StartTimerSuccess extends jspb.Message {
  getTimerId(): number;
  setTimerId(value: number): void;

  getFired(): boolean;
  setFired(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StartTimerSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: StartTimerSuccess): StartTimerSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StartTimerSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StartTimerSuccess;
  static deserializeBinaryFromReader(message: StartTimerSuccess, reader: jspb.BinaryReader): StartTimerSuccess;
}

export namespace StartTimerSuccess {
  export type AsObject = {
    timerId: number,
    fired: boolean,
  }
}

export class StopTimer extends jspb.Message {
  getTimerId(): number;
  setTimerId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StopTimer.AsObject;
  static toObject(includeInstance: boolean, msg: StopTimer): StopTimer.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StopTimer, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StopTimer;
  static deserializeBinaryFromReader(message: StopTimer, reader: jspb.BinaryReader): StopTimer;
}

export namespace StopTimer {
  export type AsObject = {
    timerId: number,
  }
}

export class StopTimerSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StopTimerSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: StopTimerSuccess): StopTimerSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StopTimerSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StopTimerSuccess;
  static deserializeBinaryFromReader(message: StopTimerSuccess, reader: jspb.BinaryReader): StopTimerSuccess;
}

export namespace StopTimerSuccess {
  export type AsObject = {
  }
}

export class SolicitClock extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolicitClock.AsObject;
  static toObject(includeInstance: boolean, msg: SolicitClock): SolicitClock.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SolicitClock, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolicitClock;
  static deserializeBinaryFromReader(message: SolicitClock, reader: jspb.BinaryReader): SolicitClock;
}

export namespace SolicitClock {
  export type AsObject = {
  }
}

export class AnnounceClock extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AnnounceClock.AsObject;
  static toObject(includeInstance: boolean, msg: AnnounceClock): AnnounceClock.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AnnounceClock, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AnnounceClock;
  static deserializeBinaryFromReader(message: AnnounceClock, reader: jspb.BinaryReader): AnnounceClock;
}

export namespace AnnounceClock {
  export type AsObject = {
  }
}

