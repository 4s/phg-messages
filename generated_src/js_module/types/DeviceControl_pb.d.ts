// package: s4.messages.interfaces.device_control
// file: DeviceControl.proto

import * as jspb from "google-protobuf";
import * as Device_pb from "./Device_pb";
import * as Error_pb from "./Error_pb";

export class C2P extends jspb.Message {
  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): C2P.Event | undefined;
  setEvent(value?: C2P.Event): void;

  getPayloadCase(): C2P.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): C2P.AsObject;
  static toObject(includeInstance: boolean, msg: C2P): C2P.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: C2P, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): C2P;
  static deserializeBinaryFromReader(message: C2P, reader: jspb.BinaryReader): C2P;
}

export namespace C2P {
  export type AsObject = {
    event?: C2P.Event.AsObject,
  }

  export class Event extends jspb.Message {
    hasSearch(): boolean;
    clearSearch(): void;
    getSearch(): Search | undefined;
    setSearch(value?: Search): void;

    hasStopSearch(): boolean;
    clearStopSearch(): void;
    getStopSearch(): StopSearch | undefined;
    setStopSearch(value?: StopSearch): void;

    hasListConnected(): boolean;
    clearListConnected(): void;
    getListConnected(): ListConnected | undefined;
    setListConnected(value?: ListConnected): void;

    hasSolicitDeviceTransport(): boolean;
    clearSolicitDeviceTransport(): void;
    getSolicitDeviceTransport(): SolicitDeviceTransport | undefined;
    setSolicitDeviceTransport(value?: SolicitDeviceTransport): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      search?: Search.AsObject,
      stopSearch?: StopSearch.AsObject,
      listConnected?: ListConnected.AsObject,
      solicitDeviceTransport?: SolicitDeviceTransport.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      SEARCH = 1,
      STOP_SEARCH = 2,
      LIST_CONNECTED = 3,
      SOLICIT_DEVICE_TRANSPORT = 4,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    EVENT = 2,
  }
}

export class P2C extends jspb.Message {
  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): P2C.Event | undefined;
  setEvent(value?: P2C.Event): void;

  getPayloadCase(): P2C.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): P2C.AsObject;
  static toObject(includeInstance: boolean, msg: P2C): P2C.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: P2C, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): P2C;
  static deserializeBinaryFromReader(message: P2C, reader: jspb.BinaryReader): P2C;
}

export namespace P2C {
  export type AsObject = {
    event?: P2C.Event.AsObject,
  }

  export class Event extends jspb.Message {
    hasSearchInitiated(): boolean;
    clearSearchInitiated(): void;
    getSearchInitiated(): SearchInitiated | undefined;
    setSearchInitiated(value?: SearchInitiated): void;

    hasSearchResult(): boolean;
    clearSearchResult(): void;
    getSearchResult(): SearchResult | undefined;
    setSearchResult(value?: SearchResult): void;

    hasCurrentlyConnected(): boolean;
    clearCurrentlyConnected(): void;
    getCurrentlyConnected(): CurrentlyConnected | undefined;
    setCurrentlyConnected(value?: CurrentlyConnected): void;

    hasConnected(): boolean;
    clearConnected(): void;
    getConnected(): Connected | undefined;
    setConnected(value?: Connected): void;

    hasDisconnected(): boolean;
    clearDisconnected(): void;
    getDisconnected(): Disconnected | undefined;
    setDisconnected(value?: Disconnected): void;

    hasDeviceTransportOrError(): boolean;
    clearDeviceTransportOrError(): void;
    getDeviceTransportOrError(): DeviceTransportOrError | undefined;
    setDeviceTransportOrError(value?: DeviceTransportOrError): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      searchInitiated?: SearchInitiated.AsObject,
      searchResult?: SearchResult.AsObject,
      currentlyConnected?: CurrentlyConnected.AsObject,
      connected?: Connected.AsObject,
      disconnected?: Disconnected.AsObject,
      deviceTransportOrError?: DeviceTransportOrError.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      SEARCH_INITIATED = 1,
      SEARCH_RESULT = 2,
      CURRENTLY_CONNECTED = 3,
      CONNECTED = 4,
      DISCONNECTED = 5,
      DEVICE_TRANSPORT_OR_ERROR = 6,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    EVENT = 2,
  }
}

export class TransportBundle extends jspb.Message {
  getTransportPersistentId(): string;
  setTransportPersistentId(value: string): void;

  hasDeviceTransport(): boolean;
  clearDeviceTransport(): void;
  getDeviceTransport(): Device_pb.DeviceTransport | undefined;
  setDeviceTransport(value?: Device_pb.DeviceTransport): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TransportBundle.AsObject;
  static toObject(includeInstance: boolean, msg: TransportBundle): TransportBundle.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TransportBundle, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TransportBundle;
  static deserializeBinaryFromReader(message: TransportBundle, reader: jspb.BinaryReader): TransportBundle;
}

export namespace TransportBundle {
  export type AsObject = {
    transportPersistentId: string,
    deviceTransport?: Device_pb.DeviceTransport.AsObject,
  }
}

export class Search extends jspb.Message {
  getSearchId(): number;
  setSearchId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Search.AsObject;
  static toObject(includeInstance: boolean, msg: Search): Search.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Search, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Search;
  static deserializeBinaryFromReader(message: Search, reader: jspb.BinaryReader): Search;
}

export namespace Search {
  export type AsObject = {
    searchId: number,
  }
}

export class StopSearch extends jspb.Message {
  getSearchId(): number;
  setSearchId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StopSearch.AsObject;
  static toObject(includeInstance: boolean, msg: StopSearch): StopSearch.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StopSearch, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StopSearch;
  static deserializeBinaryFromReader(message: StopSearch, reader: jspb.BinaryReader): StopSearch;
}

export namespace StopSearch {
  export type AsObject = {
    searchId: number,
  }
}

export class ListConnected extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListConnected.AsObject;
  static toObject(includeInstance: boolean, msg: ListConnected): ListConnected.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListConnected, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListConnected;
  static deserializeBinaryFromReader(message: ListConnected, reader: jspb.BinaryReader): ListConnected;
}

export namespace ListConnected {
  export type AsObject = {
  }
}

export class SearchInitiated extends jspb.Message {
  getSearchId(): number;
  setSearchId(value: number): void;

  clearFoundSoFarList(): void;
  getFoundSoFarList(): Array<TransportBundle>;
  setFoundSoFarList(value: Array<TransportBundle>): void;
  addFoundSoFar(value?: TransportBundle, index?: number): TransportBundle;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SearchInitiated.AsObject;
  static toObject(includeInstance: boolean, msg: SearchInitiated): SearchInitiated.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SearchInitiated, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SearchInitiated;
  static deserializeBinaryFromReader(message: SearchInitiated, reader: jspb.BinaryReader): SearchInitiated;
}

export namespace SearchInitiated {
  export type AsObject = {
    searchId: number,
    foundSoFarList: Array<TransportBundle.AsObject>,
  }
}

export class CurrentlyConnected extends jspb.Message {
  clearConnectedListList(): void;
  getConnectedListList(): Array<TransportBundle>;
  setConnectedListList(value: Array<TransportBundle>): void;
  addConnectedList(value?: TransportBundle, index?: number): TransportBundle;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CurrentlyConnected.AsObject;
  static toObject(includeInstance: boolean, msg: CurrentlyConnected): CurrentlyConnected.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CurrentlyConnected, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CurrentlyConnected;
  static deserializeBinaryFromReader(message: CurrentlyConnected, reader: jspb.BinaryReader): CurrentlyConnected;
}

export namespace CurrentlyConnected {
  export type AsObject = {
    connectedListList: Array<TransportBundle.AsObject>,
  }
}

export class SearchResult extends jspb.Message {
  getSearchId(): number;
  setSearchId(value: number): void;

  hasResult(): boolean;
  clearResult(): void;
  getResult(): TransportBundle | undefined;
  setResult(value?: TransportBundle): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SearchResult.AsObject;
  static toObject(includeInstance: boolean, msg: SearchResult): SearchResult.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SearchResult, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SearchResult;
  static deserializeBinaryFromReader(message: SearchResult, reader: jspb.BinaryReader): SearchResult;
}

export namespace SearchResult {
  export type AsObject = {
    searchId: number,
    result?: TransportBundle.AsObject,
  }
}

export class Connected extends jspb.Message {
  hasSubject(): boolean;
  clearSubject(): void;
  getSubject(): TransportBundle | undefined;
  setSubject(value?: TransportBundle): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Connected.AsObject;
  static toObject(includeInstance: boolean, msg: Connected): Connected.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Connected, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Connected;
  static deserializeBinaryFromReader(message: Connected, reader: jspb.BinaryReader): Connected;
}

export namespace Connected {
  export type AsObject = {
    subject?: TransportBundle.AsObject,
  }
}

export class Disconnected extends jspb.Message {
  hasSubject(): boolean;
  clearSubject(): void;
  getSubject(): TransportBundle | undefined;
  setSubject(value?: TransportBundle): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Disconnected.AsObject;
  static toObject(includeInstance: boolean, msg: Disconnected): Disconnected.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Disconnected, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Disconnected;
  static deserializeBinaryFromReader(message: Disconnected, reader: jspb.BinaryReader): Disconnected;
}

export namespace Disconnected {
  export type AsObject = {
    subject?: TransportBundle.AsObject,
  }
}

export class SolicitDeviceTransport extends jspb.Message {
  hasTransportPersistentId(): boolean;
  clearTransportPersistentId(): void;
  getTransportPersistentId(): string;
  setTransportPersistentId(value: string): void;

  hasHandle(): boolean;
  clearHandle(): void;
  getHandle(): number;
  setHandle(value: number): void;

  hasDeviceTransport(): boolean;
  clearDeviceTransport(): void;
  getDeviceTransport(): Device_pb.DeviceTransport | undefined;
  setDeviceTransport(value?: Device_pb.DeviceTransport): void;

  getIdTypeCase(): SolicitDeviceTransport.IdTypeCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolicitDeviceTransport.AsObject;
  static toObject(includeInstance: boolean, msg: SolicitDeviceTransport): SolicitDeviceTransport.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SolicitDeviceTransport, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolicitDeviceTransport;
  static deserializeBinaryFromReader(message: SolicitDeviceTransport, reader: jspb.BinaryReader): SolicitDeviceTransport;
}

export namespace SolicitDeviceTransport {
  export type AsObject = {
    transportPersistentId: string,
    handle: number,
    deviceTransport?: Device_pb.DeviceTransport.AsObject,
  }

  export enum IdTypeCase {
    ID_TYPE_NOT_SET = 0,
    TRANSPORT_PERSISTENT_ID = 1,
    HANDLE = 2,
  }
}

export class DeviceTransportOrError extends jspb.Message {
  hasSubject(): boolean;
  clearSubject(): void;
  getSubject(): TransportBundle | undefined;
  setSubject(value?: TransportBundle): void;

  hasError(): boolean;
  clearError(): void;
  getError(): Error_pb.Error | undefined;
  setError(value?: Error_pb.Error): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeviceTransportOrError.AsObject;
  static toObject(includeInstance: boolean, msg: DeviceTransportOrError): DeviceTransportOrError.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeviceTransportOrError, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeviceTransportOrError;
  static deserializeBinaryFromReader(message: DeviceTransportOrError, reader: jspb.BinaryReader): DeviceTransportOrError;
}

export namespace DeviceTransportOrError {
  export type AsObject = {
    subject?: TransportBundle.AsObject,
    error?: Error_pb.Error.AsObject,
  }
}

