// package: s4.messages.interfaces.gatt
// file: GATT.proto

import * as jspb from "google-protobuf";

export class C2P extends jspb.Message {
  hasRequest(): boolean;
  clearRequest(): void;
  getRequest(): C2P.Request | undefined;
  setRequest(value?: C2P.Request): void;

  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): C2P.Event | undefined;
  setEvent(value?: C2P.Event): void;

  getPayloadCase(): C2P.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): C2P.AsObject;
  static toObject(includeInstance: boolean, msg: C2P): C2P.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: C2P, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): C2P;
  static deserializeBinaryFromReader(message: C2P, reader: jspb.BinaryReader): C2P;
}

export namespace C2P {
  export type AsObject = {
    request?: C2P.Request.AsObject,
    event?: C2P.Event.AsObject,
  }

  export class Request extends jspb.Message {
    hasConnect(): boolean;
    clearConnect(): void;
    getConnect(): Connect | undefined;
    setConnect(value?: Connect): void;

    hasDiscoverServices(): boolean;
    clearDiscoverServices(): void;
    getDiscoverServices(): DiscoverServices | undefined;
    setDiscoverServices(value?: DiscoverServices): void;

    hasFindIncludedServices(): boolean;
    clearFindIncludedServices(): void;
    getFindIncludedServices(): FindIncludedServices | undefined;
    setFindIncludedServices(value?: FindIncludedServices): void;

    hasDiscoverCharacteristics(): boolean;
    clearDiscoverCharacteristics(): void;
    getDiscoverCharacteristics(): DiscoverCharacteristics | undefined;
    setDiscoverCharacteristics(value?: DiscoverCharacteristics): void;

    hasDiscoverDescriptors(): boolean;
    clearDiscoverDescriptors(): void;
    getDiscoverDescriptors(): DiscoverDescriptors | undefined;
    setDiscoverDescriptors(value?: DiscoverDescriptors): void;

    hasReadCharacteristic(): boolean;
    clearReadCharacteristic(): void;
    getReadCharacteristic(): ReadCharacteristic | undefined;
    setReadCharacteristic(value?: ReadCharacteristic): void;

    hasWriteCharacteristic(): boolean;
    clearWriteCharacteristic(): void;
    getWriteCharacteristic(): WriteCharacteristic | undefined;
    setWriteCharacteristic(value?: WriteCharacteristic): void;

    hasWriteCharacteristicWithoutResponse(): boolean;
    clearWriteCharacteristicWithoutResponse(): void;
    getWriteCharacteristicWithoutResponse(): WriteCharacteristicWithoutResponse | undefined;
    setWriteCharacteristicWithoutResponse(value?: WriteCharacteristicWithoutResponse): void;

    hasReadDescriptor(): boolean;
    clearReadDescriptor(): void;
    getReadDescriptor(): ReadDescriptor | undefined;
    setReadDescriptor(value?: ReadDescriptor): void;

    hasWriteDescriptor(): boolean;
    clearWriteDescriptor(): void;
    getWriteDescriptor(): WriteDescriptor | undefined;
    setWriteDescriptor(value?: WriteDescriptor): void;

    hasSetNotification(): boolean;
    clearSetNotification(): void;
    getSetNotification(): SetNotification | undefined;
    setSetNotification(value?: SetNotification): void;

    getRequestCase(): Request.RequestCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Request.AsObject;
    static toObject(includeInstance: boolean, msg: Request): Request.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Request, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Request;
    static deserializeBinaryFromReader(message: Request, reader: jspb.BinaryReader): Request;
  }

  export namespace Request {
    export type AsObject = {
      connect?: Connect.AsObject,
      discoverServices?: DiscoverServices.AsObject,
      findIncludedServices?: FindIncludedServices.AsObject,
      discoverCharacteristics?: DiscoverCharacteristics.AsObject,
      discoverDescriptors?: DiscoverDescriptors.AsObject,
      readCharacteristic?: ReadCharacteristic.AsObject,
      writeCharacteristic?: WriteCharacteristic.AsObject,
      writeCharacteristicWithoutResponse?: WriteCharacteristicWithoutResponse.AsObject,
      readDescriptor?: ReadDescriptor.AsObject,
      writeDescriptor?: WriteDescriptor.AsObject,
      setNotification?: SetNotification.AsObject,
    }

    export enum RequestCase {
      REQUEST_NOT_SET = 0,
      CONNECT = 1,
      DISCOVER_SERVICES = 2,
      FIND_INCLUDED_SERVICES = 3,
      DISCOVER_CHARACTERISTICS = 4,
      DISCOVER_DESCRIPTORS = 5,
      READ_CHARACTERISTIC = 6,
      WRITE_CHARACTERISTIC = 7,
      WRITE_CHARACTERISTIC_WITHOUT_RESPONSE = 8,
      READ_DESCRIPTOR = 9,
      WRITE_DESCRIPTOR = 10,
      SET_NOTIFICATION = 11,
    }
  }

  export class Event extends jspb.Message {
    hasDisconnect(): boolean;
    clearDisconnect(): void;
    getDisconnect(): Disconnect | undefined;
    setDisconnect(value?: Disconnect): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      disconnect?: Disconnect.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      DISCONNECT = 1,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    REQUEST = 1,
    EVENT = 2,
  }
}

export class P2C extends jspb.Message {
  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): P2C.Event | undefined;
  setEvent(value?: P2C.Event): void;

  getPayloadCase(): P2C.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): P2C.AsObject;
  static toObject(includeInstance: boolean, msg: P2C): P2C.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: P2C, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): P2C;
  static deserializeBinaryFromReader(message: P2C, reader: jspb.BinaryReader): P2C;
}

export namespace P2C {
  export type AsObject = {
    event?: P2C.Event.AsObject,
  }

  export class Event extends jspb.Message {
    hasDisconnected(): boolean;
    clearDisconnected(): void;
    getDisconnected(): Disconnected | undefined;
    setDisconnected(value?: Disconnected): void;

    hasNotification(): boolean;
    clearNotification(): void;
    getNotification(): Notification | undefined;
    setNotification(value?: Notification): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      disconnected?: Disconnected.AsObject,
      notification?: Notification.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      DISCONNECTED = 1,
      NOTIFICATION = 2,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    EVENT = 2,
  }
}

export class BTUUID extends jspb.Message {
  hasShort(): boolean;
  clearShort(): void;
  getShort(): number;
  setShort(value: number): void;

  hasLong(): boolean;
  clearLong(): void;
  getLong(): Uint8Array | string;
  getLong_asU8(): Uint8Array;
  getLong_asB64(): string;
  setLong(value: Uint8Array | string): void;

  getSizeCase(): BTUUID.SizeCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BTUUID.AsObject;
  static toObject(includeInstance: boolean, msg: BTUUID): BTUUID.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: BTUUID, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BTUUID;
  static deserializeBinaryFromReader(message: BTUUID, reader: jspb.BinaryReader): BTUUID;
}

export namespace BTUUID {
  export type AsObject = {
    pb_short: number,
    long: Uint8Array | string,
  }

  export enum SizeCase {
    SIZE_NOT_SET = 0,
    SHORT = 1,
    LONG = 2,
  }
}

export class GattService extends jspb.Message {
  getHandle(): number;
  setHandle(value: number): void;

  hasUuid(): boolean;
  clearUuid(): void;
  getUuid(): BTUUID | undefined;
  setUuid(value?: BTUUID): void;

  getPrimary(): boolean;
  setPrimary(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GattService.AsObject;
  static toObject(includeInstance: boolean, msg: GattService): GattService.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GattService, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GattService;
  static deserializeBinaryFromReader(message: GattService, reader: jspb.BinaryReader): GattService;
}

export namespace GattService {
  export type AsObject = {
    handle: number,
    uuid?: BTUUID.AsObject,
    primary: boolean,
  }
}

export class GattCharacteristic extends jspb.Message {
  getHandle(): number;
  setHandle(value: number): void;

  hasUuid(): boolean;
  clearUuid(): void;
  getUuid(): BTUUID | undefined;
  setUuid(value?: BTUUID): void;

  getBroadcast(): boolean;
  setBroadcast(value: boolean): void;

  getRead(): boolean;
  setRead(value: boolean): void;

  getWriteWithoutResponse(): boolean;
  setWriteWithoutResponse(value: boolean): void;

  getWrite(): boolean;
  setWrite(value: boolean): void;

  getNotify(): boolean;
  setNotify(value: boolean): void;

  getIndicate(): boolean;
  setIndicate(value: boolean): void;

  getAuthenticatedSignedWrite(): boolean;
  setAuthenticatedSignedWrite(value: boolean): void;

  getExtendedProperties(): boolean;
  setExtendedProperties(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GattCharacteristic.AsObject;
  static toObject(includeInstance: boolean, msg: GattCharacteristic): GattCharacteristic.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GattCharacteristic, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GattCharacteristic;
  static deserializeBinaryFromReader(message: GattCharacteristic, reader: jspb.BinaryReader): GattCharacteristic;
}

export namespace GattCharacteristic {
  export type AsObject = {
    handle: number,
    uuid?: BTUUID.AsObject,
    broadcast: boolean,
    read: boolean,
    writeWithoutResponse: boolean,
    write: boolean,
    notify: boolean,
    indicate: boolean,
    authenticatedSignedWrite: boolean,
    extendedProperties: boolean,
  }
}

export class GattDescriptor extends jspb.Message {
  getHandle(): number;
  setHandle(value: number): void;

  getType(): GattDescriptor.TypeMap[keyof GattDescriptor.TypeMap];
  setType(value: GattDescriptor.TypeMap[keyof GattDescriptor.TypeMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GattDescriptor.AsObject;
  static toObject(includeInstance: boolean, msg: GattDescriptor): GattDescriptor.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GattDescriptor, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GattDescriptor;
  static deserializeBinaryFromReader(message: GattDescriptor, reader: jspb.BinaryReader): GattDescriptor;
}

export namespace GattDescriptor {
  export type AsObject = {
    handle: number,
    type: GattDescriptor.TypeMap[keyof GattDescriptor.TypeMap],
  }

  export interface TypeMap {
    CHARACTERISTIC_EXTENDED_PROPERTIES: 0;
    CHARACTERISTIC_USER_DESCRIPTION: 1;
    CLIENT_CHARACTERISTIC_CONFIGURATION: 2;
    SERVER_CHARACTERISTIC_CONFIGURATION: 3;
    CHARACTERISTIC_PRESENTATION_FORMAT: 4;
    CHARACTERISTIC_AGGREGATE_FORMAT: 5;
    USER_DEFINED_DESCRIPTOR: 6;
  }

  export const Type: TypeMap;
}

export class GattDescriptorValue extends jspb.Message {
  hasCharacteristicExtendedProperties(): boolean;
  clearCharacteristicExtendedProperties(): void;
  getCharacteristicExtendedProperties(): GattDescriptorValue.CharacteristicExtendedProperties | undefined;
  setCharacteristicExtendedProperties(value?: GattDescriptorValue.CharacteristicExtendedProperties): void;

  hasCharacteristicUserDescription(): boolean;
  clearCharacteristicUserDescription(): void;
  getCharacteristicUserDescription(): GattDescriptorValue.CharacteristicUserDescription | undefined;
  setCharacteristicUserDescription(value?: GattDescriptorValue.CharacteristicUserDescription): void;

  hasClientCharacteristicConfiguration(): boolean;
  clearClientCharacteristicConfiguration(): void;
  getClientCharacteristicConfiguration(): GattDescriptorValue.ClientCharacteristicConfiguration | undefined;
  setClientCharacteristicConfiguration(value?: GattDescriptorValue.ClientCharacteristicConfiguration): void;

  hasServerCharacteristicConfiguration(): boolean;
  clearServerCharacteristicConfiguration(): void;
  getServerCharacteristicConfiguration(): GattDescriptorValue.ServerCharacteristicConfiguration | undefined;
  setServerCharacteristicConfiguration(value?: GattDescriptorValue.ServerCharacteristicConfiguration): void;

  hasCharacteristicPresentationFormat(): boolean;
  clearCharacteristicPresentationFormat(): void;
  getCharacteristicPresentationFormat(): GattDescriptorValue.CharacteristicPresentationFormat | undefined;
  setCharacteristicPresentationFormat(value?: GattDescriptorValue.CharacteristicPresentationFormat): void;

  hasCharacteristicAggregateFormat(): boolean;
  clearCharacteristicAggregateFormat(): void;
  getCharacteristicAggregateFormat(): GattDescriptorValue.CharacteristicAggregateFormat | undefined;
  setCharacteristicAggregateFormat(value?: GattDescriptorValue.CharacteristicAggregateFormat): void;

  hasUserDefinedDescriptor(): boolean;
  clearUserDefinedDescriptor(): void;
  getUserDefinedDescriptor(): GattDescriptorValue.UserDefinedDescriptor | undefined;
  setUserDefinedDescriptor(value?: GattDescriptorValue.UserDefinedDescriptor): void;

  getTypeCase(): GattDescriptorValue.TypeCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GattDescriptorValue.AsObject;
  static toObject(includeInstance: boolean, msg: GattDescriptorValue): GattDescriptorValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GattDescriptorValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GattDescriptorValue;
  static deserializeBinaryFromReader(message: GattDescriptorValue, reader: jspb.BinaryReader): GattDescriptorValue;
}

export namespace GattDescriptorValue {
  export type AsObject = {
    characteristicExtendedProperties?: GattDescriptorValue.CharacteristicExtendedProperties.AsObject,
    characteristicUserDescription?: GattDescriptorValue.CharacteristicUserDescription.AsObject,
    clientCharacteristicConfiguration?: GattDescriptorValue.ClientCharacteristicConfiguration.AsObject,
    serverCharacteristicConfiguration?: GattDescriptorValue.ServerCharacteristicConfiguration.AsObject,
    characteristicPresentationFormat?: GattDescriptorValue.CharacteristicPresentationFormat.AsObject,
    characteristicAggregateFormat?: GattDescriptorValue.CharacteristicAggregateFormat.AsObject,
    userDefinedDescriptor?: GattDescriptorValue.UserDefinedDescriptor.AsObject,
  }

  export class CharacteristicExtendedProperties extends jspb.Message {
    getReliableWrite(): boolean;
    setReliableWrite(value: boolean): void;

    getWritableAuxiliaries(): boolean;
    setWritableAuxiliaries(value: boolean): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): CharacteristicExtendedProperties.AsObject;
    static toObject(includeInstance: boolean, msg: CharacteristicExtendedProperties): CharacteristicExtendedProperties.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: CharacteristicExtendedProperties, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): CharacteristicExtendedProperties;
    static deserializeBinaryFromReader(message: CharacteristicExtendedProperties, reader: jspb.BinaryReader): CharacteristicExtendedProperties;
  }

  export namespace CharacteristicExtendedProperties {
    export type AsObject = {
      reliableWrite: boolean,
      writableAuxiliaries: boolean,
    }
  }

  export class CharacteristicUserDescription extends jspb.Message {
    getUserDescription(): string;
    setUserDescription(value: string): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): CharacteristicUserDescription.AsObject;
    static toObject(includeInstance: boolean, msg: CharacteristicUserDescription): CharacteristicUserDescription.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: CharacteristicUserDescription, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): CharacteristicUserDescription;
    static deserializeBinaryFromReader(message: CharacteristicUserDescription, reader: jspb.BinaryReader): CharacteristicUserDescription;
  }

  export namespace CharacteristicUserDescription {
    export type AsObject = {
      userDescription: string,
    }
  }

  export class ClientCharacteristicConfiguration extends jspb.Message {
    getNotification(): boolean;
    setNotification(value: boolean): void;

    getIndication(): boolean;
    setIndication(value: boolean): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ClientCharacteristicConfiguration.AsObject;
    static toObject(includeInstance: boolean, msg: ClientCharacteristicConfiguration): ClientCharacteristicConfiguration.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ClientCharacteristicConfiguration, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ClientCharacteristicConfiguration;
    static deserializeBinaryFromReader(message: ClientCharacteristicConfiguration, reader: jspb.BinaryReader): ClientCharacteristicConfiguration;
  }

  export namespace ClientCharacteristicConfiguration {
    export type AsObject = {
      notification: boolean,
      indication: boolean,
    }
  }

  export class ServerCharacteristicConfiguration extends jspb.Message {
    getBroadcast(): boolean;
    setBroadcast(value: boolean): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ServerCharacteristicConfiguration.AsObject;
    static toObject(includeInstance: boolean, msg: ServerCharacteristicConfiguration): ServerCharacteristicConfiguration.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ServerCharacteristicConfiguration, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ServerCharacteristicConfiguration;
    static deserializeBinaryFromReader(message: ServerCharacteristicConfiguration, reader: jspb.BinaryReader): ServerCharacteristicConfiguration;
  }

  export namespace ServerCharacteristicConfiguration {
    export type AsObject = {
      broadcast: boolean,
    }
  }

  export class CharacteristicPresentationFormat extends jspb.Message {
    getFormat(): Uint8Array | string;
    getFormat_asU8(): Uint8Array;
    getFormat_asB64(): string;
    setFormat(value: Uint8Array | string): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): CharacteristicPresentationFormat.AsObject;
    static toObject(includeInstance: boolean, msg: CharacteristicPresentationFormat): CharacteristicPresentationFormat.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: CharacteristicPresentationFormat, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): CharacteristicPresentationFormat;
    static deserializeBinaryFromReader(message: CharacteristicPresentationFormat, reader: jspb.BinaryReader): CharacteristicPresentationFormat;
  }

  export namespace CharacteristicPresentationFormat {
    export type AsObject = {
      format: Uint8Array | string,
    }
  }

  export class CharacteristicAggregateFormat extends jspb.Message {
    clearDescriptorsList(): void;
    getDescriptorsList(): Array<number>;
    setDescriptorsList(value: Array<number>): void;
    addDescriptors(value: number, index?: number): number;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): CharacteristicAggregateFormat.AsObject;
    static toObject(includeInstance: boolean, msg: CharacteristicAggregateFormat): CharacteristicAggregateFormat.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: CharacteristicAggregateFormat, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): CharacteristicAggregateFormat;
    static deserializeBinaryFromReader(message: CharacteristicAggregateFormat, reader: jspb.BinaryReader): CharacteristicAggregateFormat;
  }

  export namespace CharacteristicAggregateFormat {
    export type AsObject = {
      descriptorsList: Array<number>,
    }
  }

  export class UserDefinedDescriptor extends jspb.Message {
    hasType(): boolean;
    clearType(): void;
    getType(): BTUUID | undefined;
    setType(value?: BTUUID): void;

    getValue(): Uint8Array | string;
    getValue_asU8(): Uint8Array;
    getValue_asB64(): string;
    setValue(value: Uint8Array | string): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): UserDefinedDescriptor.AsObject;
    static toObject(includeInstance: boolean, msg: UserDefinedDescriptor): UserDefinedDescriptor.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: UserDefinedDescriptor, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): UserDefinedDescriptor;
    static deserializeBinaryFromReader(message: UserDefinedDescriptor, reader: jspb.BinaryReader): UserDefinedDescriptor;
  }

  export namespace UserDefinedDescriptor {
    export type AsObject = {
      type?: BTUUID.AsObject,
      value: Uint8Array | string,
    }
  }

  export enum TypeCase {
    TYPE_NOT_SET = 0,
    CHARACTERISTIC_EXTENDED_PROPERTIES = 1,
    CHARACTERISTIC_USER_DESCRIPTION = 2,
    CLIENT_CHARACTERISTIC_CONFIGURATION = 3,
    SERVER_CHARACTERISTIC_CONFIGURATION = 4,
    CHARACTERISTIC_PRESENTATION_FORMAT = 5,
    CHARACTERISTIC_AGGREGATE_FORMAT = 6,
    USER_DEFINED_DESCRIPTOR = 7,
  }
}

export class Connect extends jspb.Message {
  getGattHandle(): number;
  setGattHandle(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Connect.AsObject;
  static toObject(includeInstance: boolean, msg: Connect): Connect.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Connect, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Connect;
  static deserializeBinaryFromReader(message: Connect, reader: jspb.BinaryReader): Connect;
}

export namespace Connect {
  export type AsObject = {
    gattHandle: number,
  }
}

export class ConnectSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ConnectSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: ConnectSuccess): ConnectSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ConnectSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ConnectSuccess;
  static deserializeBinaryFromReader(message: ConnectSuccess, reader: jspb.BinaryReader): ConnectSuccess;
}

export namespace ConnectSuccess {
  export type AsObject = {
  }
}

export class Disconnect extends jspb.Message {
  getGattHandle(): number;
  setGattHandle(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Disconnect.AsObject;
  static toObject(includeInstance: boolean, msg: Disconnect): Disconnect.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Disconnect, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Disconnect;
  static deserializeBinaryFromReader(message: Disconnect, reader: jspb.BinaryReader): Disconnect;
}

export namespace Disconnect {
  export type AsObject = {
    gattHandle: number,
  }
}

export class Disconnected extends jspb.Message {
  getGattHandle(): number;
  setGattHandle(value: number): void;

  getReason(): Disconnected.ReasonMap[keyof Disconnected.ReasonMap];
  setReason(value: Disconnected.ReasonMap[keyof Disconnected.ReasonMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Disconnected.AsObject;
  static toObject(includeInstance: boolean, msg: Disconnected): Disconnected.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Disconnected, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Disconnected;
  static deserializeBinaryFromReader(message: Disconnected, reader: jspb.BinaryReader): Disconnected;
}

export namespace Disconnected {
  export type AsObject = {
    gattHandle: number,
    reason: Disconnected.ReasonMap[keyof Disconnected.ReasonMap],
  }

  export interface ReasonMap {
    MY_DISCONNECT: 0;
    OUT_OF_RANGE: 1;
    PEER_DISCONNECT: 2;
    TIMEOUT: 3;
    IO_ERROR: 4;
  }

  export const Reason: ReasonMap;
}

export class DiscoverServices extends jspb.Message {
  getGattHandle(): number;
  setGattHandle(value: number): void;

  hasUuid(): boolean;
  clearUuid(): void;
  getUuid(): BTUUID | undefined;
  setUuid(value?: BTUUID): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DiscoverServices.AsObject;
  static toObject(includeInstance: boolean, msg: DiscoverServices): DiscoverServices.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DiscoverServices, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DiscoverServices;
  static deserializeBinaryFromReader(message: DiscoverServices, reader: jspb.BinaryReader): DiscoverServices;
}

export namespace DiscoverServices {
  export type AsObject = {
    gattHandle: number,
    uuid?: BTUUID.AsObject,
  }
}

export class DiscoverServicesSuccess extends jspb.Message {
  clearServicesList(): void;
  getServicesList(): Array<GattService>;
  setServicesList(value: Array<GattService>): void;
  addServices(value?: GattService, index?: number): GattService;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DiscoverServicesSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: DiscoverServicesSuccess): DiscoverServicesSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DiscoverServicesSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DiscoverServicesSuccess;
  static deserializeBinaryFromReader(message: DiscoverServicesSuccess, reader: jspb.BinaryReader): DiscoverServicesSuccess;
}

export namespace DiscoverServicesSuccess {
  export type AsObject = {
    servicesList: Array<GattService.AsObject>,
  }
}

export class FindIncludedServices extends jspb.Message {
  getService(): number;
  setService(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FindIncludedServices.AsObject;
  static toObject(includeInstance: boolean, msg: FindIncludedServices): FindIncludedServices.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FindIncludedServices, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FindIncludedServices;
  static deserializeBinaryFromReader(message: FindIncludedServices, reader: jspb.BinaryReader): FindIncludedServices;
}

export namespace FindIncludedServices {
  export type AsObject = {
    service: number,
  }
}

export class FindIncludedServicesSuccess extends jspb.Message {
  clearServicesList(): void;
  getServicesList(): Array<GattService>;
  setServicesList(value: Array<GattService>): void;
  addServices(value?: GattService, index?: number): GattService;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FindIncludedServicesSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: FindIncludedServicesSuccess): FindIncludedServicesSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FindIncludedServicesSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FindIncludedServicesSuccess;
  static deserializeBinaryFromReader(message: FindIncludedServicesSuccess, reader: jspb.BinaryReader): FindIncludedServicesSuccess;
}

export namespace FindIncludedServicesSuccess {
  export type AsObject = {
    servicesList: Array<GattService.AsObject>,
  }
}

export class DiscoverCharacteristics extends jspb.Message {
  getService(): number;
  setService(value: number): void;

  hasUuid(): boolean;
  clearUuid(): void;
  getUuid(): BTUUID | undefined;
  setUuid(value?: BTUUID): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DiscoverCharacteristics.AsObject;
  static toObject(includeInstance: boolean, msg: DiscoverCharacteristics): DiscoverCharacteristics.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DiscoverCharacteristics, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DiscoverCharacteristics;
  static deserializeBinaryFromReader(message: DiscoverCharacteristics, reader: jspb.BinaryReader): DiscoverCharacteristics;
}

export namespace DiscoverCharacteristics {
  export type AsObject = {
    service: number,
    uuid?: BTUUID.AsObject,
  }
}

export class DiscoverCharacteristicsSuccess extends jspb.Message {
  clearCharacteristicsList(): void;
  getCharacteristicsList(): Array<GattCharacteristic>;
  setCharacteristicsList(value: Array<GattCharacteristic>): void;
  addCharacteristics(value?: GattCharacteristic, index?: number): GattCharacteristic;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DiscoverCharacteristicsSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: DiscoverCharacteristicsSuccess): DiscoverCharacteristicsSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DiscoverCharacteristicsSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DiscoverCharacteristicsSuccess;
  static deserializeBinaryFromReader(message: DiscoverCharacteristicsSuccess, reader: jspb.BinaryReader): DiscoverCharacteristicsSuccess;
}

export namespace DiscoverCharacteristicsSuccess {
  export type AsObject = {
    characteristicsList: Array<GattCharacteristic.AsObject>,
  }
}

export class DiscoverDescriptors extends jspb.Message {
  getCharacteristic(): number;
  setCharacteristic(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DiscoverDescriptors.AsObject;
  static toObject(includeInstance: boolean, msg: DiscoverDescriptors): DiscoverDescriptors.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DiscoverDescriptors, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DiscoverDescriptors;
  static deserializeBinaryFromReader(message: DiscoverDescriptors, reader: jspb.BinaryReader): DiscoverDescriptors;
}

export namespace DiscoverDescriptors {
  export type AsObject = {
    characteristic: number,
  }
}

export class DiscoverDescriptorsSuccess extends jspb.Message {
  clearDescriptorsList(): void;
  getDescriptorsList(): Array<GattDescriptor>;
  setDescriptorsList(value: Array<GattDescriptor>): void;
  addDescriptors(value?: GattDescriptor, index?: number): GattDescriptor;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DiscoverDescriptorsSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: DiscoverDescriptorsSuccess): DiscoverDescriptorsSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DiscoverDescriptorsSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DiscoverDescriptorsSuccess;
  static deserializeBinaryFromReader(message: DiscoverDescriptorsSuccess, reader: jspb.BinaryReader): DiscoverDescriptorsSuccess;
}

export namespace DiscoverDescriptorsSuccess {
  export type AsObject = {
    descriptorsList: Array<GattDescriptor.AsObject>,
  }
}

export class ReadCharacteristic extends jspb.Message {
  getCharacteristic(): number;
  setCharacteristic(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReadCharacteristic.AsObject;
  static toObject(includeInstance: boolean, msg: ReadCharacteristic): ReadCharacteristic.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ReadCharacteristic, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReadCharacteristic;
  static deserializeBinaryFromReader(message: ReadCharacteristic, reader: jspb.BinaryReader): ReadCharacteristic;
}

export namespace ReadCharacteristic {
  export type AsObject = {
    characteristic: number,
  }
}

export class ReadCharacteristicSuccess extends jspb.Message {
  getValue(): Uint8Array | string;
  getValue_asU8(): Uint8Array;
  getValue_asB64(): string;
  setValue(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReadCharacteristicSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: ReadCharacteristicSuccess): ReadCharacteristicSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ReadCharacteristicSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReadCharacteristicSuccess;
  static deserializeBinaryFromReader(message: ReadCharacteristicSuccess, reader: jspb.BinaryReader): ReadCharacteristicSuccess;
}

export namespace ReadCharacteristicSuccess {
  export type AsObject = {
    value: Uint8Array | string,
  }
}

export class WriteCharacteristic extends jspb.Message {
  getCharacteristic(): number;
  setCharacteristic(value: number): void;

  getValue(): Uint8Array | string;
  getValue_asU8(): Uint8Array;
  getValue_asB64(): string;
  setValue(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WriteCharacteristic.AsObject;
  static toObject(includeInstance: boolean, msg: WriteCharacteristic): WriteCharacteristic.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: WriteCharacteristic, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WriteCharacteristic;
  static deserializeBinaryFromReader(message: WriteCharacteristic, reader: jspb.BinaryReader): WriteCharacteristic;
}

export namespace WriteCharacteristic {
  export type AsObject = {
    characteristic: number,
    value: Uint8Array | string,
  }
}

export class WriteCharacteristicSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WriteCharacteristicSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: WriteCharacteristicSuccess): WriteCharacteristicSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: WriteCharacteristicSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WriteCharacteristicSuccess;
  static deserializeBinaryFromReader(message: WriteCharacteristicSuccess, reader: jspb.BinaryReader): WriteCharacteristicSuccess;
}

export namespace WriteCharacteristicSuccess {
  export type AsObject = {
  }
}

export class WriteCharacteristicWithoutResponse extends jspb.Message {
  getCharacteristic(): number;
  setCharacteristic(value: number): void;

  getValue(): Uint8Array | string;
  getValue_asU8(): Uint8Array;
  getValue_asB64(): string;
  setValue(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WriteCharacteristicWithoutResponse.AsObject;
  static toObject(includeInstance: boolean, msg: WriteCharacteristicWithoutResponse): WriteCharacteristicWithoutResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: WriteCharacteristicWithoutResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WriteCharacteristicWithoutResponse;
  static deserializeBinaryFromReader(message: WriteCharacteristicWithoutResponse, reader: jspb.BinaryReader): WriteCharacteristicWithoutResponse;
}

export namespace WriteCharacteristicWithoutResponse {
  export type AsObject = {
    characteristic: number,
    value: Uint8Array | string,
  }
}

export class WriteCharacteristicWithoutResponseSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WriteCharacteristicWithoutResponseSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: WriteCharacteristicWithoutResponseSuccess): WriteCharacteristicWithoutResponseSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: WriteCharacteristicWithoutResponseSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WriteCharacteristicWithoutResponseSuccess;
  static deserializeBinaryFromReader(message: WriteCharacteristicWithoutResponseSuccess, reader: jspb.BinaryReader): WriteCharacteristicWithoutResponseSuccess;
}

export namespace WriteCharacteristicWithoutResponseSuccess {
  export type AsObject = {
  }
}

export class Notification extends jspb.Message {
  getCharacteristic(): number;
  setCharacteristic(value: number): void;

  getValue(): Uint8Array | string;
  getValue_asU8(): Uint8Array;
  getValue_asB64(): string;
  setValue(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Notification.AsObject;
  static toObject(includeInstance: boolean, msg: Notification): Notification.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Notification, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Notification;
  static deserializeBinaryFromReader(message: Notification, reader: jspb.BinaryReader): Notification;
}

export namespace Notification {
  export type AsObject = {
    characteristic: number,
    value: Uint8Array | string,
  }
}

export class ReadDescriptor extends jspb.Message {
  getDescr(): number;
  setDescr(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReadDescriptor.AsObject;
  static toObject(includeInstance: boolean, msg: ReadDescriptor): ReadDescriptor.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ReadDescriptor, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReadDescriptor;
  static deserializeBinaryFromReader(message: ReadDescriptor, reader: jspb.BinaryReader): ReadDescriptor;
}

export namespace ReadDescriptor {
  export type AsObject = {
    descr: number,
  }
}

export class ReadDescriptorSuccess extends jspb.Message {
  hasValue(): boolean;
  clearValue(): void;
  getValue(): GattDescriptorValue | undefined;
  setValue(value?: GattDescriptorValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReadDescriptorSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: ReadDescriptorSuccess): ReadDescriptorSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ReadDescriptorSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReadDescriptorSuccess;
  static deserializeBinaryFromReader(message: ReadDescriptorSuccess, reader: jspb.BinaryReader): ReadDescriptorSuccess;
}

export namespace ReadDescriptorSuccess {
  export type AsObject = {
    value?: GattDescriptorValue.AsObject,
  }
}

export class WriteDescriptor extends jspb.Message {
  getDescr(): number;
  setDescr(value: number): void;

  hasValue(): boolean;
  clearValue(): void;
  getValue(): GattDescriptorValue | undefined;
  setValue(value?: GattDescriptorValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WriteDescriptor.AsObject;
  static toObject(includeInstance: boolean, msg: WriteDescriptor): WriteDescriptor.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: WriteDescriptor, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WriteDescriptor;
  static deserializeBinaryFromReader(message: WriteDescriptor, reader: jspb.BinaryReader): WriteDescriptor;
}

export namespace WriteDescriptor {
  export type AsObject = {
    descr: number,
    value?: GattDescriptorValue.AsObject,
  }
}

export class WriteDescriptorSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WriteDescriptorSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: WriteDescriptorSuccess): WriteDescriptorSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: WriteDescriptorSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WriteDescriptorSuccess;
  static deserializeBinaryFromReader(message: WriteDescriptorSuccess, reader: jspb.BinaryReader): WriteDescriptorSuccess;
}

export namespace WriteDescriptorSuccess {
  export type AsObject = {
  }
}

export class SetNotification extends jspb.Message {
  getCharacteristic(): number;
  setCharacteristic(value: number): void;

  getEnableNotification(): boolean;
  setEnableNotification(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetNotification.AsObject;
  static toObject(includeInstance: boolean, msg: SetNotification): SetNotification.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetNotification, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetNotification;
  static deserializeBinaryFromReader(message: SetNotification, reader: jspb.BinaryReader): SetNotification;
}

export namespace SetNotification {
  export type AsObject = {
    characteristic: number,
    enableNotification: boolean,
  }
}

export class SetNotificationSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetNotificationSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: SetNotificationSuccess): SetNotificationSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetNotificationSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetNotificationSuccess;
  static deserializeBinaryFromReader(message: SetNotificationSuccess, reader: jspb.BinaryReader): SetNotificationSuccess;
}

export namespace SetNotificationSuccess {
  export type AsObject = {
  }
}

