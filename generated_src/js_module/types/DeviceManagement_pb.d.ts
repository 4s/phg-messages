// package: s4.messages.interfaces.device_management
// file: DeviceManagement.proto

import * as jspb from "google-protobuf";
import * as Device_pb from "./Device_pb";

export class C2P extends jspb.Message {
  hasRequest(): boolean;
  clearRequest(): void;
  getRequest(): C2P.Request | undefined;
  setRequest(value?: C2P.Request): void;

  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): C2P.Event | undefined;
  setEvent(value?: C2P.Event): void;

  getPayloadCase(): C2P.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): C2P.AsObject;
  static toObject(includeInstance: boolean, msg: C2P): C2P.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: C2P, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): C2P;
  static deserializeBinaryFromReader(message: C2P, reader: jspb.BinaryReader): C2P;
}

export namespace C2P {
  export type AsObject = {
    request?: C2P.Request.AsObject,
    event?: C2P.Event.AsObject,
  }

  export class Request extends jspb.Message {
    hasLink(): boolean;
    clearLink(): void;
    getLink(): Link | undefined;
    setLink(value?: Link): void;

    hasUnlink(): boolean;
    clearUnlink(): void;
    getUnlink(): Unlink | undefined;
    setUnlink(value?: Unlink): void;

    hasConnect(): boolean;
    clearConnect(): void;
    getConnect(): Connect | undefined;
    setConnect(value?: Connect): void;

    hasDisconnect(): boolean;
    clearDisconnect(): void;
    getDisconnect(): Disconnect | undefined;
    setDisconnect(value?: Disconnect): void;

    hasSetName(): boolean;
    clearSetName(): void;
    getSetName(): SetName | undefined;
    setSetName(value?: SetName): void;

    getRequestCase(): Request.RequestCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Request.AsObject;
    static toObject(includeInstance: boolean, msg: Request): Request.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Request, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Request;
    static deserializeBinaryFromReader(message: Request, reader: jspb.BinaryReader): Request;
  }

  export namespace Request {
    export type AsObject = {
      link?: Link.AsObject,
      unlink?: Unlink.AsObject,
      connect?: Connect.AsObject,
      disconnect?: Disconnect.AsObject,
      setName?: SetName.AsObject,
    }

    export enum RequestCase {
      REQUEST_NOT_SET = 0,
      LINK = 1,
      UNLINK = 2,
      CONNECT = 3,
      DISCONNECT = 4,
      SET_NAME = 5,
    }
  }

  export class Event extends jspb.Message {
    hasGetAvailableDevices(): boolean;
    clearGetAvailableDevices(): void;
    getGetAvailableDevices(): GetAvailableDevices | undefined;
    setGetAvailableDevices(value?: GetAvailableDevices): void;

    hasSearchSupportingDevices(): boolean;
    clearSearchSupportingDevices(): void;
    getSearchSupportingDevices(): SearchSupportingDevices | undefined;
    setSearchSupportingDevices(value?: SearchSupportingDevices): void;

    hasStopSearch(): boolean;
    clearStopSearch(): void;
    getStopSearch(): StopSearch | undefined;
    setStopSearch(value?: StopSearch): void;

    hasSolicitDevice(): boolean;
    clearSolicitDevice(): void;
    getSolicitDevice(): SolicitDevice | undefined;
    setSolicitDevice(value?: SolicitDevice): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      getAvailableDevices?: GetAvailableDevices.AsObject,
      searchSupportingDevices?: SearchSupportingDevices.AsObject,
      stopSearch?: StopSearch.AsObject,
      solicitDevice?: SolicitDevice.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      GET_AVAILABLE_DEVICES = 1,
      SEARCH_SUPPORTING_DEVICES = 2,
      STOP_SEARCH = 3,
      SOLICIT_DEVICE = 4,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    REQUEST = 1,
    EVENT = 2,
  }
}

export class P2C extends jspb.Message {
  hasRequest(): boolean;
  clearRequest(): void;
  getRequest(): P2C.Request | undefined;
  setRequest(value?: P2C.Request): void;

  hasEvent(): boolean;
  clearEvent(): void;
  getEvent(): P2C.Event | undefined;
  setEvent(value?: P2C.Event): void;

  getPayloadCase(): P2C.PayloadCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): P2C.AsObject;
  static toObject(includeInstance: boolean, msg: P2C): P2C.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: P2C, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): P2C;
  static deserializeBinaryFromReader(message: P2C, reader: jspb.BinaryReader): P2C;
}

export namespace P2C {
  export type AsObject = {
    request?: P2C.Request.AsObject,
    event?: P2C.Event.AsObject,
  }

  export class Request extends jspb.Message {
    hasBluetoothPairing(): boolean;
    clearBluetoothPairing(): void;
    getBluetoothPairing(): BluetoothPairing | undefined;
    setBluetoothPairing(value?: BluetoothPairing): void;

    getRequestCase(): Request.RequestCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Request.AsObject;
    static toObject(includeInstance: boolean, msg: Request): Request.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Request, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Request;
    static deserializeBinaryFromReader(message: Request, reader: jspb.BinaryReader): Request;
  }

  export namespace Request {
    export type AsObject = {
      bluetoothPairing?: BluetoothPairing.AsObject,
    }

    export enum RequestCase {
      REQUEST_NOT_SET = 0,
      BLUETOOTH_PAIRING = 1,
    }
  }

  export class Event extends jspb.Message {
    hasAvailableDevices(): boolean;
    clearAvailableDevices(): void;
    getAvailableDevices(): AvailableDevices | undefined;
    setAvailableDevices(value?: AvailableDevices): void;

    hasSearchResult(): boolean;
    clearSearchResult(): void;
    getSearchResult(): SearchResult | undefined;
    setSearchResult(value?: SearchResult): void;

    hasDevice(): boolean;
    clearDevice(): void;
    getDevice(): Device_pb.Device | undefined;
    setDevice(value?: Device_pb.Device): void;

    getEventCase(): Event.EventCase;
    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Event.AsObject;
    static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Event;
    static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
  }

  export namespace Event {
    export type AsObject = {
      availableDevices?: AvailableDevices.AsObject,
      searchResult?: SearchResult.AsObject,
      device?: Device_pb.Device.AsObject,
    }

    export enum EventCase {
      EVENT_NOT_SET = 0,
      AVAILABLE_DEVICES = 1,
      SEARCH_RESULT = 2,
      DEVICE = 3,
    }
  }

  export enum PayloadCase {
    PAYLOAD_NOT_SET = 0,
    REQUEST = 1,
    EVENT = 2,
  }
}

export class Link extends jspb.Message {
  getDeviceHandle(): number;
  setDeviceHandle(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Link.AsObject;
  static toObject(includeInstance: boolean, msg: Link): Link.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Link, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Link;
  static deserializeBinaryFromReader(message: Link, reader: jspb.BinaryReader): Link;
}

export namespace Link {
  export type AsObject = {
    deviceHandle: number,
  }
}

export class LinkSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LinkSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: LinkSuccess): LinkSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LinkSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LinkSuccess;
  static deserializeBinaryFromReader(message: LinkSuccess, reader: jspb.BinaryReader): LinkSuccess;
}

export namespace LinkSuccess {
  export type AsObject = {
  }
}

export class Unlink extends jspb.Message {
  getDeviceHandle(): number;
  setDeviceHandle(value: number): void;

  getUnlinkTransport(): boolean;
  setUnlinkTransport(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Unlink.AsObject;
  static toObject(includeInstance: boolean, msg: Unlink): Unlink.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Unlink, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Unlink;
  static deserializeBinaryFromReader(message: Unlink, reader: jspb.BinaryReader): Unlink;
}

export namespace Unlink {
  export type AsObject = {
    deviceHandle: number,
    unlinkTransport: boolean,
  }
}

export class UnlinkSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UnlinkSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: UnlinkSuccess): UnlinkSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UnlinkSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UnlinkSuccess;
  static deserializeBinaryFromReader(message: UnlinkSuccess, reader: jspb.BinaryReader): UnlinkSuccess;
}

export namespace UnlinkSuccess {
  export type AsObject = {
  }
}

export class Connect extends jspb.Message {
  getDeviceHandle(): number;
  setDeviceHandle(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Connect.AsObject;
  static toObject(includeInstance: boolean, msg: Connect): Connect.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Connect, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Connect;
  static deserializeBinaryFromReader(message: Connect, reader: jspb.BinaryReader): Connect;
}

export namespace Connect {
  export type AsObject = {
    deviceHandle: number,
  }
}

export class ConnectSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ConnectSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: ConnectSuccess): ConnectSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ConnectSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ConnectSuccess;
  static deserializeBinaryFromReader(message: ConnectSuccess, reader: jspb.BinaryReader): ConnectSuccess;
}

export namespace ConnectSuccess {
  export type AsObject = {
  }
}

export class Disconnect extends jspb.Message {
  getDeviceHandle(): number;
  setDeviceHandle(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Disconnect.AsObject;
  static toObject(includeInstance: boolean, msg: Disconnect): Disconnect.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Disconnect, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Disconnect;
  static deserializeBinaryFromReader(message: Disconnect, reader: jspb.BinaryReader): Disconnect;
}

export namespace Disconnect {
  export type AsObject = {
    deviceHandle: number,
  }
}

export class DisconnectSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DisconnectSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: DisconnectSuccess): DisconnectSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DisconnectSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DisconnectSuccess;
  static deserializeBinaryFromReader(message: DisconnectSuccess, reader: jspb.BinaryReader): DisconnectSuccess;
}

export namespace DisconnectSuccess {
  export type AsObject = {
  }
}

export class SetName extends jspb.Message {
  getDeviceHandle(): number;
  setDeviceHandle(value: number): void;

  getNewName(): string;
  setNewName(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetName.AsObject;
  static toObject(includeInstance: boolean, msg: SetName): SetName.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetName, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetName;
  static deserializeBinaryFromReader(message: SetName, reader: jspb.BinaryReader): SetName;
}

export namespace SetName {
  export type AsObject = {
    deviceHandle: number,
    newName: string,
  }
}

export class SetNameSuccess extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetNameSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: SetNameSuccess): SetNameSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetNameSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetNameSuccess;
  static deserializeBinaryFromReader(message: SetNameSuccess, reader: jspb.BinaryReader): SetNameSuccess;
}

export namespace SetNameSuccess {
  export type AsObject = {
  }
}

export class BluetoothPairing extends jspb.Message {
  hasDevice(): boolean;
  clearDevice(): void;
  getDevice(): Device_pb.Device | undefined;
  setDevice(value?: Device_pb.Device): void;

  getMethod(): BluetoothPairing.MethodMap[keyof BluetoothPairing.MethodMap];
  setMethod(value: BluetoothPairing.MethodMap[keyof BluetoothPairing.MethodMap]): void;

  getNumericComparisonValue(): number;
  setNumericComparisonValue(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BluetoothPairing.AsObject;
  static toObject(includeInstance: boolean, msg: BluetoothPairing): BluetoothPairing.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: BluetoothPairing, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BluetoothPairing;
  static deserializeBinaryFromReader(message: BluetoothPairing, reader: jspb.BinaryReader): BluetoothPairing;
}

export namespace BluetoothPairing {
  export type AsObject = {
    device?: Device_pb.Device.AsObject,
    method: BluetoothPairing.MethodMap[keyof BluetoothPairing.MethodMap],
    numericComparisonValue: number,
  }

  export interface MethodMap {
    LEGACY: 0;
    JUST_WORKS: 1;
    NUMERIC_COMPARISON: 2;
    PASSKEY: 3;
  }

  export const Method: MethodMap;
}

export class BluetoothPairingSuccess extends jspb.Message {
  hasLegacy(): boolean;
  clearLegacy(): void;
  getLegacy(): string;
  setLegacy(value: string): void;

  hasAccept(): boolean;
  clearAccept(): void;
  getAccept(): boolean;
  setAccept(value: boolean): void;

  hasPasskey(): boolean;
  clearPasskey(): void;
  getPasskey(): number;
  setPasskey(value: number): void;

  getTypeCase(): BluetoothPairingSuccess.TypeCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BluetoothPairingSuccess.AsObject;
  static toObject(includeInstance: boolean, msg: BluetoothPairingSuccess): BluetoothPairingSuccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: BluetoothPairingSuccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BluetoothPairingSuccess;
  static deserializeBinaryFromReader(message: BluetoothPairingSuccess, reader: jspb.BinaryReader): BluetoothPairingSuccess;
}

export namespace BluetoothPairingSuccess {
  export type AsObject = {
    legacy: string,
    accept: boolean,
    passkey: number,
  }

  export enum TypeCase {
    TYPE_NOT_SET = 0,
    LEGACY = 1,
    ACCEPT = 2,
    PASSKEY = 3,
  }
}

export class GetAvailableDevices extends jspb.Message {
  clearPrototypesList(): void;
  getPrototypesList(): Array<Device_pb.Device>;
  setPrototypesList(value: Array<Device_pb.Device>): void;
  addPrototypes(value?: Device_pb.Device, index?: number): Device_pb.Device;

  getAllLinked(): boolean;
  setAllLinked(value: boolean): void;

  getAllConnected(): boolean;
  setAllConnected(value: boolean): void;

  getAllConnectable(): boolean;
  setAllConnectable(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetAvailableDevices.AsObject;
  static toObject(includeInstance: boolean, msg: GetAvailableDevices): GetAvailableDevices.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetAvailableDevices, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetAvailableDevices;
  static deserializeBinaryFromReader(message: GetAvailableDevices, reader: jspb.BinaryReader): GetAvailableDevices;
}

export namespace GetAvailableDevices {
  export type AsObject = {
    prototypesList: Array<Device_pb.Device.AsObject>,
    allLinked: boolean,
    allConnected: boolean,
    allConnectable: boolean,
  }
}

export class SearchSupportingDevices extends jspb.Message {
  clearPrototypesList(): void;
  getPrototypesList(): Array<Device_pb.Device>;
  setPrototypesList(value: Array<Device_pb.Device>): void;
  addPrototypes(value?: Device_pb.Device, index?: number): Device_pb.Device;

  getSearchId(): number;
  setSearchId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SearchSupportingDevices.AsObject;
  static toObject(includeInstance: boolean, msg: SearchSupportingDevices): SearchSupportingDevices.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SearchSupportingDevices, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SearchSupportingDevices;
  static deserializeBinaryFromReader(message: SearchSupportingDevices, reader: jspb.BinaryReader): SearchSupportingDevices;
}

export namespace SearchSupportingDevices {
  export type AsObject = {
    prototypesList: Array<Device_pb.Device.AsObject>,
    searchId: number,
  }
}

export class StopSearch extends jspb.Message {
  getSearchId(): number;
  setSearchId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StopSearch.AsObject;
  static toObject(includeInstance: boolean, msg: StopSearch): StopSearch.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StopSearch, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StopSearch;
  static deserializeBinaryFromReader(message: StopSearch, reader: jspb.BinaryReader): StopSearch;
}

export namespace StopSearch {
  export type AsObject = {
    searchId: number,
  }
}

export class SolicitDevice extends jspb.Message {
  getDeviceHandle(): number;
  setDeviceHandle(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolicitDevice.AsObject;
  static toObject(includeInstance: boolean, msg: SolicitDevice): SolicitDevice.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SolicitDevice, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolicitDevice;
  static deserializeBinaryFromReader(message: SolicitDevice, reader: jspb.BinaryReader): SolicitDevice;
}

export namespace SolicitDevice {
  export type AsObject = {
    deviceHandle: number,
  }
}

export class AvailableDevices extends jspb.Message {
  clearDevicesList(): void;
  getDevicesList(): Array<Device_pb.Device>;
  setDevicesList(value: Array<Device_pb.Device>): void;
  addDevices(value?: Device_pb.Device, index?: number): Device_pb.Device;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AvailableDevices.AsObject;
  static toObject(includeInstance: boolean, msg: AvailableDevices): AvailableDevices.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AvailableDevices, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AvailableDevices;
  static deserializeBinaryFromReader(message: AvailableDevices, reader: jspb.BinaryReader): AvailableDevices;
}

export namespace AvailableDevices {
  export type AsObject = {
    devicesList: Array<Device_pb.Device.AsObject>,
  }
}

export class SearchResult extends jspb.Message {
  getSearchId(): number;
  setSearchId(value: number): void;

  clearDevicesList(): void;
  getDevicesList(): Array<Device_pb.Device>;
  setDevicesList(value: Array<Device_pb.Device>): void;
  addDevices(value?: Device_pb.Device, index?: number): Device_pb.Device;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SearchResult.AsObject;
  static toObject(includeInstance: boolean, msg: SearchResult): SearchResult.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SearchResult, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SearchResult;
  static deserializeBinaryFromReader(message: SearchResult, reader: jspb.BinaryReader): SearchResult;
}

export namespace SearchResult {
  export type AsObject = {
    searchId: number,
    devicesList: Array<Device_pb.Device.AsObject>,
  }
}

