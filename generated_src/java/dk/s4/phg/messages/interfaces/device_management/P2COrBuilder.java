// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: DeviceManagement.proto

package dk.s4.phg.messages.interfaces.device_management;

public interface P2COrBuilder extends
    // @@protoc_insertion_point(interface_extends:s4.messages.interfaces.device_management.P2C)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>.s4.messages.interfaces.device_management.P2C.Request request = 1;</code>
   */
  boolean hasRequest();
  /**
   * <code>.s4.messages.interfaces.device_management.P2C.Request request = 1;</code>
   */
  dk.s4.phg.messages.interfaces.device_management.P2C.Request getRequest();
  /**
   * <code>.s4.messages.interfaces.device_management.P2C.Request request = 1;</code>
   */
  dk.s4.phg.messages.interfaces.device_management.P2C.RequestOrBuilder getRequestOrBuilder();

  /**
   * <code>.s4.messages.interfaces.device_management.P2C.Event event = 2;</code>
   */
  boolean hasEvent();
  /**
   * <code>.s4.messages.interfaces.device_management.P2C.Event event = 2;</code>
   */
  dk.s4.phg.messages.interfaces.device_management.P2C.Event getEvent();
  /**
   * <code>.s4.messages.interfaces.device_management.P2C.Event event = 2;</code>
   */
  dk.s4.phg.messages.interfaces.device_management.P2C.EventOrBuilder getEventOrBuilder();

  public dk.s4.phg.messages.interfaces.device_management.P2C.PayloadCase getPayloadCase();
}
