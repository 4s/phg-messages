// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Time.proto

package dk.s4.phg.messages.interfaces.time;

public interface StartTimerOrBuilder extends
    // @@protoc_insertion_point(interface_extends:s4.messages.interfaces.time.StartTimer)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   **
   * Delay in milliseconds.
   * Mandatory
   * </pre>
   *
   * <code>uint32 delay_millis = 1;</code>
   */
  int getDelayMillis();

  /**
   * <pre>
   **
   * True if the timer should repeat, false or missing for a
   * single-shot timer
   * Optional
   * </pre>
   *
   * <code>bool repeat = 2;</code>
   */
  boolean getRepeat();
}
