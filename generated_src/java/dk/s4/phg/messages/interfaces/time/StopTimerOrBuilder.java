// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Time.proto

package dk.s4.phg.messages.interfaces.time;

public interface StopTimerOrBuilder extends
    // @@protoc_insertion_point(interface_extends:s4.messages.interfaces.time.StopTimer)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   **
   * Id of the timer to stop.
   * Mandatory
   * </pre>
   *
   * <code>int32 timer_id = 1;</code>
   */
  int getTimerId();
}
