// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: DeviceControl.proto

package dk.s4.phg.messages.interfaces.device_control;

/**
 * <pre>
 * P2C
 * </pre>
 *
 * Protobuf type {@code s4.messages.interfaces.device_control.SearchInitiated}
 */
public  final class SearchInitiated extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:s4.messages.interfaces.device_control.SearchInitiated)
    SearchInitiatedOrBuilder {
private static final long serialVersionUID = 0L;
  // Use SearchInitiated.newBuilder() to construct.
  private SearchInitiated(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private SearchInitiated() {
    foundSoFar_ = java.util.Collections.emptyList();
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new SearchInitiated();
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private SearchInitiated(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new java.lang.NullPointerException();
    }
    int mutable_bitField0_ = 0;
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          case 8: {

            searchId_ = input.readInt64();
            break;
          }
          case 18: {
            if (!((mutable_bitField0_ & 0x00000001) != 0)) {
              foundSoFar_ = new java.util.ArrayList<dk.s4.phg.messages.interfaces.device_control.TransportBundle>();
              mutable_bitField0_ |= 0x00000001;
            }
            foundSoFar_.add(
                input.readMessage(dk.s4.phg.messages.interfaces.device_control.TransportBundle.parser(), extensionRegistry));
            break;
          }
          default: {
            if (!parseUnknownField(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      if (((mutable_bitField0_ & 0x00000001) != 0)) {
        foundSoFar_ = java.util.Collections.unmodifiableList(foundSoFar_);
      }
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return dk.s4.phg.messages.interfaces.device_control.DeviceControl.internal_static_s4_messages_interfaces_device_control_SearchInitiated_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return dk.s4.phg.messages.interfaces.device_control.DeviceControl.internal_static_s4_messages_interfaces_device_control_SearchInitiated_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            dk.s4.phg.messages.interfaces.device_control.SearchInitiated.class, dk.s4.phg.messages.interfaces.device_control.SearchInitiated.Builder.class);
  }

  public static final int SEARCH_ID_FIELD_NUMBER = 1;
  private long searchId_;
  /**
   * <pre>
   * objId_t 
   * </pre>
   *
   * <code>int64 search_id = 1;</code>
   */
  public long getSearchId() {
    return searchId_;
  }

  public static final int FOUND_SO_FAR_FIELD_NUMBER = 2;
  private java.util.List<dk.s4.phg.messages.interfaces.device_control.TransportBundle> foundSoFar_;
  /**
   * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
   */
  public java.util.List<dk.s4.phg.messages.interfaces.device_control.TransportBundle> getFoundSoFarList() {
    return foundSoFar_;
  }
  /**
   * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
   */
  public java.util.List<? extends dk.s4.phg.messages.interfaces.device_control.TransportBundleOrBuilder> 
      getFoundSoFarOrBuilderList() {
    return foundSoFar_;
  }
  /**
   * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
   */
  public int getFoundSoFarCount() {
    return foundSoFar_.size();
  }
  /**
   * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
   */
  public dk.s4.phg.messages.interfaces.device_control.TransportBundle getFoundSoFar(int index) {
    return foundSoFar_.get(index);
  }
  /**
   * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
   */
  public dk.s4.phg.messages.interfaces.device_control.TransportBundleOrBuilder getFoundSoFarOrBuilder(
      int index) {
    return foundSoFar_.get(index);
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (searchId_ != 0L) {
      output.writeInt64(1, searchId_);
    }
    for (int i = 0; i < foundSoFar_.size(); i++) {
      output.writeMessage(2, foundSoFar_.get(i));
    }
    unknownFields.writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (searchId_ != 0L) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt64Size(1, searchId_);
    }
    for (int i = 0; i < foundSoFar_.size(); i++) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, foundSoFar_.get(i));
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof dk.s4.phg.messages.interfaces.device_control.SearchInitiated)) {
      return super.equals(obj);
    }
    dk.s4.phg.messages.interfaces.device_control.SearchInitiated other = (dk.s4.phg.messages.interfaces.device_control.SearchInitiated) obj;

    if (getSearchId()
        != other.getSearchId()) return false;
    if (!getFoundSoFarList()
        .equals(other.getFoundSoFarList())) return false;
    if (!unknownFields.equals(other.unknownFields)) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + SEARCH_ID_FIELD_NUMBER;
    hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
        getSearchId());
    if (getFoundSoFarCount() > 0) {
      hash = (37 * hash) + FOUND_SO_FAR_FIELD_NUMBER;
      hash = (53 * hash) + getFoundSoFarList().hashCode();
    }
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static dk.s4.phg.messages.interfaces.device_control.SearchInitiated parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static dk.s4.phg.messages.interfaces.device_control.SearchInitiated parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static dk.s4.phg.messages.interfaces.device_control.SearchInitiated parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static dk.s4.phg.messages.interfaces.device_control.SearchInitiated parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static dk.s4.phg.messages.interfaces.device_control.SearchInitiated parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static dk.s4.phg.messages.interfaces.device_control.SearchInitiated parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static dk.s4.phg.messages.interfaces.device_control.SearchInitiated parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static dk.s4.phg.messages.interfaces.device_control.SearchInitiated parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static dk.s4.phg.messages.interfaces.device_control.SearchInitiated parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static dk.s4.phg.messages.interfaces.device_control.SearchInitiated parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static dk.s4.phg.messages.interfaces.device_control.SearchInitiated parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static dk.s4.phg.messages.interfaces.device_control.SearchInitiated parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(dk.s4.phg.messages.interfaces.device_control.SearchInitiated prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * P2C
   * </pre>
   *
   * Protobuf type {@code s4.messages.interfaces.device_control.SearchInitiated}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:s4.messages.interfaces.device_control.SearchInitiated)
      dk.s4.phg.messages.interfaces.device_control.SearchInitiatedOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return dk.s4.phg.messages.interfaces.device_control.DeviceControl.internal_static_s4_messages_interfaces_device_control_SearchInitiated_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return dk.s4.phg.messages.interfaces.device_control.DeviceControl.internal_static_s4_messages_interfaces_device_control_SearchInitiated_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              dk.s4.phg.messages.interfaces.device_control.SearchInitiated.class, dk.s4.phg.messages.interfaces.device_control.SearchInitiated.Builder.class);
    }

    // Construct using dk.s4.phg.messages.interfaces.device_control.SearchInitiated.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
        getFoundSoFarFieldBuilder();
      }
    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      searchId_ = 0L;

      if (foundSoFarBuilder_ == null) {
        foundSoFar_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000001);
      } else {
        foundSoFarBuilder_.clear();
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return dk.s4.phg.messages.interfaces.device_control.DeviceControl.internal_static_s4_messages_interfaces_device_control_SearchInitiated_descriptor;
    }

    @java.lang.Override
    public dk.s4.phg.messages.interfaces.device_control.SearchInitiated getDefaultInstanceForType() {
      return dk.s4.phg.messages.interfaces.device_control.SearchInitiated.getDefaultInstance();
    }

    @java.lang.Override
    public dk.s4.phg.messages.interfaces.device_control.SearchInitiated build() {
      dk.s4.phg.messages.interfaces.device_control.SearchInitiated result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public dk.s4.phg.messages.interfaces.device_control.SearchInitiated buildPartial() {
      dk.s4.phg.messages.interfaces.device_control.SearchInitiated result = new dk.s4.phg.messages.interfaces.device_control.SearchInitiated(this);
      int from_bitField0_ = bitField0_;
      result.searchId_ = searchId_;
      if (foundSoFarBuilder_ == null) {
        if (((bitField0_ & 0x00000001) != 0)) {
          foundSoFar_ = java.util.Collections.unmodifiableList(foundSoFar_);
          bitField0_ = (bitField0_ & ~0x00000001);
        }
        result.foundSoFar_ = foundSoFar_;
      } else {
        result.foundSoFar_ = foundSoFarBuilder_.build();
      }
      onBuilt();
      return result;
    }

    @java.lang.Override
    public Builder clone() {
      return super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof dk.s4.phg.messages.interfaces.device_control.SearchInitiated) {
        return mergeFrom((dk.s4.phg.messages.interfaces.device_control.SearchInitiated)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(dk.s4.phg.messages.interfaces.device_control.SearchInitiated other) {
      if (other == dk.s4.phg.messages.interfaces.device_control.SearchInitiated.getDefaultInstance()) return this;
      if (other.getSearchId() != 0L) {
        setSearchId(other.getSearchId());
      }
      if (foundSoFarBuilder_ == null) {
        if (!other.foundSoFar_.isEmpty()) {
          if (foundSoFar_.isEmpty()) {
            foundSoFar_ = other.foundSoFar_;
            bitField0_ = (bitField0_ & ~0x00000001);
          } else {
            ensureFoundSoFarIsMutable();
            foundSoFar_.addAll(other.foundSoFar_);
          }
          onChanged();
        }
      } else {
        if (!other.foundSoFar_.isEmpty()) {
          if (foundSoFarBuilder_.isEmpty()) {
            foundSoFarBuilder_.dispose();
            foundSoFarBuilder_ = null;
            foundSoFar_ = other.foundSoFar_;
            bitField0_ = (bitField0_ & ~0x00000001);
            foundSoFarBuilder_ = 
              com.google.protobuf.GeneratedMessageV3.alwaysUseFieldBuilders ?
                 getFoundSoFarFieldBuilder() : null;
          } else {
            foundSoFarBuilder_.addAllMessages(other.foundSoFar_);
          }
        }
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      dk.s4.phg.messages.interfaces.device_control.SearchInitiated parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (dk.s4.phg.messages.interfaces.device_control.SearchInitiated) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int bitField0_;

    private long searchId_ ;
    /**
     * <pre>
     * objId_t 
     * </pre>
     *
     * <code>int64 search_id = 1;</code>
     */
    public long getSearchId() {
      return searchId_;
    }
    /**
     * <pre>
     * objId_t 
     * </pre>
     *
     * <code>int64 search_id = 1;</code>
     */
    public Builder setSearchId(long value) {
      
      searchId_ = value;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * objId_t 
     * </pre>
     *
     * <code>int64 search_id = 1;</code>
     */
    public Builder clearSearchId() {
      
      searchId_ = 0L;
      onChanged();
      return this;
    }

    private java.util.List<dk.s4.phg.messages.interfaces.device_control.TransportBundle> foundSoFar_ =
      java.util.Collections.emptyList();
    private void ensureFoundSoFarIsMutable() {
      if (!((bitField0_ & 0x00000001) != 0)) {
        foundSoFar_ = new java.util.ArrayList<dk.s4.phg.messages.interfaces.device_control.TransportBundle>(foundSoFar_);
        bitField0_ |= 0x00000001;
       }
    }

    private com.google.protobuf.RepeatedFieldBuilderV3<
        dk.s4.phg.messages.interfaces.device_control.TransportBundle, dk.s4.phg.messages.interfaces.device_control.TransportBundle.Builder, dk.s4.phg.messages.interfaces.device_control.TransportBundleOrBuilder> foundSoFarBuilder_;

    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public java.util.List<dk.s4.phg.messages.interfaces.device_control.TransportBundle> getFoundSoFarList() {
      if (foundSoFarBuilder_ == null) {
        return java.util.Collections.unmodifiableList(foundSoFar_);
      } else {
        return foundSoFarBuilder_.getMessageList();
      }
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public int getFoundSoFarCount() {
      if (foundSoFarBuilder_ == null) {
        return foundSoFar_.size();
      } else {
        return foundSoFarBuilder_.getCount();
      }
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public dk.s4.phg.messages.interfaces.device_control.TransportBundle getFoundSoFar(int index) {
      if (foundSoFarBuilder_ == null) {
        return foundSoFar_.get(index);
      } else {
        return foundSoFarBuilder_.getMessage(index);
      }
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public Builder setFoundSoFar(
        int index, dk.s4.phg.messages.interfaces.device_control.TransportBundle value) {
      if (foundSoFarBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureFoundSoFarIsMutable();
        foundSoFar_.set(index, value);
        onChanged();
      } else {
        foundSoFarBuilder_.setMessage(index, value);
      }
      return this;
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public Builder setFoundSoFar(
        int index, dk.s4.phg.messages.interfaces.device_control.TransportBundle.Builder builderForValue) {
      if (foundSoFarBuilder_ == null) {
        ensureFoundSoFarIsMutable();
        foundSoFar_.set(index, builderForValue.build());
        onChanged();
      } else {
        foundSoFarBuilder_.setMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public Builder addFoundSoFar(dk.s4.phg.messages.interfaces.device_control.TransportBundle value) {
      if (foundSoFarBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureFoundSoFarIsMutable();
        foundSoFar_.add(value);
        onChanged();
      } else {
        foundSoFarBuilder_.addMessage(value);
      }
      return this;
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public Builder addFoundSoFar(
        int index, dk.s4.phg.messages.interfaces.device_control.TransportBundle value) {
      if (foundSoFarBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureFoundSoFarIsMutable();
        foundSoFar_.add(index, value);
        onChanged();
      } else {
        foundSoFarBuilder_.addMessage(index, value);
      }
      return this;
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public Builder addFoundSoFar(
        dk.s4.phg.messages.interfaces.device_control.TransportBundle.Builder builderForValue) {
      if (foundSoFarBuilder_ == null) {
        ensureFoundSoFarIsMutable();
        foundSoFar_.add(builderForValue.build());
        onChanged();
      } else {
        foundSoFarBuilder_.addMessage(builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public Builder addFoundSoFar(
        int index, dk.s4.phg.messages.interfaces.device_control.TransportBundle.Builder builderForValue) {
      if (foundSoFarBuilder_ == null) {
        ensureFoundSoFarIsMutable();
        foundSoFar_.add(index, builderForValue.build());
        onChanged();
      } else {
        foundSoFarBuilder_.addMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public Builder addAllFoundSoFar(
        java.lang.Iterable<? extends dk.s4.phg.messages.interfaces.device_control.TransportBundle> values) {
      if (foundSoFarBuilder_ == null) {
        ensureFoundSoFarIsMutable();
        com.google.protobuf.AbstractMessageLite.Builder.addAll(
            values, foundSoFar_);
        onChanged();
      } else {
        foundSoFarBuilder_.addAllMessages(values);
      }
      return this;
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public Builder clearFoundSoFar() {
      if (foundSoFarBuilder_ == null) {
        foundSoFar_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000001);
        onChanged();
      } else {
        foundSoFarBuilder_.clear();
      }
      return this;
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public Builder removeFoundSoFar(int index) {
      if (foundSoFarBuilder_ == null) {
        ensureFoundSoFarIsMutable();
        foundSoFar_.remove(index);
        onChanged();
      } else {
        foundSoFarBuilder_.remove(index);
      }
      return this;
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public dk.s4.phg.messages.interfaces.device_control.TransportBundle.Builder getFoundSoFarBuilder(
        int index) {
      return getFoundSoFarFieldBuilder().getBuilder(index);
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public dk.s4.phg.messages.interfaces.device_control.TransportBundleOrBuilder getFoundSoFarOrBuilder(
        int index) {
      if (foundSoFarBuilder_ == null) {
        return foundSoFar_.get(index);  } else {
        return foundSoFarBuilder_.getMessageOrBuilder(index);
      }
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public java.util.List<? extends dk.s4.phg.messages.interfaces.device_control.TransportBundleOrBuilder> 
         getFoundSoFarOrBuilderList() {
      if (foundSoFarBuilder_ != null) {
        return foundSoFarBuilder_.getMessageOrBuilderList();
      } else {
        return java.util.Collections.unmodifiableList(foundSoFar_);
      }
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public dk.s4.phg.messages.interfaces.device_control.TransportBundle.Builder addFoundSoFarBuilder() {
      return getFoundSoFarFieldBuilder().addBuilder(
          dk.s4.phg.messages.interfaces.device_control.TransportBundle.getDefaultInstance());
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public dk.s4.phg.messages.interfaces.device_control.TransportBundle.Builder addFoundSoFarBuilder(
        int index) {
      return getFoundSoFarFieldBuilder().addBuilder(
          index, dk.s4.phg.messages.interfaces.device_control.TransportBundle.getDefaultInstance());
    }
    /**
     * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
     */
    public java.util.List<dk.s4.phg.messages.interfaces.device_control.TransportBundle.Builder> 
         getFoundSoFarBuilderList() {
      return getFoundSoFarFieldBuilder().getBuilderList();
    }
    private com.google.protobuf.RepeatedFieldBuilderV3<
        dk.s4.phg.messages.interfaces.device_control.TransportBundle, dk.s4.phg.messages.interfaces.device_control.TransportBundle.Builder, dk.s4.phg.messages.interfaces.device_control.TransportBundleOrBuilder> 
        getFoundSoFarFieldBuilder() {
      if (foundSoFarBuilder_ == null) {
        foundSoFarBuilder_ = new com.google.protobuf.RepeatedFieldBuilderV3<
            dk.s4.phg.messages.interfaces.device_control.TransportBundle, dk.s4.phg.messages.interfaces.device_control.TransportBundle.Builder, dk.s4.phg.messages.interfaces.device_control.TransportBundleOrBuilder>(
                foundSoFar_,
                ((bitField0_ & 0x00000001) != 0),
                getParentForChildren(),
                isClean());
        foundSoFar_ = null;
      }
      return foundSoFarBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:s4.messages.interfaces.device_control.SearchInitiated)
  }

  // @@protoc_insertion_point(class_scope:s4.messages.interfaces.device_control.SearchInitiated)
  private static final dk.s4.phg.messages.interfaces.device_control.SearchInitiated DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new dk.s4.phg.messages.interfaces.device_control.SearchInitiated();
  }

  public static dk.s4.phg.messages.interfaces.device_control.SearchInitiated getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<SearchInitiated>
      PARSER = new com.google.protobuf.AbstractParser<SearchInitiated>() {
    @java.lang.Override
    public SearchInitiated parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new SearchInitiated(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<SearchInitiated> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<SearchInitiated> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public dk.s4.phg.messages.interfaces.device_control.SearchInitiated getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

