// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: DeviceControl.proto

package dk.s4.phg.messages.interfaces.device_control;

public interface SearchInitiatedOrBuilder extends
    // @@protoc_insertion_point(interface_extends:s4.messages.interfaces.device_control.SearchInitiated)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * objId_t 
   * </pre>
   *
   * <code>int64 search_id = 1;</code>
   */
  long getSearchId();

  /**
   * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
   */
  java.util.List<dk.s4.phg.messages.interfaces.device_control.TransportBundle> 
      getFoundSoFarList();
  /**
   * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
   */
  dk.s4.phg.messages.interfaces.device_control.TransportBundle getFoundSoFar(int index);
  /**
   * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
   */
  int getFoundSoFarCount();
  /**
   * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
   */
  java.util.List<? extends dk.s4.phg.messages.interfaces.device_control.TransportBundleOrBuilder> 
      getFoundSoFarOrBuilderList();
  /**
   * <code>repeated .s4.messages.interfaces.device_control.TransportBundle found_so_far = 2;</code>
   */
  dk.s4.phg.messages.interfaces.device_control.TransportBundleOrBuilder getFoundSoFarOrBuilder(
      int index);
}
