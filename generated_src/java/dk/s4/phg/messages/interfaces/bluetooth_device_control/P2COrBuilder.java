// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: BluetoothDeviceControl.proto

package dk.s4.phg.messages.interfaces.bluetooth_device_control;

public interface P2COrBuilder extends
    // @@protoc_insertion_point(interface_extends:s4.messages.interfaces.bluetooth_device_control.P2C)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * No events defined at this point
   * </pre>
   *
   * <code>.s4.messages.interfaces.bluetooth_device_control.P2C.Request request = 1;</code>
   */
  boolean hasRequest();
  /**
   * <pre>
   * No events defined at this point
   * </pre>
   *
   * <code>.s4.messages.interfaces.bluetooth_device_control.P2C.Request request = 1;</code>
   */
  dk.s4.phg.messages.interfaces.bluetooth_device_control.P2C.Request getRequest();
  /**
   * <pre>
   * No events defined at this point
   * </pre>
   *
   * <code>.s4.messages.interfaces.bluetooth_device_control.P2C.Request request = 1;</code>
   */
  dk.s4.phg.messages.interfaces.bluetooth_device_control.P2C.RequestOrBuilder getRequestOrBuilder();

  public dk.s4.phg.messages.interfaces.bluetooth_device_control.P2C.PayloadCase getPayloadCase();
}
