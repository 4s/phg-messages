// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ConfigSettings.proto

package dk.s4.phg.messages.interfaces.config_settings;

public interface C2POrBuilder extends
    // @@protoc_insertion_point(interface_extends:s4.messages.interfaces.config_settings.C2P)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>.s4.messages.interfaces.config_settings.C2P.Request request = 1;</code>
   */
  boolean hasRequest();
  /**
   * <code>.s4.messages.interfaces.config_settings.C2P.Request request = 1;</code>
   */
  dk.s4.phg.messages.interfaces.config_settings.C2P.Request getRequest();
  /**
   * <code>.s4.messages.interfaces.config_settings.C2P.Request request = 1;</code>
   */
  dk.s4.phg.messages.interfaces.config_settings.C2P.RequestOrBuilder getRequestOrBuilder();

  /**
   * <code>.s4.messages.interfaces.config_settings.C2P.Event event = 2;</code>
   */
  boolean hasEvent();
  /**
   * <code>.s4.messages.interfaces.config_settings.C2P.Event event = 2;</code>
   */
  dk.s4.phg.messages.interfaces.config_settings.C2P.Event getEvent();
  /**
   * <code>.s4.messages.interfaces.config_settings.C2P.Event event = 2;</code>
   */
  dk.s4.phg.messages.interfaces.config_settings.C2P.EventOrBuilder getEventOrBuilder();

  public dk.s4.phg.messages.interfaces.config_settings.C2P.PayloadCase getPayloadCase();
}
