// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ConfigSettings.proto

package dk.s4.phg.messages.interfaces.config_settings;

/**
 * <pre>
 **
 * A ConfigSettings interface provider (store) announces its service to all
 * interested modules
 * </pre>
 *
 * Protobuf type {@code s4.messages.interfaces.config_settings.AnnounceStore}
 */
public  final class AnnounceStore extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:s4.messages.interfaces.config_settings.AnnounceStore)
    AnnounceStoreOrBuilder {
private static final long serialVersionUID = 0L;
  // Use AnnounceStore.newBuilder() to construct.
  private AnnounceStore(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private AnnounceStore() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new AnnounceStore();
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private AnnounceStore(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new java.lang.NullPointerException();
    }
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!parseUnknownField(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return dk.s4.phg.messages.interfaces.config_settings.ConfigSettings.internal_static_s4_messages_interfaces_config_settings_AnnounceStore_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return dk.s4.phg.messages.interfaces.config_settings.ConfigSettings.internal_static_s4_messages_interfaces_config_settings_AnnounceStore_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            dk.s4.phg.messages.interfaces.config_settings.AnnounceStore.class, dk.s4.phg.messages.interfaces.config_settings.AnnounceStore.Builder.class);
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    unknownFields.writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof dk.s4.phg.messages.interfaces.config_settings.AnnounceStore)) {
      return super.equals(obj);
    }
    dk.s4.phg.messages.interfaces.config_settings.AnnounceStore other = (dk.s4.phg.messages.interfaces.config_settings.AnnounceStore) obj;

    if (!unknownFields.equals(other.unknownFields)) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static dk.s4.phg.messages.interfaces.config_settings.AnnounceStore parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static dk.s4.phg.messages.interfaces.config_settings.AnnounceStore parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static dk.s4.phg.messages.interfaces.config_settings.AnnounceStore parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static dk.s4.phg.messages.interfaces.config_settings.AnnounceStore parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static dk.s4.phg.messages.interfaces.config_settings.AnnounceStore parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static dk.s4.phg.messages.interfaces.config_settings.AnnounceStore parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static dk.s4.phg.messages.interfaces.config_settings.AnnounceStore parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static dk.s4.phg.messages.interfaces.config_settings.AnnounceStore parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static dk.s4.phg.messages.interfaces.config_settings.AnnounceStore parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static dk.s4.phg.messages.interfaces.config_settings.AnnounceStore parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static dk.s4.phg.messages.interfaces.config_settings.AnnounceStore parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static dk.s4.phg.messages.interfaces.config_settings.AnnounceStore parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(dk.s4.phg.messages.interfaces.config_settings.AnnounceStore prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   **
   * A ConfigSettings interface provider (store) announces its service to all
   * interested modules
   * </pre>
   *
   * Protobuf type {@code s4.messages.interfaces.config_settings.AnnounceStore}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:s4.messages.interfaces.config_settings.AnnounceStore)
      dk.s4.phg.messages.interfaces.config_settings.AnnounceStoreOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return dk.s4.phg.messages.interfaces.config_settings.ConfigSettings.internal_static_s4_messages_interfaces_config_settings_AnnounceStore_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return dk.s4.phg.messages.interfaces.config_settings.ConfigSettings.internal_static_s4_messages_interfaces_config_settings_AnnounceStore_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              dk.s4.phg.messages.interfaces.config_settings.AnnounceStore.class, dk.s4.phg.messages.interfaces.config_settings.AnnounceStore.Builder.class);
    }

    // Construct using dk.s4.phg.messages.interfaces.config_settings.AnnounceStore.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return dk.s4.phg.messages.interfaces.config_settings.ConfigSettings.internal_static_s4_messages_interfaces_config_settings_AnnounceStore_descriptor;
    }

    @java.lang.Override
    public dk.s4.phg.messages.interfaces.config_settings.AnnounceStore getDefaultInstanceForType() {
      return dk.s4.phg.messages.interfaces.config_settings.AnnounceStore.getDefaultInstance();
    }

    @java.lang.Override
    public dk.s4.phg.messages.interfaces.config_settings.AnnounceStore build() {
      dk.s4.phg.messages.interfaces.config_settings.AnnounceStore result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public dk.s4.phg.messages.interfaces.config_settings.AnnounceStore buildPartial() {
      dk.s4.phg.messages.interfaces.config_settings.AnnounceStore result = new dk.s4.phg.messages.interfaces.config_settings.AnnounceStore(this);
      onBuilt();
      return result;
    }

    @java.lang.Override
    public Builder clone() {
      return super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof dk.s4.phg.messages.interfaces.config_settings.AnnounceStore) {
        return mergeFrom((dk.s4.phg.messages.interfaces.config_settings.AnnounceStore)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(dk.s4.phg.messages.interfaces.config_settings.AnnounceStore other) {
      if (other == dk.s4.phg.messages.interfaces.config_settings.AnnounceStore.getDefaultInstance()) return this;
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      dk.s4.phg.messages.interfaces.config_settings.AnnounceStore parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (dk.s4.phg.messages.interfaces.config_settings.AnnounceStore) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:s4.messages.interfaces.config_settings.AnnounceStore)
  }

  // @@protoc_insertion_point(class_scope:s4.messages.interfaces.config_settings.AnnounceStore)
  private static final dk.s4.phg.messages.interfaces.config_settings.AnnounceStore DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new dk.s4.phg.messages.interfaces.config_settings.AnnounceStore();
  }

  public static dk.s4.phg.messages.interfaces.config_settings.AnnounceStore getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<AnnounceStore>
      PARSER = new com.google.protobuf.AbstractParser<AnnounceStore>() {
    @java.lang.Override
    public AnnounceStore parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new AnnounceStore(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<AnnounceStore> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<AnnounceStore> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public dk.s4.phg.messages.interfaces.config_settings.AnnounceStore getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

