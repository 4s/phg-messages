// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: GATT.proto

package dk.s4.phg.messages.interfaces.gatt;

public interface ReadCharacteristicSuccessOrBuilder extends
    // @@protoc_insertion_point(interface_extends:s4.messages.interfaces.gatt.ReadCharacteristicSuccess)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   **
   * The data read from the characteristic
   * </pre>
   *
   * <code>bytes value = 1;</code>
   */
  com.google.protobuf.ByteString getValue();
}
