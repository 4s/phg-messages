// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: GATT.proto

package dk.s4.phg.messages.interfaces.gatt;

public interface ReadCharacteristicOrBuilder extends
    // @@protoc_insertion_point(interface_extends:s4.messages.interfaces.gatt.ReadCharacteristic)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * objId_t 
   * </pre>
   *
   * <code>int64 characteristic = 1;</code>
   */
  long getCharacteristic();
}
