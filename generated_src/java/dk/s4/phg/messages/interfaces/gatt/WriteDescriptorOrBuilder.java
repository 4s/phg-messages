// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: GATT.proto

package dk.s4.phg.messages.interfaces.gatt;

public interface WriteDescriptorOrBuilder extends
    // @@protoc_insertion_point(interface_extends:s4.messages.interfaces.gatt.WriteDescriptor)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * objId_t 
   * </pre>
   *
   * <code>int64 descr = 1;</code>
   */
  long getDescr();

  /**
   * <pre>
   **
   * The value to write
   * </pre>
   *
   * <code>.s4.messages.interfaces.gatt.GattDescriptorValue value = 2;</code>
   */
  boolean hasValue();
  /**
   * <pre>
   **
   * The value to write
   * </pre>
   *
   * <code>.s4.messages.interfaces.gatt.GattDescriptorValue value = 2;</code>
   */
  dk.s4.phg.messages.interfaces.gatt.GattDescriptorValue getValue();
  /**
   * <pre>
   **
   * The value to write
   * </pre>
   *
   * <code>.s4.messages.interfaces.gatt.GattDescriptorValue value = 2;</code>
   */
  dk.s4.phg.messages.interfaces.gatt.GattDescriptorValueOrBuilder getValueOrBuilder();
}
