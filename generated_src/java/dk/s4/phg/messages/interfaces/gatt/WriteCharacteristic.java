// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: GATT.proto

package dk.s4.phg.messages.interfaces.gatt;

/**
 * <pre>
 **
 * Write a characteristic value
 * Covers the GATT sub-procedures "Write Characteristic Value" and
 * "Write Long Characteristic Values". The sub-procedure
 * "Characteristic Values Reliable Write" is currently unsupported by
 * this interface.
 * The response to this function confirms that the write was completed
 * (or reports an error).
 * The following errors may be generated:
 *  1.  Invalid or missing argument - if characteristic is missing or does not represent
 *      a GATT characteristic known to this provider, or if value is missing.
 *  3.  Timeout - the write request timed out
 *  5.  Not permitted - the characteristic cannot be written
 *  6.  Insufficient authentication - the attribute requires
 *      authentication before it can be written
 *  7.  Insufficient authorization - the attribute requires
 *      authorization before it can be written
 *  8.  Insufficient encryption - the attribute requires encryption
 *      before it can be written, or the key size is insufficient
 *  11. Invalid attribute value or length - the attribute value
 *      length is invalid for the operation or the value itself is invalid
 *  12. Operation aborted - if the connection was closed while writing
 *  13. Not connected - the request was sent to a server which is currently
 *      not connected
 *  31. Communication error - any other communication problem occuring
 *      during the operation (which did not abort the connection)
 * </pre>
 *
 * Protobuf type {@code s4.messages.interfaces.gatt.WriteCharacteristic}
 */
public  final class WriteCharacteristic extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:s4.messages.interfaces.gatt.WriteCharacteristic)
    WriteCharacteristicOrBuilder {
private static final long serialVersionUID = 0L;
  // Use WriteCharacteristic.newBuilder() to construct.
  private WriteCharacteristic(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private WriteCharacteristic() {
    value_ = com.google.protobuf.ByteString.EMPTY;
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new WriteCharacteristic();
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private WriteCharacteristic(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new java.lang.NullPointerException();
    }
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          case 8: {

            characteristic_ = input.readInt64();
            break;
          }
          case 18: {

            value_ = input.readBytes();
            break;
          }
          default: {
            if (!parseUnknownField(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return dk.s4.phg.messages.interfaces.gatt.GATT.internal_static_s4_messages_interfaces_gatt_WriteCharacteristic_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return dk.s4.phg.messages.interfaces.gatt.GATT.internal_static_s4_messages_interfaces_gatt_WriteCharacteristic_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic.class, dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic.Builder.class);
  }

  public static final int CHARACTERISTIC_FIELD_NUMBER = 1;
  private long characteristic_;
  /**
   * <pre>
   * objId_t 
   * </pre>
   *
   * <code>int64 characteristic = 1;</code>
   */
  public long getCharacteristic() {
    return characteristic_;
  }

  public static final int VALUE_FIELD_NUMBER = 2;
  private com.google.protobuf.ByteString value_;
  /**
   * <pre>
   **
   * The value to write
   * </pre>
   *
   * <code>bytes value = 2;</code>
   */
  public com.google.protobuf.ByteString getValue() {
    return value_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (characteristic_ != 0L) {
      output.writeInt64(1, characteristic_);
    }
    if (!value_.isEmpty()) {
      output.writeBytes(2, value_);
    }
    unknownFields.writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (characteristic_ != 0L) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt64Size(1, characteristic_);
    }
    if (!value_.isEmpty()) {
      size += com.google.protobuf.CodedOutputStream
        .computeBytesSize(2, value_);
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic)) {
      return super.equals(obj);
    }
    dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic other = (dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic) obj;

    if (getCharacteristic()
        != other.getCharacteristic()) return false;
    if (!getValue()
        .equals(other.getValue())) return false;
    if (!unknownFields.equals(other.unknownFields)) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + CHARACTERISTIC_FIELD_NUMBER;
    hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
        getCharacteristic());
    hash = (37 * hash) + VALUE_FIELD_NUMBER;
    hash = (53 * hash) + getValue().hashCode();
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   **
   * Write a characteristic value
   * Covers the GATT sub-procedures "Write Characteristic Value" and
   * "Write Long Characteristic Values". The sub-procedure
   * "Characteristic Values Reliable Write" is currently unsupported by
   * this interface.
   * The response to this function confirms that the write was completed
   * (or reports an error).
   * The following errors may be generated:
   *  1.  Invalid or missing argument - if characteristic is missing or does not represent
   *      a GATT characteristic known to this provider, or if value is missing.
   *  3.  Timeout - the write request timed out
   *  5.  Not permitted - the characteristic cannot be written
   *  6.  Insufficient authentication - the attribute requires
   *      authentication before it can be written
   *  7.  Insufficient authorization - the attribute requires
   *      authorization before it can be written
   *  8.  Insufficient encryption - the attribute requires encryption
   *      before it can be written, or the key size is insufficient
   *  11. Invalid attribute value or length - the attribute value
   *      length is invalid for the operation or the value itself is invalid
   *  12. Operation aborted - if the connection was closed while writing
   *  13. Not connected - the request was sent to a server which is currently
   *      not connected
   *  31. Communication error - any other communication problem occuring
   *      during the operation (which did not abort the connection)
   * </pre>
   *
   * Protobuf type {@code s4.messages.interfaces.gatt.WriteCharacteristic}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:s4.messages.interfaces.gatt.WriteCharacteristic)
      dk.s4.phg.messages.interfaces.gatt.WriteCharacteristicOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return dk.s4.phg.messages.interfaces.gatt.GATT.internal_static_s4_messages_interfaces_gatt_WriteCharacteristic_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return dk.s4.phg.messages.interfaces.gatt.GATT.internal_static_s4_messages_interfaces_gatt_WriteCharacteristic_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic.class, dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic.Builder.class);
    }

    // Construct using dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      characteristic_ = 0L;

      value_ = com.google.protobuf.ByteString.EMPTY;

      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return dk.s4.phg.messages.interfaces.gatt.GATT.internal_static_s4_messages_interfaces_gatt_WriteCharacteristic_descriptor;
    }

    @java.lang.Override
    public dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic getDefaultInstanceForType() {
      return dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic.getDefaultInstance();
    }

    @java.lang.Override
    public dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic build() {
      dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic buildPartial() {
      dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic result = new dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic(this);
      result.characteristic_ = characteristic_;
      result.value_ = value_;
      onBuilt();
      return result;
    }

    @java.lang.Override
    public Builder clone() {
      return super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic) {
        return mergeFrom((dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic other) {
      if (other == dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic.getDefaultInstance()) return this;
      if (other.getCharacteristic() != 0L) {
        setCharacteristic(other.getCharacteristic());
      }
      if (other.getValue() != com.google.protobuf.ByteString.EMPTY) {
        setValue(other.getValue());
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private long characteristic_ ;
    /**
     * <pre>
     * objId_t 
     * </pre>
     *
     * <code>int64 characteristic = 1;</code>
     */
    public long getCharacteristic() {
      return characteristic_;
    }
    /**
     * <pre>
     * objId_t 
     * </pre>
     *
     * <code>int64 characteristic = 1;</code>
     */
    public Builder setCharacteristic(long value) {
      
      characteristic_ = value;
      onChanged();
      return this;
    }
    /**
     * <pre>
     * objId_t 
     * </pre>
     *
     * <code>int64 characteristic = 1;</code>
     */
    public Builder clearCharacteristic() {
      
      characteristic_ = 0L;
      onChanged();
      return this;
    }

    private com.google.protobuf.ByteString value_ = com.google.protobuf.ByteString.EMPTY;
    /**
     * <pre>
     **
     * The value to write
     * </pre>
     *
     * <code>bytes value = 2;</code>
     */
    public com.google.protobuf.ByteString getValue() {
      return value_;
    }
    /**
     * <pre>
     **
     * The value to write
     * </pre>
     *
     * <code>bytes value = 2;</code>
     */
    public Builder setValue(com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      value_ = value;
      onChanged();
      return this;
    }
    /**
     * <pre>
     **
     * The value to write
     * </pre>
     *
     * <code>bytes value = 2;</code>
     */
    public Builder clearValue() {
      
      value_ = getDefaultInstance().getValue();
      onChanged();
      return this;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:s4.messages.interfaces.gatt.WriteCharacteristic)
  }

  // @@protoc_insertion_point(class_scope:s4.messages.interfaces.gatt.WriteCharacteristic)
  private static final dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic();
  }

  public static dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<WriteCharacteristic>
      PARSER = new com.google.protobuf.AbstractParser<WriteCharacteristic>() {
    @java.lang.Override
    public WriteCharacteristic parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new WriteCharacteristic(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<WriteCharacteristic> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<WriteCharacteristic> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public dk.s4.phg.messages.interfaces.gatt.WriteCharacteristic getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

