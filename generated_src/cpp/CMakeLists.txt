cmake_minimum_required(VERSION 3.6.0)
project(PHG-messages)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

set(CMAKE_CXX_STANDARD 11)

file(GLOB PROTO_SRC *.pb.cc)
file(GLOB PROTO_HEADER *.h)

add_library(messages
  ${PROTO_HEADER}
  ${PROTO_SRC}
)

target_link_libraries(messages
  debug protobuf-lited optimized protobuf-lite
)
