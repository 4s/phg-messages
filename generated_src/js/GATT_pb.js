// source: GATT.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

goog.exportSymbol('proto.s4.messages.interfaces.gatt.BTUUID', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.BTUUID.SizeCase', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.C2P', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.C2P.Event', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.C2P.Event.EventCase', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.C2P.PayloadCase', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.C2P.Request', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.C2P.Request.RequestCase', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.Connect', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.ConnectSuccess', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.Disconnect', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.Disconnected', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.Disconnected.Reason', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.DiscoverCharacteristics', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.DiscoverDescriptors', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.DiscoverServices', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.FindIncludedServices', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.GattCharacteristic', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.GattDescriptor', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.GattDescriptor.Type', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.GattDescriptorValue', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.GattDescriptorValue.TypeCase', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.GattService', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.Notification', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.P2C', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.P2C.Event', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.P2C.Event.EventCase', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.P2C.PayloadCase', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.ReadCharacteristic', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.ReadDescriptor', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.SetNotification', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.SetNotificationSuccess', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.WriteCharacteristic', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.WriteDescriptor', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.C2P = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.s4.messages.interfaces.gatt.C2P.oneofGroups_);
};
goog.inherits(proto.s4.messages.interfaces.gatt.C2P, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.C2P.displayName = 'proto.s4.messages.interfaces.gatt.C2P';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.C2P.Request = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.s4.messages.interfaces.gatt.C2P.Request.oneofGroups_);
};
goog.inherits(proto.s4.messages.interfaces.gatt.C2P.Request, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.C2P.Request.displayName = 'proto.s4.messages.interfaces.gatt.C2P.Request';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.C2P.Event = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.s4.messages.interfaces.gatt.C2P.Event.oneofGroups_);
};
goog.inherits(proto.s4.messages.interfaces.gatt.C2P.Event, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.C2P.Event.displayName = 'proto.s4.messages.interfaces.gatt.C2P.Event';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.P2C = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.s4.messages.interfaces.gatt.P2C.oneofGroups_);
};
goog.inherits(proto.s4.messages.interfaces.gatt.P2C, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.P2C.displayName = 'proto.s4.messages.interfaces.gatt.P2C';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.P2C.Event = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.s4.messages.interfaces.gatt.P2C.Event.oneofGroups_);
};
goog.inherits(proto.s4.messages.interfaces.gatt.P2C.Event, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.P2C.Event.displayName = 'proto.s4.messages.interfaces.gatt.P2C.Event';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.BTUUID = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.s4.messages.interfaces.gatt.BTUUID.oneofGroups_);
};
goog.inherits(proto.s4.messages.interfaces.gatt.BTUUID, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.BTUUID.displayName = 'proto.s4.messages.interfaces.gatt.BTUUID';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.GattService = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.GattService, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.GattService.displayName = 'proto.s4.messages.interfaces.gatt.GattService';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.GattCharacteristic, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.GattCharacteristic.displayName = 'proto.s4.messages.interfaces.gatt.GattCharacteristic';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.GattDescriptor = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.GattDescriptor, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.GattDescriptor.displayName = 'proto.s4.messages.interfaces.gatt.GattDescriptor';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.s4.messages.interfaces.gatt.GattDescriptorValue.oneofGroups_);
};
goog.inherits(proto.s4.messages.interfaces.gatt.GattDescriptorValue, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.displayName = 'proto.s4.messages.interfaces.gatt.GattDescriptorValue';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.displayName = 'proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription.displayName = 'proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.displayName = 'proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration.displayName = 'proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.displayName = 'proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.repeatedFields_, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.displayName = 'proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.displayName = 'proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.Connect = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.Connect, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.Connect.displayName = 'proto.s4.messages.interfaces.gatt.Connect';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.ConnectSuccess = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.ConnectSuccess, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.ConnectSuccess.displayName = 'proto.s4.messages.interfaces.gatt.ConnectSuccess';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.Disconnect = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.Disconnect, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.Disconnect.displayName = 'proto.s4.messages.interfaces.gatt.Disconnect';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.Disconnected = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.Disconnected, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.Disconnected.displayName = 'proto.s4.messages.interfaces.gatt.Disconnected';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.DiscoverServices = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.DiscoverServices, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.DiscoverServices.displayName = 'proto.s4.messages.interfaces.gatt.DiscoverServices';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.repeatedFields_, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.displayName = 'proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.FindIncludedServices = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.FindIncludedServices, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.FindIncludedServices.displayName = 'proto.s4.messages.interfaces.gatt.FindIncludedServices';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.repeatedFields_, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.displayName = 'proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristics = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.DiscoverCharacteristics, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.displayName = 'proto.s4.messages.interfaces.gatt.DiscoverCharacteristics';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.repeatedFields_, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.displayName = 'proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptors = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.DiscoverDescriptors, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.DiscoverDescriptors.displayName = 'proto.s4.messages.interfaces.gatt.DiscoverDescriptors';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.repeatedFields_, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.displayName = 'proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristic = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.ReadCharacteristic, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.ReadCharacteristic.displayName = 'proto.s4.messages.interfaces.gatt.ReadCharacteristic';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess.displayName = 'proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristic = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.WriteCharacteristic, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.WriteCharacteristic.displayName = 'proto.s4.messages.interfaces.gatt.WriteCharacteristic';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess.displayName = 'proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.displayName = 'proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess.displayName = 'proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.Notification = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.Notification, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.Notification.displayName = 'proto.s4.messages.interfaces.gatt.Notification';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.ReadDescriptor = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.ReadDescriptor, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.ReadDescriptor.displayName = 'proto.s4.messages.interfaces.gatt.ReadDescriptor';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess.displayName = 'proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.WriteDescriptor = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.WriteDescriptor, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.WriteDescriptor.displayName = 'proto.s4.messages.interfaces.gatt.WriteDescriptor';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess.displayName = 'proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.SetNotification = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.SetNotification, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.SetNotification.displayName = 'proto.s4.messages.interfaces.gatt.SetNotification';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.gatt.SetNotificationSuccess = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.gatt.SetNotificationSuccess, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.gatt.SetNotificationSuccess.displayName = 'proto.s4.messages.interfaces.gatt.SetNotificationSuccess';
}

/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.s4.messages.interfaces.gatt.C2P.oneofGroups_ = [[1,2]];

/**
 * @enum {number}
 */
proto.s4.messages.interfaces.gatt.C2P.PayloadCase = {
  PAYLOAD_NOT_SET: 0,
  REQUEST: 1,
  EVENT: 2
};

/**
 * @return {proto.s4.messages.interfaces.gatt.C2P.PayloadCase}
 */
proto.s4.messages.interfaces.gatt.C2P.prototype.getPayloadCase = function() {
  return /** @type {proto.s4.messages.interfaces.gatt.C2P.PayloadCase} */(jspb.Message.computeOneofCase(this, proto.s4.messages.interfaces.gatt.C2P.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.C2P.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.C2P.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.C2P} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.C2P.toObject = function(includeInstance, msg) {
  var f, obj = {
    request: (f = msg.getRequest()) && proto.s4.messages.interfaces.gatt.C2P.Request.toObject(includeInstance, f),
    event: (f = msg.getEvent()) && proto.s4.messages.interfaces.gatt.C2P.Event.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.C2P}
 */
proto.s4.messages.interfaces.gatt.C2P.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.C2P;
  return proto.s4.messages.interfaces.gatt.C2P.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.C2P} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.C2P}
 */
proto.s4.messages.interfaces.gatt.C2P.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.s4.messages.interfaces.gatt.C2P.Request;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.C2P.Request.deserializeBinaryFromReader);
      msg.setRequest(value);
      break;
    case 2:
      var value = new proto.s4.messages.interfaces.gatt.C2P.Event;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.C2P.Event.deserializeBinaryFromReader);
      msg.setEvent(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.C2P.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.C2P.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.C2P} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.C2P.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRequest();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.s4.messages.interfaces.gatt.C2P.Request.serializeBinaryToWriter
    );
  }
  f = message.getEvent();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.s4.messages.interfaces.gatt.C2P.Event.serializeBinaryToWriter
    );
  }
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.s4.messages.interfaces.gatt.C2P.Request.oneofGroups_ = [[1,2,3,4,5,6,7,8,9,10,11]];

/**
 * @enum {number}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.RequestCase = {
  REQUEST_NOT_SET: 0,
  CONNECT: 1,
  DISCOVER_SERVICES: 2,
  FIND_INCLUDED_SERVICES: 3,
  DISCOVER_CHARACTERISTICS: 4,
  DISCOVER_DESCRIPTORS: 5,
  READ_CHARACTERISTIC: 6,
  WRITE_CHARACTERISTIC: 7,
  WRITE_CHARACTERISTIC_WITHOUT_RESPONSE: 8,
  READ_DESCRIPTOR: 9,
  WRITE_DESCRIPTOR: 10,
  SET_NOTIFICATION: 11
};

/**
 * @return {proto.s4.messages.interfaces.gatt.C2P.Request.RequestCase}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.getRequestCase = function() {
  return /** @type {proto.s4.messages.interfaces.gatt.C2P.Request.RequestCase} */(jspb.Message.computeOneofCase(this, proto.s4.messages.interfaces.gatt.C2P.Request.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.C2P.Request.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.C2P.Request} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.C2P.Request.toObject = function(includeInstance, msg) {
  var f, obj = {
    connect: (f = msg.getConnect()) && proto.s4.messages.interfaces.gatt.Connect.toObject(includeInstance, f),
    discoverServices: (f = msg.getDiscoverServices()) && proto.s4.messages.interfaces.gatt.DiscoverServices.toObject(includeInstance, f),
    findIncludedServices: (f = msg.getFindIncludedServices()) && proto.s4.messages.interfaces.gatt.FindIncludedServices.toObject(includeInstance, f),
    discoverCharacteristics: (f = msg.getDiscoverCharacteristics()) && proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.toObject(includeInstance, f),
    discoverDescriptors: (f = msg.getDiscoverDescriptors()) && proto.s4.messages.interfaces.gatt.DiscoverDescriptors.toObject(includeInstance, f),
    readCharacteristic: (f = msg.getReadCharacteristic()) && proto.s4.messages.interfaces.gatt.ReadCharacteristic.toObject(includeInstance, f),
    writeCharacteristic: (f = msg.getWriteCharacteristic()) && proto.s4.messages.interfaces.gatt.WriteCharacteristic.toObject(includeInstance, f),
    writeCharacteristicWithoutResponse: (f = msg.getWriteCharacteristicWithoutResponse()) && proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.toObject(includeInstance, f),
    readDescriptor: (f = msg.getReadDescriptor()) && proto.s4.messages.interfaces.gatt.ReadDescriptor.toObject(includeInstance, f),
    writeDescriptor: (f = msg.getWriteDescriptor()) && proto.s4.messages.interfaces.gatt.WriteDescriptor.toObject(includeInstance, f),
    setNotification: (f = msg.getSetNotification()) && proto.s4.messages.interfaces.gatt.SetNotification.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.C2P.Request}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.C2P.Request;
  return proto.s4.messages.interfaces.gatt.C2P.Request.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.C2P.Request} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.C2P.Request}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.s4.messages.interfaces.gatt.Connect;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.Connect.deserializeBinaryFromReader);
      msg.setConnect(value);
      break;
    case 2:
      var value = new proto.s4.messages.interfaces.gatt.DiscoverServices;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.DiscoverServices.deserializeBinaryFromReader);
      msg.setDiscoverServices(value);
      break;
    case 3:
      var value = new proto.s4.messages.interfaces.gatt.FindIncludedServices;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.FindIncludedServices.deserializeBinaryFromReader);
      msg.setFindIncludedServices(value);
      break;
    case 4:
      var value = new proto.s4.messages.interfaces.gatt.DiscoverCharacteristics;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.deserializeBinaryFromReader);
      msg.setDiscoverCharacteristics(value);
      break;
    case 5:
      var value = new proto.s4.messages.interfaces.gatt.DiscoverDescriptors;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.DiscoverDescriptors.deserializeBinaryFromReader);
      msg.setDiscoverDescriptors(value);
      break;
    case 6:
      var value = new proto.s4.messages.interfaces.gatt.ReadCharacteristic;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.ReadCharacteristic.deserializeBinaryFromReader);
      msg.setReadCharacteristic(value);
      break;
    case 7:
      var value = new proto.s4.messages.interfaces.gatt.WriteCharacteristic;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.WriteCharacteristic.deserializeBinaryFromReader);
      msg.setWriteCharacteristic(value);
      break;
    case 8:
      var value = new proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.deserializeBinaryFromReader);
      msg.setWriteCharacteristicWithoutResponse(value);
      break;
    case 9:
      var value = new proto.s4.messages.interfaces.gatt.ReadDescriptor;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.ReadDescriptor.deserializeBinaryFromReader);
      msg.setReadDescriptor(value);
      break;
    case 10:
      var value = new proto.s4.messages.interfaces.gatt.WriteDescriptor;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.WriteDescriptor.deserializeBinaryFromReader);
      msg.setWriteDescriptor(value);
      break;
    case 11:
      var value = new proto.s4.messages.interfaces.gatt.SetNotification;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.SetNotification.deserializeBinaryFromReader);
      msg.setSetNotification(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.C2P.Request.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.C2P.Request} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.C2P.Request.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getConnect();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.s4.messages.interfaces.gatt.Connect.serializeBinaryToWriter
    );
  }
  f = message.getDiscoverServices();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.s4.messages.interfaces.gatt.DiscoverServices.serializeBinaryToWriter
    );
  }
  f = message.getFindIncludedServices();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.s4.messages.interfaces.gatt.FindIncludedServices.serializeBinaryToWriter
    );
  }
  f = message.getDiscoverCharacteristics();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.serializeBinaryToWriter
    );
  }
  f = message.getDiscoverDescriptors();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      proto.s4.messages.interfaces.gatt.DiscoverDescriptors.serializeBinaryToWriter
    );
  }
  f = message.getReadCharacteristic();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      proto.s4.messages.interfaces.gatt.ReadCharacteristic.serializeBinaryToWriter
    );
  }
  f = message.getWriteCharacteristic();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      proto.s4.messages.interfaces.gatt.WriteCharacteristic.serializeBinaryToWriter
    );
  }
  f = message.getWriteCharacteristicWithoutResponse();
  if (f != null) {
    writer.writeMessage(
      8,
      f,
      proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.serializeBinaryToWriter
    );
  }
  f = message.getReadDescriptor();
  if (f != null) {
    writer.writeMessage(
      9,
      f,
      proto.s4.messages.interfaces.gatt.ReadDescriptor.serializeBinaryToWriter
    );
  }
  f = message.getWriteDescriptor();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      proto.s4.messages.interfaces.gatt.WriteDescriptor.serializeBinaryToWriter
    );
  }
  f = message.getSetNotification();
  if (f != null) {
    writer.writeMessage(
      11,
      f,
      proto.s4.messages.interfaces.gatt.SetNotification.serializeBinaryToWriter
    );
  }
};


/**
 * optional Connect connect = 1;
 * @return {?proto.s4.messages.interfaces.gatt.Connect}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.getConnect = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.Connect} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.Connect, 1));
};


/** @param {?proto.s4.messages.interfaces.gatt.Connect|undefined} value */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.setConnect = function(value) {
  jspb.Message.setOneofWrapperField(this, 1, proto.s4.messages.interfaces.gatt.C2P.Request.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.clearConnect = function() {
  this.setConnect(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.hasConnect = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional DiscoverServices discover_services = 2;
 * @return {?proto.s4.messages.interfaces.gatt.DiscoverServices}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.getDiscoverServices = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.DiscoverServices} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.DiscoverServices, 2));
};


/** @param {?proto.s4.messages.interfaces.gatt.DiscoverServices|undefined} value */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.setDiscoverServices = function(value) {
  jspb.Message.setOneofWrapperField(this, 2, proto.s4.messages.interfaces.gatt.C2P.Request.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.clearDiscoverServices = function() {
  this.setDiscoverServices(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.hasDiscoverServices = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional FindIncludedServices find_included_services = 3;
 * @return {?proto.s4.messages.interfaces.gatt.FindIncludedServices}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.getFindIncludedServices = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.FindIncludedServices} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.FindIncludedServices, 3));
};


/** @param {?proto.s4.messages.interfaces.gatt.FindIncludedServices|undefined} value */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.setFindIncludedServices = function(value) {
  jspb.Message.setOneofWrapperField(this, 3, proto.s4.messages.interfaces.gatt.C2P.Request.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.clearFindIncludedServices = function() {
  this.setFindIncludedServices(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.hasFindIncludedServices = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional DiscoverCharacteristics discover_characteristics = 4;
 * @return {?proto.s4.messages.interfaces.gatt.DiscoverCharacteristics}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.getDiscoverCharacteristics = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.DiscoverCharacteristics} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.DiscoverCharacteristics, 4));
};


/** @param {?proto.s4.messages.interfaces.gatt.DiscoverCharacteristics|undefined} value */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.setDiscoverCharacteristics = function(value) {
  jspb.Message.setOneofWrapperField(this, 4, proto.s4.messages.interfaces.gatt.C2P.Request.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.clearDiscoverCharacteristics = function() {
  this.setDiscoverCharacteristics(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.hasDiscoverCharacteristics = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional DiscoverDescriptors discover_descriptors = 5;
 * @return {?proto.s4.messages.interfaces.gatt.DiscoverDescriptors}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.getDiscoverDescriptors = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.DiscoverDescriptors} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.DiscoverDescriptors, 5));
};


/** @param {?proto.s4.messages.interfaces.gatt.DiscoverDescriptors|undefined} value */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.setDiscoverDescriptors = function(value) {
  jspb.Message.setOneofWrapperField(this, 5, proto.s4.messages.interfaces.gatt.C2P.Request.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.clearDiscoverDescriptors = function() {
  this.setDiscoverDescriptors(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.hasDiscoverDescriptors = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional ReadCharacteristic read_characteristic = 6;
 * @return {?proto.s4.messages.interfaces.gatt.ReadCharacteristic}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.getReadCharacteristic = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.ReadCharacteristic} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.ReadCharacteristic, 6));
};


/** @param {?proto.s4.messages.interfaces.gatt.ReadCharacteristic|undefined} value */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.setReadCharacteristic = function(value) {
  jspb.Message.setOneofWrapperField(this, 6, proto.s4.messages.interfaces.gatt.C2P.Request.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.clearReadCharacteristic = function() {
  this.setReadCharacteristic(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.hasReadCharacteristic = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional WriteCharacteristic write_characteristic = 7;
 * @return {?proto.s4.messages.interfaces.gatt.WriteCharacteristic}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.getWriteCharacteristic = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.WriteCharacteristic} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.WriteCharacteristic, 7));
};


/** @param {?proto.s4.messages.interfaces.gatt.WriteCharacteristic|undefined} value */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.setWriteCharacteristic = function(value) {
  jspb.Message.setOneofWrapperField(this, 7, proto.s4.messages.interfaces.gatt.C2P.Request.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.clearWriteCharacteristic = function() {
  this.setWriteCharacteristic(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.hasWriteCharacteristic = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional WriteCharacteristicWithoutResponse write_characteristic_without_response = 8;
 * @return {?proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.getWriteCharacteristicWithoutResponse = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse, 8));
};


/** @param {?proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse|undefined} value */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.setWriteCharacteristicWithoutResponse = function(value) {
  jspb.Message.setOneofWrapperField(this, 8, proto.s4.messages.interfaces.gatt.C2P.Request.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.clearWriteCharacteristicWithoutResponse = function() {
  this.setWriteCharacteristicWithoutResponse(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.hasWriteCharacteristicWithoutResponse = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional ReadDescriptor read_descriptor = 9;
 * @return {?proto.s4.messages.interfaces.gatt.ReadDescriptor}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.getReadDescriptor = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.ReadDescriptor} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.ReadDescriptor, 9));
};


/** @param {?proto.s4.messages.interfaces.gatt.ReadDescriptor|undefined} value */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.setReadDescriptor = function(value) {
  jspb.Message.setOneofWrapperField(this, 9, proto.s4.messages.interfaces.gatt.C2P.Request.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.clearReadDescriptor = function() {
  this.setReadDescriptor(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.hasReadDescriptor = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional WriteDescriptor write_descriptor = 10;
 * @return {?proto.s4.messages.interfaces.gatt.WriteDescriptor}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.getWriteDescriptor = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.WriteDescriptor} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.WriteDescriptor, 10));
};


/** @param {?proto.s4.messages.interfaces.gatt.WriteDescriptor|undefined} value */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.setWriteDescriptor = function(value) {
  jspb.Message.setOneofWrapperField(this, 10, proto.s4.messages.interfaces.gatt.C2P.Request.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.clearWriteDescriptor = function() {
  this.setWriteDescriptor(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.hasWriteDescriptor = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional SetNotification set_notification = 11;
 * @return {?proto.s4.messages.interfaces.gatt.SetNotification}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.getSetNotification = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.SetNotification} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.SetNotification, 11));
};


/** @param {?proto.s4.messages.interfaces.gatt.SetNotification|undefined} value */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.setSetNotification = function(value) {
  jspb.Message.setOneofWrapperField(this, 11, proto.s4.messages.interfaces.gatt.C2P.Request.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.clearSetNotification = function() {
  this.setSetNotification(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.C2P.Request.prototype.hasSetNotification = function() {
  return jspb.Message.getField(this, 11) != null;
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.s4.messages.interfaces.gatt.C2P.Event.oneofGroups_ = [[1]];

/**
 * @enum {number}
 */
proto.s4.messages.interfaces.gatt.C2P.Event.EventCase = {
  EVENT_NOT_SET: 0,
  DISCONNECT: 1
};

/**
 * @return {proto.s4.messages.interfaces.gatt.C2P.Event.EventCase}
 */
proto.s4.messages.interfaces.gatt.C2P.Event.prototype.getEventCase = function() {
  return /** @type {proto.s4.messages.interfaces.gatt.C2P.Event.EventCase} */(jspb.Message.computeOneofCase(this, proto.s4.messages.interfaces.gatt.C2P.Event.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.C2P.Event.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.C2P.Event.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.C2P.Event} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.C2P.Event.toObject = function(includeInstance, msg) {
  var f, obj = {
    disconnect: (f = msg.getDisconnect()) && proto.s4.messages.interfaces.gatt.Disconnect.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.C2P.Event}
 */
proto.s4.messages.interfaces.gatt.C2P.Event.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.C2P.Event;
  return proto.s4.messages.interfaces.gatt.C2P.Event.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.C2P.Event} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.C2P.Event}
 */
proto.s4.messages.interfaces.gatt.C2P.Event.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.s4.messages.interfaces.gatt.Disconnect;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.Disconnect.deserializeBinaryFromReader);
      msg.setDisconnect(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.C2P.Event.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.C2P.Event.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.C2P.Event} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.C2P.Event.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getDisconnect();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.s4.messages.interfaces.gatt.Disconnect.serializeBinaryToWriter
    );
  }
};


/**
 * optional Disconnect disconnect = 1;
 * @return {?proto.s4.messages.interfaces.gatt.Disconnect}
 */
proto.s4.messages.interfaces.gatt.C2P.Event.prototype.getDisconnect = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.Disconnect} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.Disconnect, 1));
};


/** @param {?proto.s4.messages.interfaces.gatt.Disconnect|undefined} value */
proto.s4.messages.interfaces.gatt.C2P.Event.prototype.setDisconnect = function(value) {
  jspb.Message.setOneofWrapperField(this, 1, proto.s4.messages.interfaces.gatt.C2P.Event.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.C2P.Event.prototype.clearDisconnect = function() {
  this.setDisconnect(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.C2P.Event.prototype.hasDisconnect = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional Request request = 1;
 * @return {?proto.s4.messages.interfaces.gatt.C2P.Request}
 */
proto.s4.messages.interfaces.gatt.C2P.prototype.getRequest = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.C2P.Request} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.C2P.Request, 1));
};


/** @param {?proto.s4.messages.interfaces.gatt.C2P.Request|undefined} value */
proto.s4.messages.interfaces.gatt.C2P.prototype.setRequest = function(value) {
  jspb.Message.setOneofWrapperField(this, 1, proto.s4.messages.interfaces.gatt.C2P.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.C2P.prototype.clearRequest = function() {
  this.setRequest(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.C2P.prototype.hasRequest = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional Event event = 2;
 * @return {?proto.s4.messages.interfaces.gatt.C2P.Event}
 */
proto.s4.messages.interfaces.gatt.C2P.prototype.getEvent = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.C2P.Event} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.C2P.Event, 2));
};


/** @param {?proto.s4.messages.interfaces.gatt.C2P.Event|undefined} value */
proto.s4.messages.interfaces.gatt.C2P.prototype.setEvent = function(value) {
  jspb.Message.setOneofWrapperField(this, 2, proto.s4.messages.interfaces.gatt.C2P.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.C2P.prototype.clearEvent = function() {
  this.setEvent(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.C2P.prototype.hasEvent = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.s4.messages.interfaces.gatt.P2C.oneofGroups_ = [[2]];

/**
 * @enum {number}
 */
proto.s4.messages.interfaces.gatt.P2C.PayloadCase = {
  PAYLOAD_NOT_SET: 0,
  EVENT: 2
};

/**
 * @return {proto.s4.messages.interfaces.gatt.P2C.PayloadCase}
 */
proto.s4.messages.interfaces.gatt.P2C.prototype.getPayloadCase = function() {
  return /** @type {proto.s4.messages.interfaces.gatt.P2C.PayloadCase} */(jspb.Message.computeOneofCase(this, proto.s4.messages.interfaces.gatt.P2C.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.P2C.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.P2C.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.P2C} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.P2C.toObject = function(includeInstance, msg) {
  var f, obj = {
    event: (f = msg.getEvent()) && proto.s4.messages.interfaces.gatt.P2C.Event.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.P2C}
 */
proto.s4.messages.interfaces.gatt.P2C.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.P2C;
  return proto.s4.messages.interfaces.gatt.P2C.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.P2C} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.P2C}
 */
proto.s4.messages.interfaces.gatt.P2C.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 2:
      var value = new proto.s4.messages.interfaces.gatt.P2C.Event;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.P2C.Event.deserializeBinaryFromReader);
      msg.setEvent(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.P2C.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.P2C.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.P2C} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.P2C.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEvent();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.s4.messages.interfaces.gatt.P2C.Event.serializeBinaryToWriter
    );
  }
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.s4.messages.interfaces.gatt.P2C.Event.oneofGroups_ = [[1,2]];

/**
 * @enum {number}
 */
proto.s4.messages.interfaces.gatt.P2C.Event.EventCase = {
  EVENT_NOT_SET: 0,
  DISCONNECTED: 1,
  NOTIFICATION: 2
};

/**
 * @return {proto.s4.messages.interfaces.gatt.P2C.Event.EventCase}
 */
proto.s4.messages.interfaces.gatt.P2C.Event.prototype.getEventCase = function() {
  return /** @type {proto.s4.messages.interfaces.gatt.P2C.Event.EventCase} */(jspb.Message.computeOneofCase(this, proto.s4.messages.interfaces.gatt.P2C.Event.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.P2C.Event.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.P2C.Event.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.P2C.Event} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.P2C.Event.toObject = function(includeInstance, msg) {
  var f, obj = {
    disconnected: (f = msg.getDisconnected()) && proto.s4.messages.interfaces.gatt.Disconnected.toObject(includeInstance, f),
    notification: (f = msg.getNotification()) && proto.s4.messages.interfaces.gatt.Notification.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.P2C.Event}
 */
proto.s4.messages.interfaces.gatt.P2C.Event.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.P2C.Event;
  return proto.s4.messages.interfaces.gatt.P2C.Event.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.P2C.Event} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.P2C.Event}
 */
proto.s4.messages.interfaces.gatt.P2C.Event.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.s4.messages.interfaces.gatt.Disconnected;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.Disconnected.deserializeBinaryFromReader);
      msg.setDisconnected(value);
      break;
    case 2:
      var value = new proto.s4.messages.interfaces.gatt.Notification;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.Notification.deserializeBinaryFromReader);
      msg.setNotification(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.P2C.Event.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.P2C.Event.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.P2C.Event} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.P2C.Event.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getDisconnected();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.s4.messages.interfaces.gatt.Disconnected.serializeBinaryToWriter
    );
  }
  f = message.getNotification();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.s4.messages.interfaces.gatt.Notification.serializeBinaryToWriter
    );
  }
};


/**
 * optional Disconnected disconnected = 1;
 * @return {?proto.s4.messages.interfaces.gatt.Disconnected}
 */
proto.s4.messages.interfaces.gatt.P2C.Event.prototype.getDisconnected = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.Disconnected} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.Disconnected, 1));
};


/** @param {?proto.s4.messages.interfaces.gatt.Disconnected|undefined} value */
proto.s4.messages.interfaces.gatt.P2C.Event.prototype.setDisconnected = function(value) {
  jspb.Message.setOneofWrapperField(this, 1, proto.s4.messages.interfaces.gatt.P2C.Event.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.P2C.Event.prototype.clearDisconnected = function() {
  this.setDisconnected(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.P2C.Event.prototype.hasDisconnected = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional Notification notification = 2;
 * @return {?proto.s4.messages.interfaces.gatt.Notification}
 */
proto.s4.messages.interfaces.gatt.P2C.Event.prototype.getNotification = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.Notification} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.Notification, 2));
};


/** @param {?proto.s4.messages.interfaces.gatt.Notification|undefined} value */
proto.s4.messages.interfaces.gatt.P2C.Event.prototype.setNotification = function(value) {
  jspb.Message.setOneofWrapperField(this, 2, proto.s4.messages.interfaces.gatt.P2C.Event.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.P2C.Event.prototype.clearNotification = function() {
  this.setNotification(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.P2C.Event.prototype.hasNotification = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional Event event = 2;
 * @return {?proto.s4.messages.interfaces.gatt.P2C.Event}
 */
proto.s4.messages.interfaces.gatt.P2C.prototype.getEvent = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.P2C.Event} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.P2C.Event, 2));
};


/** @param {?proto.s4.messages.interfaces.gatt.P2C.Event|undefined} value */
proto.s4.messages.interfaces.gatt.P2C.prototype.setEvent = function(value) {
  jspb.Message.setOneofWrapperField(this, 2, proto.s4.messages.interfaces.gatt.P2C.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.P2C.prototype.clearEvent = function() {
  this.setEvent(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.P2C.prototype.hasEvent = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.s4.messages.interfaces.gatt.BTUUID.oneofGroups_ = [[1,2]];

/**
 * @enum {number}
 */
proto.s4.messages.interfaces.gatt.BTUUID.SizeCase = {
  SIZE_NOT_SET: 0,
  SHORT: 1,
  LONG: 2
};

/**
 * @return {proto.s4.messages.interfaces.gatt.BTUUID.SizeCase}
 */
proto.s4.messages.interfaces.gatt.BTUUID.prototype.getSizeCase = function() {
  return /** @type {proto.s4.messages.interfaces.gatt.BTUUID.SizeCase} */(jspb.Message.computeOneofCase(this, proto.s4.messages.interfaces.gatt.BTUUID.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.BTUUID.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.BTUUID.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.BTUUID} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.BTUUID.toObject = function(includeInstance, msg) {
  var f, obj = {
    pb_short: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pb_long: msg.getLong_asB64()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.BTUUID}
 */
proto.s4.messages.interfaces.gatt.BTUUID.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.BTUUID;
  return proto.s4.messages.interfaces.gatt.BTUUID.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.BTUUID} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.BTUUID}
 */
proto.s4.messages.interfaces.gatt.BTUUID.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setShort(value);
      break;
    case 2:
      var value = /** @type {!Uint8Array} */ (reader.readBytes());
      msg.setLong(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.BTUUID.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.BTUUID.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.BTUUID} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.BTUUID.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {number} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeUint32(
      1,
      f
    );
  }
  f = /** @type {!(string|Uint8Array)} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeBytes(
      2,
      f
    );
  }
};


/**
 * optional uint32 short = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.BTUUID.prototype.getShort = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.BTUUID.prototype.setShort = function(value) {
  jspb.Message.setOneofField(this, 1, proto.s4.messages.interfaces.gatt.BTUUID.oneofGroups_[0], value);
};


/**
 * Clears the field making it undefined.
 */
proto.s4.messages.interfaces.gatt.BTUUID.prototype.clearShort = function() {
  jspb.Message.setOneofField(this, 1, proto.s4.messages.interfaces.gatt.BTUUID.oneofGroups_[0], undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.BTUUID.prototype.hasShort = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional bytes long = 2;
 * @return {!(string|Uint8Array)}
 */
proto.s4.messages.interfaces.gatt.BTUUID.prototype.getLong = function() {
  return /** @type {!(string|Uint8Array)} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * optional bytes long = 2;
 * This is a type-conversion wrapper around `getLong()`
 * @return {string}
 */
proto.s4.messages.interfaces.gatt.BTUUID.prototype.getLong_asB64 = function() {
  return /** @type {string} */ (jspb.Message.bytesAsB64(
      this.getLong()));
};


/**
 * optional bytes long = 2;
 * Note that Uint8Array is not supported on all browsers.
 * @see http://caniuse.com/Uint8Array
 * This is a type-conversion wrapper around `getLong()`
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.BTUUID.prototype.getLong_asU8 = function() {
  return /** @type {!Uint8Array} */ (jspb.Message.bytesAsU8(
      this.getLong()));
};


/** @param {!(string|Uint8Array)} value */
proto.s4.messages.interfaces.gatt.BTUUID.prototype.setLong = function(value) {
  jspb.Message.setOneofField(this, 2, proto.s4.messages.interfaces.gatt.BTUUID.oneofGroups_[0], value);
};


/**
 * Clears the field making it undefined.
 */
proto.s4.messages.interfaces.gatt.BTUUID.prototype.clearLong = function() {
  jspb.Message.setOneofField(this, 2, proto.s4.messages.interfaces.gatt.BTUUID.oneofGroups_[0], undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.BTUUID.prototype.hasLong = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.GattService.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.GattService.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.GattService} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattService.toObject = function(includeInstance, msg) {
  var f, obj = {
    handle: jspb.Message.getFieldWithDefault(msg, 1, 0),
    uuid: (f = msg.getUuid()) && proto.s4.messages.interfaces.gatt.BTUUID.toObject(includeInstance, f),
    primary: jspb.Message.getBooleanFieldWithDefault(msg, 3, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.GattService}
 */
proto.s4.messages.interfaces.gatt.GattService.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.GattService;
  return proto.s4.messages.interfaces.gatt.GattService.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.GattService} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.GattService}
 */
proto.s4.messages.interfaces.gatt.GattService.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setHandle(value);
      break;
    case 2:
      var value = new proto.s4.messages.interfaces.gatt.BTUUID;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.BTUUID.deserializeBinaryFromReader);
      msg.setUuid(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setPrimary(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.GattService.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.GattService.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.GattService} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattService.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getHandle();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getUuid();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.s4.messages.interfaces.gatt.BTUUID.serializeBinaryToWriter
    );
  }
  f = message.getPrimary();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
};


/**
 * optional int64 handle = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.GattService.prototype.getHandle = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.GattService.prototype.setHandle = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional BTUUID uuid = 2;
 * @return {?proto.s4.messages.interfaces.gatt.BTUUID}
 */
proto.s4.messages.interfaces.gatt.GattService.prototype.getUuid = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.BTUUID} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.BTUUID, 2));
};


/** @param {?proto.s4.messages.interfaces.gatt.BTUUID|undefined} value */
proto.s4.messages.interfaces.gatt.GattService.prototype.setUuid = function(value) {
  jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.GattService.prototype.clearUuid = function() {
  this.setUuid(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattService.prototype.hasUuid = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional bool primary = 3;
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattService.prototype.getPrimary = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/** @param {boolean} value */
proto.s4.messages.interfaces.gatt.GattService.prototype.setPrimary = function(value) {
  jspb.Message.setProto3BooleanField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.GattCharacteristic.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.GattCharacteristic} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.toObject = function(includeInstance, msg) {
  var f, obj = {
    handle: jspb.Message.getFieldWithDefault(msg, 1, 0),
    uuid: (f = msg.getUuid()) && proto.s4.messages.interfaces.gatt.BTUUID.toObject(includeInstance, f),
    broadcast: jspb.Message.getBooleanFieldWithDefault(msg, 3, false),
    read: jspb.Message.getBooleanFieldWithDefault(msg, 4, false),
    writeWithoutResponse: jspb.Message.getBooleanFieldWithDefault(msg, 5, false),
    write: jspb.Message.getBooleanFieldWithDefault(msg, 6, false),
    notify: jspb.Message.getBooleanFieldWithDefault(msg, 7, false),
    indicate: jspb.Message.getBooleanFieldWithDefault(msg, 8, false),
    authenticatedSignedWrite: jspb.Message.getBooleanFieldWithDefault(msg, 9, false),
    extendedProperties: jspb.Message.getBooleanFieldWithDefault(msg, 10, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.GattCharacteristic}
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.GattCharacteristic;
  return proto.s4.messages.interfaces.gatt.GattCharacteristic.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.GattCharacteristic} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.GattCharacteristic}
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setHandle(value);
      break;
    case 2:
      var value = new proto.s4.messages.interfaces.gatt.BTUUID;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.BTUUID.deserializeBinaryFromReader);
      msg.setUuid(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setBroadcast(value);
      break;
    case 4:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setRead(value);
      break;
    case 5:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setWriteWithoutResponse(value);
      break;
    case 6:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setWrite(value);
      break;
    case 7:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setNotify(value);
      break;
    case 8:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setIndicate(value);
      break;
    case 9:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setAuthenticatedSignedWrite(value);
      break;
    case 10:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setExtendedProperties(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.GattCharacteristic.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.GattCharacteristic} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getHandle();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getUuid();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.s4.messages.interfaces.gatt.BTUUID.serializeBinaryToWriter
    );
  }
  f = message.getBroadcast();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
  f = message.getRead();
  if (f) {
    writer.writeBool(
      4,
      f
    );
  }
  f = message.getWriteWithoutResponse();
  if (f) {
    writer.writeBool(
      5,
      f
    );
  }
  f = message.getWrite();
  if (f) {
    writer.writeBool(
      6,
      f
    );
  }
  f = message.getNotify();
  if (f) {
    writer.writeBool(
      7,
      f
    );
  }
  f = message.getIndicate();
  if (f) {
    writer.writeBool(
      8,
      f
    );
  }
  f = message.getAuthenticatedSignedWrite();
  if (f) {
    writer.writeBool(
      9,
      f
    );
  }
  f = message.getExtendedProperties();
  if (f) {
    writer.writeBool(
      10,
      f
    );
  }
};


/**
 * optional int64 handle = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.getHandle = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.setHandle = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional BTUUID uuid = 2;
 * @return {?proto.s4.messages.interfaces.gatt.BTUUID}
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.getUuid = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.BTUUID} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.BTUUID, 2));
};


/** @param {?proto.s4.messages.interfaces.gatt.BTUUID|undefined} value */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.setUuid = function(value) {
  jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.clearUuid = function() {
  this.setUuid(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.hasUuid = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional bool broadcast = 3;
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.getBroadcast = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/** @param {boolean} value */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.setBroadcast = function(value) {
  jspb.Message.setProto3BooleanField(this, 3, value);
};


/**
 * optional bool read = 4;
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.getRead = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 4, false));
};


/** @param {boolean} value */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.setRead = function(value) {
  jspb.Message.setProto3BooleanField(this, 4, value);
};


/**
 * optional bool write_without_response = 5;
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.getWriteWithoutResponse = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 5, false));
};


/** @param {boolean} value */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.setWriteWithoutResponse = function(value) {
  jspb.Message.setProto3BooleanField(this, 5, value);
};


/**
 * optional bool write = 6;
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.getWrite = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 6, false));
};


/** @param {boolean} value */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.setWrite = function(value) {
  jspb.Message.setProto3BooleanField(this, 6, value);
};


/**
 * optional bool notify = 7;
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.getNotify = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 7, false));
};


/** @param {boolean} value */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.setNotify = function(value) {
  jspb.Message.setProto3BooleanField(this, 7, value);
};


/**
 * optional bool indicate = 8;
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.getIndicate = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 8, false));
};


/** @param {boolean} value */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.setIndicate = function(value) {
  jspb.Message.setProto3BooleanField(this, 8, value);
};


/**
 * optional bool authenticated_signed_write = 9;
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.getAuthenticatedSignedWrite = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 9, false));
};


/** @param {boolean} value */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.setAuthenticatedSignedWrite = function(value) {
  jspb.Message.setProto3BooleanField(this, 9, value);
};


/**
 * optional bool extended_properties = 10;
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.getExtendedProperties = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 10, false));
};


/** @param {boolean} value */
proto.s4.messages.interfaces.gatt.GattCharacteristic.prototype.setExtendedProperties = function(value) {
  jspb.Message.setProto3BooleanField(this, 10, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.GattDescriptor.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.GattDescriptor.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptor} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptor.toObject = function(includeInstance, msg) {
  var f, obj = {
    handle: jspb.Message.getFieldWithDefault(msg, 1, 0),
    type: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptor}
 */
proto.s4.messages.interfaces.gatt.GattDescriptor.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.GattDescriptor;
  return proto.s4.messages.interfaces.gatt.GattDescriptor.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptor} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptor}
 */
proto.s4.messages.interfaces.gatt.GattDescriptor.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setHandle(value);
      break;
    case 2:
      var value = /** @type {!proto.s4.messages.interfaces.gatt.GattDescriptor.Type} */ (reader.readEnum());
      msg.setType(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.GattDescriptor.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.GattDescriptor.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptor} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptor.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getHandle();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getType();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.s4.messages.interfaces.gatt.GattDescriptor.Type = {
  CHARACTERISTIC_EXTENDED_PROPERTIES: 0,
  CHARACTERISTIC_USER_DESCRIPTION: 1,
  CLIENT_CHARACTERISTIC_CONFIGURATION: 2,
  SERVER_CHARACTERISTIC_CONFIGURATION: 3,
  CHARACTERISTIC_PRESENTATION_FORMAT: 4,
  CHARACTERISTIC_AGGREGATE_FORMAT: 5,
  USER_DEFINED_DESCRIPTOR: 6
};

/**
 * optional int64 handle = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.GattDescriptor.prototype.getHandle = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.GattDescriptor.prototype.setHandle = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional Type type = 2;
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptor.Type}
 */
proto.s4.messages.interfaces.gatt.GattDescriptor.prototype.getType = function() {
  return /** @type {!proto.s4.messages.interfaces.gatt.GattDescriptor.Type} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/** @param {!proto.s4.messages.interfaces.gatt.GattDescriptor.Type} value */
proto.s4.messages.interfaces.gatt.GattDescriptor.prototype.setType = function(value) {
  jspb.Message.setProto3EnumField(this, 2, value);
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.oneofGroups_ = [[1,2,3,4,5,6,7]];

/**
 * @enum {number}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.TypeCase = {
  TYPE_NOT_SET: 0,
  CHARACTERISTIC_EXTENDED_PROPERTIES: 1,
  CHARACTERISTIC_USER_DESCRIPTION: 2,
  CLIENT_CHARACTERISTIC_CONFIGURATION: 3,
  SERVER_CHARACTERISTIC_CONFIGURATION: 4,
  CHARACTERISTIC_PRESENTATION_FORMAT: 5,
  CHARACTERISTIC_AGGREGATE_FORMAT: 6,
  USER_DEFINED_DESCRIPTOR: 7
};

/**
 * @return {proto.s4.messages.interfaces.gatt.GattDescriptorValue.TypeCase}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.getTypeCase = function() {
  return /** @type {proto.s4.messages.interfaces.gatt.GattDescriptorValue.TypeCase} */(jspb.Message.computeOneofCase(this, proto.s4.messages.interfaces.gatt.GattDescriptorValue.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.toObject = function(includeInstance, msg) {
  var f, obj = {
    characteristicExtendedProperties: (f = msg.getCharacteristicExtendedProperties()) && proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.toObject(includeInstance, f),
    characteristicUserDescription: (f = msg.getCharacteristicUserDescription()) && proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription.toObject(includeInstance, f),
    clientCharacteristicConfiguration: (f = msg.getClientCharacteristicConfiguration()) && proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.toObject(includeInstance, f),
    serverCharacteristicConfiguration: (f = msg.getServerCharacteristicConfiguration()) && proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration.toObject(includeInstance, f),
    characteristicPresentationFormat: (f = msg.getCharacteristicPresentationFormat()) && proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.toObject(includeInstance, f),
    characteristicAggregateFormat: (f = msg.getCharacteristicAggregateFormat()) && proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.toObject(includeInstance, f),
    userDefinedDescriptor: (f = msg.getUserDefinedDescriptor()) && proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.GattDescriptorValue;
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.deserializeBinaryFromReader);
      msg.setCharacteristicExtendedProperties(value);
      break;
    case 2:
      var value = new proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription.deserializeBinaryFromReader);
      msg.setCharacteristicUserDescription(value);
      break;
    case 3:
      var value = new proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.deserializeBinaryFromReader);
      msg.setClientCharacteristicConfiguration(value);
      break;
    case 4:
      var value = new proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration.deserializeBinaryFromReader);
      msg.setServerCharacteristicConfiguration(value);
      break;
    case 5:
      var value = new proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.deserializeBinaryFromReader);
      msg.setCharacteristicPresentationFormat(value);
      break;
    case 6:
      var value = new proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.deserializeBinaryFromReader);
      msg.setCharacteristicAggregateFormat(value);
      break;
    case 7:
      var value = new proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.deserializeBinaryFromReader);
      msg.setUserDefinedDescriptor(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCharacteristicExtendedProperties();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.serializeBinaryToWriter
    );
  }
  f = message.getCharacteristicUserDescription();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription.serializeBinaryToWriter
    );
  }
  f = message.getClientCharacteristicConfiguration();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.serializeBinaryToWriter
    );
  }
  f = message.getServerCharacteristicConfiguration();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration.serializeBinaryToWriter
    );
  }
  f = message.getCharacteristicPresentationFormat();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.serializeBinaryToWriter
    );
  }
  f = message.getCharacteristicAggregateFormat();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.serializeBinaryToWriter
    );
  }
  f = message.getUserDefinedDescriptor();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.serializeBinaryToWriter
    );
  }
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.toObject = function(includeInstance, msg) {
  var f, obj = {
    reliableWrite: jspb.Message.getBooleanFieldWithDefault(msg, 1, false),
    writableAuxiliaries: jspb.Message.getBooleanFieldWithDefault(msg, 2, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties;
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setReliableWrite(value);
      break;
    case 2:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setWritableAuxiliaries(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getReliableWrite();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
  f = message.getWritableAuxiliaries();
  if (f) {
    writer.writeBool(
      2,
      f
    );
  }
};


/**
 * optional bool reliable_write = 1;
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.prototype.getReliableWrite = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/** @param {boolean} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.prototype.setReliableWrite = function(value) {
  jspb.Message.setProto3BooleanField(this, 1, value);
};


/**
 * optional bool writable_auxiliaries = 2;
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.prototype.getWritableAuxiliaries = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 2, false));
};


/** @param {boolean} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties.prototype.setWritableAuxiliaries = function(value) {
  jspb.Message.setProto3BooleanField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription.toObject = function(includeInstance, msg) {
  var f, obj = {
    userDescription: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription;
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserDescription(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserDescription();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string user_description = 1;
 * @return {string}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription.prototype.getUserDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/** @param {string} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription.prototype.setUserDescription = function(value) {
  jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.toObject = function(includeInstance, msg) {
  var f, obj = {
    notification: jspb.Message.getBooleanFieldWithDefault(msg, 1, false),
    indication: jspb.Message.getBooleanFieldWithDefault(msg, 2, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration;
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setNotification(value);
      break;
    case 2:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setIndication(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getNotification();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
  f = message.getIndication();
  if (f) {
    writer.writeBool(
      2,
      f
    );
  }
};


/**
 * optional bool notification = 1;
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.prototype.getNotification = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/** @param {boolean} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.prototype.setNotification = function(value) {
  jspb.Message.setProto3BooleanField(this, 1, value);
};


/**
 * optional bool indication = 2;
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.prototype.getIndication = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 2, false));
};


/** @param {boolean} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration.prototype.setIndication = function(value) {
  jspb.Message.setProto3BooleanField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration.toObject = function(includeInstance, msg) {
  var f, obj = {
    broadcast: jspb.Message.getBooleanFieldWithDefault(msg, 1, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration;
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setBroadcast(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getBroadcast();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
};


/**
 * optional bool broadcast = 1;
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration.prototype.getBroadcast = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/** @param {boolean} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration.prototype.setBroadcast = function(value) {
  jspb.Message.setProto3BooleanField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.toObject = function(includeInstance, msg) {
  var f, obj = {
    format: msg.getFormat_asB64()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat;
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!Uint8Array} */ (reader.readBytes());
      msg.setFormat(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFormat_asU8();
  if (f.length > 0) {
    writer.writeBytes(
      1,
      f
    );
  }
};


/**
 * optional bytes format = 1;
 * @return {!(string|Uint8Array)}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.prototype.getFormat = function() {
  return /** @type {!(string|Uint8Array)} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * optional bytes format = 1;
 * This is a type-conversion wrapper around `getFormat()`
 * @return {string}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.prototype.getFormat_asB64 = function() {
  return /** @type {string} */ (jspb.Message.bytesAsB64(
      this.getFormat()));
};


/**
 * optional bytes format = 1;
 * Note that Uint8Array is not supported on all browsers.
 * @see http://caniuse.com/Uint8Array
 * This is a type-conversion wrapper around `getFormat()`
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.prototype.getFormat_asU8 = function() {
  return /** @type {!Uint8Array} */ (jspb.Message.bytesAsU8(
      this.getFormat()));
};


/** @param {!(string|Uint8Array)} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat.prototype.setFormat = function(value) {
  jspb.Message.setProto3BytesField(this, 1, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.toObject = function(includeInstance, msg) {
  var f, obj = {
    descriptorsList: (f = jspb.Message.getRepeatedField(msg, 1)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat;
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setDescriptorsList(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getDescriptorsList();
  if (f.length > 0) {
    writer.writePackedInt64(
      1,
      f
    );
  }
};


/**
 * repeated int64 descriptors = 1;
 * @return {!Array<number>}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.prototype.getDescriptorsList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 1));
};


/** @param {!Array<number>} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.prototype.setDescriptorsList = function(value) {
  jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.prototype.addDescriptors = function(value, opt_index) {
  jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat.prototype.clearDescriptorsList = function() {
  this.setDescriptorsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.toObject = function(includeInstance, msg) {
  var f, obj = {
    type: (f = msg.getType()) && proto.s4.messages.interfaces.gatt.BTUUID.toObject(includeInstance, f),
    value: msg.getValue_asB64()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor;
  return proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.s4.messages.interfaces.gatt.BTUUID;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.BTUUID.deserializeBinaryFromReader);
      msg.setType(value);
      break;
    case 2:
      var value = /** @type {!Uint8Array} */ (reader.readBytes());
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getType();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.s4.messages.interfaces.gatt.BTUUID.serializeBinaryToWriter
    );
  }
  f = message.getValue_asU8();
  if (f.length > 0) {
    writer.writeBytes(
      2,
      f
    );
  }
};


/**
 * optional BTUUID type = 1;
 * @return {?proto.s4.messages.interfaces.gatt.BTUUID}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.prototype.getType = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.BTUUID} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.BTUUID, 1));
};


/** @param {?proto.s4.messages.interfaces.gatt.BTUUID|undefined} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.prototype.setType = function(value) {
  jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.prototype.clearType = function() {
  this.setType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.prototype.hasType = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional bytes value = 2;
 * @return {!(string|Uint8Array)}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.prototype.getValue = function() {
  return /** @type {!(string|Uint8Array)} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * optional bytes value = 2;
 * This is a type-conversion wrapper around `getValue()`
 * @return {string}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.prototype.getValue_asB64 = function() {
  return /** @type {string} */ (jspb.Message.bytesAsB64(
      this.getValue()));
};


/**
 * optional bytes value = 2;
 * Note that Uint8Array is not supported on all browsers.
 * @see http://caniuse.com/Uint8Array
 * This is a type-conversion wrapper around `getValue()`
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.prototype.getValue_asU8 = function() {
  return /** @type {!Uint8Array} */ (jspb.Message.bytesAsU8(
      this.getValue()));
};


/** @param {!(string|Uint8Array)} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor.prototype.setValue = function(value) {
  jspb.Message.setProto3BytesField(this, 2, value);
};


/**
 * optional CharacteristicExtendedProperties characteristic_extended_properties = 1;
 * @return {?proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.getCharacteristicExtendedProperties = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties, 1));
};


/** @param {?proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicExtendedProperties|undefined} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.setCharacteristicExtendedProperties = function(value) {
  jspb.Message.setOneofWrapperField(this, 1, proto.s4.messages.interfaces.gatt.GattDescriptorValue.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.clearCharacteristicExtendedProperties = function() {
  this.setCharacteristicExtendedProperties(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.hasCharacteristicExtendedProperties = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional CharacteristicUserDescription characteristic_user_description = 2;
 * @return {?proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.getCharacteristicUserDescription = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription, 2));
};


/** @param {?proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicUserDescription|undefined} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.setCharacteristicUserDescription = function(value) {
  jspb.Message.setOneofWrapperField(this, 2, proto.s4.messages.interfaces.gatt.GattDescriptorValue.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.clearCharacteristicUserDescription = function() {
  this.setCharacteristicUserDescription(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.hasCharacteristicUserDescription = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional ClientCharacteristicConfiguration client_characteristic_configuration = 3;
 * @return {?proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.getClientCharacteristicConfiguration = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration, 3));
};


/** @param {?proto.s4.messages.interfaces.gatt.GattDescriptorValue.ClientCharacteristicConfiguration|undefined} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.setClientCharacteristicConfiguration = function(value) {
  jspb.Message.setOneofWrapperField(this, 3, proto.s4.messages.interfaces.gatt.GattDescriptorValue.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.clearClientCharacteristicConfiguration = function() {
  this.setClientCharacteristicConfiguration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.hasClientCharacteristicConfiguration = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional ServerCharacteristicConfiguration server_characteristic_configuration = 4;
 * @return {?proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.getServerCharacteristicConfiguration = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration, 4));
};


/** @param {?proto.s4.messages.interfaces.gatt.GattDescriptorValue.ServerCharacteristicConfiguration|undefined} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.setServerCharacteristicConfiguration = function(value) {
  jspb.Message.setOneofWrapperField(this, 4, proto.s4.messages.interfaces.gatt.GattDescriptorValue.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.clearServerCharacteristicConfiguration = function() {
  this.setServerCharacteristicConfiguration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.hasServerCharacteristicConfiguration = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional CharacteristicPresentationFormat characteristic_presentation_format = 5;
 * @return {?proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.getCharacteristicPresentationFormat = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat, 5));
};


/** @param {?proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicPresentationFormat|undefined} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.setCharacteristicPresentationFormat = function(value) {
  jspb.Message.setOneofWrapperField(this, 5, proto.s4.messages.interfaces.gatt.GattDescriptorValue.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.clearCharacteristicPresentationFormat = function() {
  this.setCharacteristicPresentationFormat(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.hasCharacteristicPresentationFormat = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional CharacteristicAggregateFormat characteristic_aggregate_format = 6;
 * @return {?proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.getCharacteristicAggregateFormat = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat, 6));
};


/** @param {?proto.s4.messages.interfaces.gatt.GattDescriptorValue.CharacteristicAggregateFormat|undefined} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.setCharacteristicAggregateFormat = function(value) {
  jspb.Message.setOneofWrapperField(this, 6, proto.s4.messages.interfaces.gatt.GattDescriptorValue.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.clearCharacteristicAggregateFormat = function() {
  this.setCharacteristicAggregateFormat(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.hasCharacteristicAggregateFormat = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional UserDefinedDescriptor user_defined_descriptor = 7;
 * @return {?proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.getUserDefinedDescriptor = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor, 7));
};


/** @param {?proto.s4.messages.interfaces.gatt.GattDescriptorValue.UserDefinedDescriptor|undefined} value */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.setUserDefinedDescriptor = function(value) {
  jspb.Message.setOneofWrapperField(this, 7, proto.s4.messages.interfaces.gatt.GattDescriptorValue.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.clearUserDefinedDescriptor = function() {
  this.setUserDefinedDescriptor(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.GattDescriptorValue.prototype.hasUserDefinedDescriptor = function() {
  return jspb.Message.getField(this, 7) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.Connect.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.Connect.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.Connect} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.Connect.toObject = function(includeInstance, msg) {
  var f, obj = {
    gattHandle: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.Connect}
 */
proto.s4.messages.interfaces.gatt.Connect.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.Connect;
  return proto.s4.messages.interfaces.gatt.Connect.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.Connect} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.Connect}
 */
proto.s4.messages.interfaces.gatt.Connect.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setGattHandle(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.Connect.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.Connect.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.Connect} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.Connect.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getGattHandle();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 gatt_handle = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.Connect.prototype.getGattHandle = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.Connect.prototype.setGattHandle = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.ConnectSuccess.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.ConnectSuccess.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.ConnectSuccess} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.ConnectSuccess.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.ConnectSuccess}
 */
proto.s4.messages.interfaces.gatt.ConnectSuccess.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.ConnectSuccess;
  return proto.s4.messages.interfaces.gatt.ConnectSuccess.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.ConnectSuccess} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.ConnectSuccess}
 */
proto.s4.messages.interfaces.gatt.ConnectSuccess.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.ConnectSuccess.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.ConnectSuccess.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.ConnectSuccess} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.ConnectSuccess.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.Disconnect.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.Disconnect.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.Disconnect} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.Disconnect.toObject = function(includeInstance, msg) {
  var f, obj = {
    gattHandle: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.Disconnect}
 */
proto.s4.messages.interfaces.gatt.Disconnect.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.Disconnect;
  return proto.s4.messages.interfaces.gatt.Disconnect.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.Disconnect} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.Disconnect}
 */
proto.s4.messages.interfaces.gatt.Disconnect.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setGattHandle(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.Disconnect.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.Disconnect.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.Disconnect} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.Disconnect.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getGattHandle();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 gatt_handle = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.Disconnect.prototype.getGattHandle = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.Disconnect.prototype.setGattHandle = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.Disconnected.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.Disconnected.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.Disconnected} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.Disconnected.toObject = function(includeInstance, msg) {
  var f, obj = {
    gattHandle: jspb.Message.getFieldWithDefault(msg, 1, 0),
    reason: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.Disconnected}
 */
proto.s4.messages.interfaces.gatt.Disconnected.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.Disconnected;
  return proto.s4.messages.interfaces.gatt.Disconnected.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.Disconnected} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.Disconnected}
 */
proto.s4.messages.interfaces.gatt.Disconnected.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setGattHandle(value);
      break;
    case 2:
      var value = /** @type {!proto.s4.messages.interfaces.gatt.Disconnected.Reason} */ (reader.readEnum());
      msg.setReason(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.Disconnected.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.Disconnected.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.Disconnected} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.Disconnected.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getGattHandle();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getReason();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.s4.messages.interfaces.gatt.Disconnected.Reason = {
  MY_DISCONNECT: 0,
  OUT_OF_RANGE: 1,
  PEER_DISCONNECT: 2,
  TIMEOUT: 3,
  IO_ERROR: 4
};

/**
 * optional int64 gatt_handle = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.Disconnected.prototype.getGattHandle = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.Disconnected.prototype.setGattHandle = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional Reason reason = 2;
 * @return {!proto.s4.messages.interfaces.gatt.Disconnected.Reason}
 */
proto.s4.messages.interfaces.gatt.Disconnected.prototype.getReason = function() {
  return /** @type {!proto.s4.messages.interfaces.gatt.Disconnected.Reason} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/** @param {!proto.s4.messages.interfaces.gatt.Disconnected.Reason} value */
proto.s4.messages.interfaces.gatt.Disconnected.prototype.setReason = function(value) {
  jspb.Message.setProto3EnumField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.DiscoverServices.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.DiscoverServices.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverServices} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.DiscoverServices.toObject = function(includeInstance, msg) {
  var f, obj = {
    gattHandle: jspb.Message.getFieldWithDefault(msg, 1, 0),
    uuid: (f = msg.getUuid()) && proto.s4.messages.interfaces.gatt.BTUUID.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.DiscoverServices}
 */
proto.s4.messages.interfaces.gatt.DiscoverServices.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.DiscoverServices;
  return proto.s4.messages.interfaces.gatt.DiscoverServices.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverServices} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.DiscoverServices}
 */
proto.s4.messages.interfaces.gatt.DiscoverServices.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setGattHandle(value);
      break;
    case 2:
      var value = new proto.s4.messages.interfaces.gatt.BTUUID;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.BTUUID.deserializeBinaryFromReader);
      msg.setUuid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.DiscoverServices.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.DiscoverServices.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverServices} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.DiscoverServices.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getGattHandle();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getUuid();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.s4.messages.interfaces.gatt.BTUUID.serializeBinaryToWriter
    );
  }
};


/**
 * optional int64 gatt_handle = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.DiscoverServices.prototype.getGattHandle = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.DiscoverServices.prototype.setGattHandle = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional BTUUID uuid = 2;
 * @return {?proto.s4.messages.interfaces.gatt.BTUUID}
 */
proto.s4.messages.interfaces.gatt.DiscoverServices.prototype.getUuid = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.BTUUID} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.BTUUID, 2));
};


/** @param {?proto.s4.messages.interfaces.gatt.BTUUID|undefined} value */
proto.s4.messages.interfaces.gatt.DiscoverServices.prototype.setUuid = function(value) {
  jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.DiscoverServices.prototype.clearUuid = function() {
  this.setUuid(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.DiscoverServices.prototype.hasUuid = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.toObject = function(includeInstance, msg) {
  var f, obj = {
    servicesList: jspb.Message.toObjectList(msg.getServicesList(),
    proto.s4.messages.interfaces.gatt.GattService.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess}
 */
proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess;
  return proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess}
 */
proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.s4.messages.interfaces.gatt.GattService;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.GattService.deserializeBinaryFromReader);
      msg.addServices(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getServicesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.s4.messages.interfaces.gatt.GattService.serializeBinaryToWriter
    );
  }
};


/**
 * repeated GattService services = 1;
 * @return {!Array<!proto.s4.messages.interfaces.gatt.GattService>}
 */
proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.prototype.getServicesList = function() {
  return /** @type{!Array<!proto.s4.messages.interfaces.gatt.GattService>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.s4.messages.interfaces.gatt.GattService, 1));
};


/** @param {!Array<!proto.s4.messages.interfaces.gatt.GattService>} value */
proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.prototype.setServicesList = function(value) {
  jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.s4.messages.interfaces.gatt.GattService=} opt_value
 * @param {number=} opt_index
 * @return {!proto.s4.messages.interfaces.gatt.GattService}
 */
proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.prototype.addServices = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.s4.messages.interfaces.gatt.GattService, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 */
proto.s4.messages.interfaces.gatt.DiscoverServicesSuccess.prototype.clearServicesList = function() {
  this.setServicesList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.FindIncludedServices.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.FindIncludedServices.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.FindIncludedServices} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.FindIncludedServices.toObject = function(includeInstance, msg) {
  var f, obj = {
    service: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.FindIncludedServices}
 */
proto.s4.messages.interfaces.gatt.FindIncludedServices.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.FindIncludedServices;
  return proto.s4.messages.interfaces.gatt.FindIncludedServices.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.FindIncludedServices} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.FindIncludedServices}
 */
proto.s4.messages.interfaces.gatt.FindIncludedServices.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setService(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.FindIncludedServices.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.FindIncludedServices.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.FindIncludedServices} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.FindIncludedServices.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getService();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 service = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.FindIncludedServices.prototype.getService = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.FindIncludedServices.prototype.setService = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.toObject = function(includeInstance, msg) {
  var f, obj = {
    servicesList: jspb.Message.toObjectList(msg.getServicesList(),
    proto.s4.messages.interfaces.gatt.GattService.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess}
 */
proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess;
  return proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess}
 */
proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.s4.messages.interfaces.gatt.GattService;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.GattService.deserializeBinaryFromReader);
      msg.addServices(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getServicesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.s4.messages.interfaces.gatt.GattService.serializeBinaryToWriter
    );
  }
};


/**
 * repeated GattService services = 1;
 * @return {!Array<!proto.s4.messages.interfaces.gatt.GattService>}
 */
proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.prototype.getServicesList = function() {
  return /** @type{!Array<!proto.s4.messages.interfaces.gatt.GattService>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.s4.messages.interfaces.gatt.GattService, 1));
};


/** @param {!Array<!proto.s4.messages.interfaces.gatt.GattService>} value */
proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.prototype.setServicesList = function(value) {
  jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.s4.messages.interfaces.gatt.GattService=} opt_value
 * @param {number=} opt_index
 * @return {!proto.s4.messages.interfaces.gatt.GattService}
 */
proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.prototype.addServices = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.s4.messages.interfaces.gatt.GattService, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 */
proto.s4.messages.interfaces.gatt.FindIncludedServicesSuccess.prototype.clearServicesList = function() {
  this.setServicesList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverCharacteristics} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.toObject = function(includeInstance, msg) {
  var f, obj = {
    service: jspb.Message.getFieldWithDefault(msg, 1, 0),
    uuid: (f = msg.getUuid()) && proto.s4.messages.interfaces.gatt.BTUUID.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.DiscoverCharacteristics}
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.DiscoverCharacteristics;
  return proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverCharacteristics} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.DiscoverCharacteristics}
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setService(value);
      break;
    case 2:
      var value = new proto.s4.messages.interfaces.gatt.BTUUID;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.BTUUID.deserializeBinaryFromReader);
      msg.setUuid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverCharacteristics} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getService();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getUuid();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.s4.messages.interfaces.gatt.BTUUID.serializeBinaryToWriter
    );
  }
};


/**
 * optional int64 service = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.prototype.getService = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.prototype.setService = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional BTUUID uuid = 2;
 * @return {?proto.s4.messages.interfaces.gatt.BTUUID}
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.prototype.getUuid = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.BTUUID} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.BTUUID, 2));
};


/** @param {?proto.s4.messages.interfaces.gatt.BTUUID|undefined} value */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.prototype.setUuid = function(value) {
  jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.prototype.clearUuid = function() {
  this.setUuid(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristics.prototype.hasUuid = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.toObject = function(includeInstance, msg) {
  var f, obj = {
    characteristicsList: jspb.Message.toObjectList(msg.getCharacteristicsList(),
    proto.s4.messages.interfaces.gatt.GattCharacteristic.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess}
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess;
  return proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess}
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.s4.messages.interfaces.gatt.GattCharacteristic;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.GattCharacteristic.deserializeBinaryFromReader);
      msg.addCharacteristics(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCharacteristicsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.s4.messages.interfaces.gatt.GattCharacteristic.serializeBinaryToWriter
    );
  }
};


/**
 * repeated GattCharacteristic characteristics = 1;
 * @return {!Array<!proto.s4.messages.interfaces.gatt.GattCharacteristic>}
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.prototype.getCharacteristicsList = function() {
  return /** @type{!Array<!proto.s4.messages.interfaces.gatt.GattCharacteristic>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.s4.messages.interfaces.gatt.GattCharacteristic, 1));
};


/** @param {!Array<!proto.s4.messages.interfaces.gatt.GattCharacteristic>} value */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.prototype.setCharacteristicsList = function(value) {
  jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.s4.messages.interfaces.gatt.GattCharacteristic=} opt_value
 * @param {number=} opt_index
 * @return {!proto.s4.messages.interfaces.gatt.GattCharacteristic}
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.prototype.addCharacteristics = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.s4.messages.interfaces.gatt.GattCharacteristic, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 */
proto.s4.messages.interfaces.gatt.DiscoverCharacteristicsSuccess.prototype.clearCharacteristicsList = function() {
  this.setCharacteristicsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptors.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.DiscoverDescriptors.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverDescriptors} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptors.toObject = function(includeInstance, msg) {
  var f, obj = {
    characteristic: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.DiscoverDescriptors}
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptors.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.DiscoverDescriptors;
  return proto.s4.messages.interfaces.gatt.DiscoverDescriptors.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverDescriptors} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.DiscoverDescriptors}
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptors.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCharacteristic(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptors.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.DiscoverDescriptors.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverDescriptors} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptors.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCharacteristic();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 characteristic = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptors.prototype.getCharacteristic = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.DiscoverDescriptors.prototype.setCharacteristic = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.toObject = function(includeInstance, msg) {
  var f, obj = {
    descriptorsList: jspb.Message.toObjectList(msg.getDescriptorsList(),
    proto.s4.messages.interfaces.gatt.GattDescriptor.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess}
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess;
  return proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess}
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.s4.messages.interfaces.gatt.GattDescriptor;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.GattDescriptor.deserializeBinaryFromReader);
      msg.addDescriptors(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getDescriptorsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.s4.messages.interfaces.gatt.GattDescriptor.serializeBinaryToWriter
    );
  }
};


/**
 * repeated GattDescriptor descriptors = 1;
 * @return {!Array<!proto.s4.messages.interfaces.gatt.GattDescriptor>}
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.prototype.getDescriptorsList = function() {
  return /** @type{!Array<!proto.s4.messages.interfaces.gatt.GattDescriptor>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.s4.messages.interfaces.gatt.GattDescriptor, 1));
};


/** @param {!Array<!proto.s4.messages.interfaces.gatt.GattDescriptor>} value */
proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.prototype.setDescriptorsList = function(value) {
  jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.s4.messages.interfaces.gatt.GattDescriptor=} opt_value
 * @param {number=} opt_index
 * @return {!proto.s4.messages.interfaces.gatt.GattDescriptor}
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.prototype.addDescriptors = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.s4.messages.interfaces.gatt.GattDescriptor, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 */
proto.s4.messages.interfaces.gatt.DiscoverDescriptorsSuccess.prototype.clearDescriptorsList = function() {
  this.setDescriptorsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristic.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.ReadCharacteristic.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.ReadCharacteristic} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristic.toObject = function(includeInstance, msg) {
  var f, obj = {
    characteristic: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.ReadCharacteristic}
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristic.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.ReadCharacteristic;
  return proto.s4.messages.interfaces.gatt.ReadCharacteristic.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.ReadCharacteristic} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.ReadCharacteristic}
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristic.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCharacteristic(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristic.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.ReadCharacteristic.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.ReadCharacteristic} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristic.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCharacteristic();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 characteristic = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristic.prototype.getCharacteristic = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.ReadCharacteristic.prototype.setCharacteristic = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess.toObject = function(includeInstance, msg) {
  var f, obj = {
    value: msg.getValue_asB64()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess}
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess;
  return proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess}
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!Uint8Array} */ (reader.readBytes());
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getValue_asU8();
  if (f.length > 0) {
    writer.writeBytes(
      1,
      f
    );
  }
};


/**
 * optional bytes value = 1;
 * @return {!(string|Uint8Array)}
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess.prototype.getValue = function() {
  return /** @type {!(string|Uint8Array)} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * optional bytes value = 1;
 * This is a type-conversion wrapper around `getValue()`
 * @return {string}
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess.prototype.getValue_asB64 = function() {
  return /** @type {string} */ (jspb.Message.bytesAsB64(
      this.getValue()));
};


/**
 * optional bytes value = 1;
 * Note that Uint8Array is not supported on all browsers.
 * @see http://caniuse.com/Uint8Array
 * This is a type-conversion wrapper around `getValue()`
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess.prototype.getValue_asU8 = function() {
  return /** @type {!Uint8Array} */ (jspb.Message.bytesAsU8(
      this.getValue()));
};


/** @param {!(string|Uint8Array)} value */
proto.s4.messages.interfaces.gatt.ReadCharacteristicSuccess.prototype.setValue = function(value) {
  jspb.Message.setProto3BytesField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristic.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.WriteCharacteristic.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.WriteCharacteristic} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristic.toObject = function(includeInstance, msg) {
  var f, obj = {
    characteristic: jspb.Message.getFieldWithDefault(msg, 1, 0),
    value: msg.getValue_asB64()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.WriteCharacteristic}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristic.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.WriteCharacteristic;
  return proto.s4.messages.interfaces.gatt.WriteCharacteristic.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.WriteCharacteristic} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.WriteCharacteristic}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristic.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCharacteristic(value);
      break;
    case 2:
      var value = /** @type {!Uint8Array} */ (reader.readBytes());
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristic.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.WriteCharacteristic.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.WriteCharacteristic} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristic.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCharacteristic();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getValue_asU8();
  if (f.length > 0) {
    writer.writeBytes(
      2,
      f
    );
  }
};


/**
 * optional int64 characteristic = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristic.prototype.getCharacteristic = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.WriteCharacteristic.prototype.setCharacteristic = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional bytes value = 2;
 * @return {!(string|Uint8Array)}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristic.prototype.getValue = function() {
  return /** @type {!(string|Uint8Array)} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * optional bytes value = 2;
 * This is a type-conversion wrapper around `getValue()`
 * @return {string}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristic.prototype.getValue_asB64 = function() {
  return /** @type {string} */ (jspb.Message.bytesAsB64(
      this.getValue()));
};


/**
 * optional bytes value = 2;
 * Note that Uint8Array is not supported on all browsers.
 * @see http://caniuse.com/Uint8Array
 * This is a type-conversion wrapper around `getValue()`
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristic.prototype.getValue_asU8 = function() {
  return /** @type {!Uint8Array} */ (jspb.Message.bytesAsU8(
      this.getValue()));
};


/** @param {!(string|Uint8Array)} value */
proto.s4.messages.interfaces.gatt.WriteCharacteristic.prototype.setValue = function(value) {
  jspb.Message.setProto3BytesField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess;
  return proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicSuccess.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    characteristic: jspb.Message.getFieldWithDefault(msg, 1, 0),
    value: msg.getValue_asB64()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse;
  return proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCharacteristic(value);
      break;
    case 2:
      var value = /** @type {!Uint8Array} */ (reader.readBytes());
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCharacteristic();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getValue_asU8();
  if (f.length > 0) {
    writer.writeBytes(
      2,
      f
    );
  }
};


/**
 * optional int64 characteristic = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.prototype.getCharacteristic = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.prototype.setCharacteristic = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional bytes value = 2;
 * @return {!(string|Uint8Array)}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.prototype.getValue = function() {
  return /** @type {!(string|Uint8Array)} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * optional bytes value = 2;
 * This is a type-conversion wrapper around `getValue()`
 * @return {string}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.prototype.getValue_asB64 = function() {
  return /** @type {string} */ (jspb.Message.bytesAsB64(
      this.getValue()));
};


/**
 * optional bytes value = 2;
 * Note that Uint8Array is not supported on all browsers.
 * @see http://caniuse.com/Uint8Array
 * This is a type-conversion wrapper around `getValue()`
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.prototype.getValue_asU8 = function() {
  return /** @type {!Uint8Array} */ (jspb.Message.bytesAsU8(
      this.getValue()));
};


/** @param {!(string|Uint8Array)} value */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponse.prototype.setValue = function(value) {
  jspb.Message.setProto3BytesField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess;
  return proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.WriteCharacteristicWithoutResponseSuccess.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.Notification.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.Notification.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.Notification} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.Notification.toObject = function(includeInstance, msg) {
  var f, obj = {
    characteristic: jspb.Message.getFieldWithDefault(msg, 1, 0),
    value: msg.getValue_asB64()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.Notification}
 */
proto.s4.messages.interfaces.gatt.Notification.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.Notification;
  return proto.s4.messages.interfaces.gatt.Notification.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.Notification} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.Notification}
 */
proto.s4.messages.interfaces.gatt.Notification.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCharacteristic(value);
      break;
    case 2:
      var value = /** @type {!Uint8Array} */ (reader.readBytes());
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.Notification.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.Notification.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.Notification} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.Notification.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCharacteristic();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getValue_asU8();
  if (f.length > 0) {
    writer.writeBytes(
      2,
      f
    );
  }
};


/**
 * optional int64 characteristic = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.Notification.prototype.getCharacteristic = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.Notification.prototype.setCharacteristic = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional bytes value = 2;
 * @return {!(string|Uint8Array)}
 */
proto.s4.messages.interfaces.gatt.Notification.prototype.getValue = function() {
  return /** @type {!(string|Uint8Array)} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * optional bytes value = 2;
 * This is a type-conversion wrapper around `getValue()`
 * @return {string}
 */
proto.s4.messages.interfaces.gatt.Notification.prototype.getValue_asB64 = function() {
  return /** @type {string} */ (jspb.Message.bytesAsB64(
      this.getValue()));
};


/**
 * optional bytes value = 2;
 * Note that Uint8Array is not supported on all browsers.
 * @see http://caniuse.com/Uint8Array
 * This is a type-conversion wrapper around `getValue()`
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.Notification.prototype.getValue_asU8 = function() {
  return /** @type {!Uint8Array} */ (jspb.Message.bytesAsU8(
      this.getValue()));
};


/** @param {!(string|Uint8Array)} value */
proto.s4.messages.interfaces.gatt.Notification.prototype.setValue = function(value) {
  jspb.Message.setProto3BytesField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.ReadDescriptor.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.ReadDescriptor.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.ReadDescriptor} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.ReadDescriptor.toObject = function(includeInstance, msg) {
  var f, obj = {
    descr: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.ReadDescriptor}
 */
proto.s4.messages.interfaces.gatt.ReadDescriptor.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.ReadDescriptor;
  return proto.s4.messages.interfaces.gatt.ReadDescriptor.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.ReadDescriptor} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.ReadDescriptor}
 */
proto.s4.messages.interfaces.gatt.ReadDescriptor.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setDescr(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.ReadDescriptor.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.ReadDescriptor.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.ReadDescriptor} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.ReadDescriptor.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getDescr();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 descr = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.ReadDescriptor.prototype.getDescr = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.ReadDescriptor.prototype.setDescr = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess.toObject = function(includeInstance, msg) {
  var f, obj = {
    value: (f = msg.getValue()) && proto.s4.messages.interfaces.gatt.GattDescriptorValue.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess}
 */
proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess;
  return proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess}
 */
proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.s4.messages.interfaces.gatt.GattDescriptorValue;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.GattDescriptorValue.deserializeBinaryFromReader);
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getValue();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.s4.messages.interfaces.gatt.GattDescriptorValue.serializeBinaryToWriter
    );
  }
};


/**
 * optional GattDescriptorValue value = 1;
 * @return {?proto.s4.messages.interfaces.gatt.GattDescriptorValue}
 */
proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess.prototype.getValue = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.GattDescriptorValue} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.GattDescriptorValue, 1));
};


/** @param {?proto.s4.messages.interfaces.gatt.GattDescriptorValue|undefined} value */
proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess.prototype.setValue = function(value) {
  jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess.prototype.clearValue = function() {
  this.setValue(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.ReadDescriptorSuccess.prototype.hasValue = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.WriteDescriptor.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.WriteDescriptor.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.WriteDescriptor} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.WriteDescriptor.toObject = function(includeInstance, msg) {
  var f, obj = {
    descr: jspb.Message.getFieldWithDefault(msg, 1, 0),
    value: (f = msg.getValue()) && proto.s4.messages.interfaces.gatt.GattDescriptorValue.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.WriteDescriptor}
 */
proto.s4.messages.interfaces.gatt.WriteDescriptor.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.WriteDescriptor;
  return proto.s4.messages.interfaces.gatt.WriteDescriptor.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.WriteDescriptor} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.WriteDescriptor}
 */
proto.s4.messages.interfaces.gatt.WriteDescriptor.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setDescr(value);
      break;
    case 2:
      var value = new proto.s4.messages.interfaces.gatt.GattDescriptorValue;
      reader.readMessage(value,proto.s4.messages.interfaces.gatt.GattDescriptorValue.deserializeBinaryFromReader);
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.WriteDescriptor.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.WriteDescriptor.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.WriteDescriptor} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.WriteDescriptor.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getDescr();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getValue();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.s4.messages.interfaces.gatt.GattDescriptorValue.serializeBinaryToWriter
    );
  }
};


/**
 * optional int64 descr = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.WriteDescriptor.prototype.getDescr = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.WriteDescriptor.prototype.setDescr = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional GattDescriptorValue value = 2;
 * @return {?proto.s4.messages.interfaces.gatt.GattDescriptorValue}
 */
proto.s4.messages.interfaces.gatt.WriteDescriptor.prototype.getValue = function() {
  return /** @type{?proto.s4.messages.interfaces.gatt.GattDescriptorValue} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.gatt.GattDescriptorValue, 2));
};


/** @param {?proto.s4.messages.interfaces.gatt.GattDescriptorValue|undefined} value */
proto.s4.messages.interfaces.gatt.WriteDescriptor.prototype.setValue = function(value) {
  jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.gatt.WriteDescriptor.prototype.clearValue = function() {
  this.setValue(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.WriteDescriptor.prototype.hasValue = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess}
 */
proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess;
  return proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess}
 */
proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.WriteDescriptorSuccess.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.SetNotification.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.SetNotification.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.SetNotification} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.SetNotification.toObject = function(includeInstance, msg) {
  var f, obj = {
    characteristic: jspb.Message.getFieldWithDefault(msg, 1, 0),
    enableNotification: jspb.Message.getBooleanFieldWithDefault(msg, 2, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.SetNotification}
 */
proto.s4.messages.interfaces.gatt.SetNotification.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.SetNotification;
  return proto.s4.messages.interfaces.gatt.SetNotification.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.SetNotification} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.SetNotification}
 */
proto.s4.messages.interfaces.gatt.SetNotification.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCharacteristic(value);
      break;
    case 2:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setEnableNotification(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.SetNotification.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.SetNotification.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.SetNotification} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.SetNotification.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCharacteristic();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getEnableNotification();
  if (f) {
    writer.writeBool(
      2,
      f
    );
  }
};


/**
 * optional int64 characteristic = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.gatt.SetNotification.prototype.getCharacteristic = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.gatt.SetNotification.prototype.setCharacteristic = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional bool enable_notification = 2;
 * @return {boolean}
 */
proto.s4.messages.interfaces.gatt.SetNotification.prototype.getEnableNotification = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 2, false));
};


/** @param {boolean} value */
proto.s4.messages.interfaces.gatt.SetNotification.prototype.setEnableNotification = function(value) {
  jspb.Message.setProto3BooleanField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.gatt.SetNotificationSuccess.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.gatt.SetNotificationSuccess.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.gatt.SetNotificationSuccess} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.SetNotificationSuccess.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.gatt.SetNotificationSuccess}
 */
proto.s4.messages.interfaces.gatt.SetNotificationSuccess.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.gatt.SetNotificationSuccess;
  return proto.s4.messages.interfaces.gatt.SetNotificationSuccess.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.gatt.SetNotificationSuccess} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.gatt.SetNotificationSuccess}
 */
proto.s4.messages.interfaces.gatt.SetNotificationSuccess.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.gatt.SetNotificationSuccess.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.gatt.SetNotificationSuccess.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.gatt.SetNotificationSuccess} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.gatt.SetNotificationSuccess.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};


goog.object.extend(exports, proto.s4.messages.interfaces.gatt);
