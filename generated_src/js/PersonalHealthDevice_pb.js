// source: PersonalHealthDevice.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

goog.exportSymbol('proto.s4.messages.interfaces.personal_health_device.C2P', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.personal_health_device.C2P.Event', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.personal_health_device.C2P.Event.EventCase', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.personal_health_device.C2P.PayloadCase', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.personal_health_device.P2C', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType', null, global);
goog.exportSymbol('proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.personal_health_device.C2P = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.s4.messages.interfaces.personal_health_device.C2P.oneofGroups_);
};
goog.inherits(proto.s4.messages.interfaces.personal_health_device.C2P, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.personal_health_device.C2P.displayName = 'proto.s4.messages.interfaces.personal_health_device.C2P';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.s4.messages.interfaces.personal_health_device.C2P.Event.oneofGroups_);
};
goog.inherits(proto.s4.messages.interfaces.personal_health_device.C2P.Event, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.personal_health_device.C2P.Event.displayName = 'proto.s4.messages.interfaces.personal_health_device.C2P.Event';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.personal_health_device.P2C = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.personal_health_device.P2C, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.personal_health_device.P2C.displayName = 'proto.s4.messages.interfaces.personal_health_device.P2C';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType.displayName = 'proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType.displayName = 'proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType';
}

/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.s4.messages.interfaces.personal_health_device.C2P.oneofGroups_ = [[2]];

/**
 * @enum {number}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.PayloadCase = {
  PAYLOAD_NOT_SET: 0,
  EVENT: 2
};

/**
 * @return {proto.s4.messages.interfaces.personal_health_device.C2P.PayloadCase}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.prototype.getPayloadCase = function() {
  return /** @type {proto.s4.messages.interfaces.personal_health_device.C2P.PayloadCase} */(jspb.Message.computeOneofCase(this, proto.s4.messages.interfaces.personal_health_device.C2P.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.personal_health_device.C2P.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.personal_health_device.C2P} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.personal_health_device.C2P.toObject = function(includeInstance, msg) {
  var f, obj = {
    event: (f = msg.getEvent()) && proto.s4.messages.interfaces.personal_health_device.C2P.Event.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.personal_health_device.C2P}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.personal_health_device.C2P;
  return proto.s4.messages.interfaces.personal_health_device.C2P.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.personal_health_device.C2P} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.personal_health_device.C2P}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 2:
      var value = new proto.s4.messages.interfaces.personal_health_device.C2P.Event;
      reader.readMessage(value,proto.s4.messages.interfaces.personal_health_device.C2P.Event.deserializeBinaryFromReader);
      msg.setEvent(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.personal_health_device.C2P.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.personal_health_device.C2P} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.personal_health_device.C2P.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEvent();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.s4.messages.interfaces.personal_health_device.C2P.Event.serializeBinaryToWriter
    );
  }
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.oneofGroups_ = [[1,2]];

/**
 * @enum {number}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.EventCase = {
  EVENT_NOT_SET: 0,
  SUBSCRIBE_DEVICE_TYPE: 1,
  UNSUBSCRIBE_DEVICE_TYPE: 2
};

/**
 * @return {proto.s4.messages.interfaces.personal_health_device.C2P.Event.EventCase}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.prototype.getEventCase = function() {
  return /** @type {proto.s4.messages.interfaces.personal_health_device.C2P.Event.EventCase} */(jspb.Message.computeOneofCase(this, proto.s4.messages.interfaces.personal_health_device.C2P.Event.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.personal_health_device.C2P.Event.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.personal_health_device.C2P.Event} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.toObject = function(includeInstance, msg) {
  var f, obj = {
    subscribeDeviceType: (f = msg.getSubscribeDeviceType()) && proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType.toObject(includeInstance, f),
    unsubscribeDeviceType: (f = msg.getUnsubscribeDeviceType()) && proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.personal_health_device.C2P.Event}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.personal_health_device.C2P.Event;
  return proto.s4.messages.interfaces.personal_health_device.C2P.Event.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.personal_health_device.C2P.Event} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.personal_health_device.C2P.Event}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType;
      reader.readMessage(value,proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType.deserializeBinaryFromReader);
      msg.setSubscribeDeviceType(value);
      break;
    case 2:
      var value = new proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType;
      reader.readMessage(value,proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType.deserializeBinaryFromReader);
      msg.setUnsubscribeDeviceType(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.personal_health_device.C2P.Event.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.personal_health_device.C2P.Event} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSubscribeDeviceType();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType.serializeBinaryToWriter
    );
  }
  f = message.getUnsubscribeDeviceType();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType.serializeBinaryToWriter
    );
  }
};


/**
 * optional SubscribeDeviceType subscribe_device_type = 1;
 * @return {?proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.prototype.getSubscribeDeviceType = function() {
  return /** @type{?proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType, 1));
};


/** @param {?proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType|undefined} value */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.prototype.setSubscribeDeviceType = function(value) {
  jspb.Message.setOneofWrapperField(this, 1, proto.s4.messages.interfaces.personal_health_device.C2P.Event.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.prototype.clearSubscribeDeviceType = function() {
  this.setSubscribeDeviceType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.prototype.hasSubscribeDeviceType = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional UnsubscribeDeviceType unsubscribe_device_type = 2;
 * @return {?proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.prototype.getUnsubscribeDeviceType = function() {
  return /** @type{?proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType, 2));
};


/** @param {?proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType|undefined} value */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.prototype.setUnsubscribeDeviceType = function(value) {
  jspb.Message.setOneofWrapperField(this, 2, proto.s4.messages.interfaces.personal_health_device.C2P.Event.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.prototype.clearUnsubscribeDeviceType = function() {
  this.setUnsubscribeDeviceType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.Event.prototype.hasUnsubscribeDeviceType = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional Event event = 2;
 * @return {?proto.s4.messages.interfaces.personal_health_device.C2P.Event}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.prototype.getEvent = function() {
  return /** @type{?proto.s4.messages.interfaces.personal_health_device.C2P.Event} */ (
    jspb.Message.getWrapperField(this, proto.s4.messages.interfaces.personal_health_device.C2P.Event, 2));
};


/** @param {?proto.s4.messages.interfaces.personal_health_device.C2P.Event|undefined} value */
proto.s4.messages.interfaces.personal_health_device.C2P.prototype.setEvent = function(value) {
  jspb.Message.setOneofWrapperField(this, 2, proto.s4.messages.interfaces.personal_health_device.C2P.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.s4.messages.interfaces.personal_health_device.C2P.prototype.clearEvent = function() {
  this.setEvent(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.s4.messages.interfaces.personal_health_device.C2P.prototype.hasEvent = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.personal_health_device.P2C.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.personal_health_device.P2C.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.personal_health_device.P2C} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.personal_health_device.P2C.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.personal_health_device.P2C}
 */
proto.s4.messages.interfaces.personal_health_device.P2C.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.personal_health_device.P2C;
  return proto.s4.messages.interfaces.personal_health_device.P2C.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.personal_health_device.P2C} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.personal_health_device.P2C}
 */
proto.s4.messages.interfaces.personal_health_device.P2C.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.personal_health_device.P2C.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.personal_health_device.P2C.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.personal_health_device.P2C} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.personal_health_device.P2C.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType.toObject = function(includeInstance, msg) {
  var f, obj = {
    mdcCode: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType}
 */
proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType;
  return proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType}
 */
proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setMdcCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getMdcCode();
  if (f !== 0) {
    writer.writeUint32(
      1,
      f
    );
  }
};


/**
 * optional uint32 mdc_code = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType.prototype.getMdcCode = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.personal_health_device.SubscribeDeviceType.prototype.setMdcCode = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType.prototype.toObject = function(opt_includeInstance) {
  return proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType.toObject = function(includeInstance, msg) {
  var f, obj = {
    mdcCode: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType}
 */
proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType;
  return proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType}
 */
proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setMdcCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getMdcCode();
  if (f !== 0) {
    writer.writeUint32(
      1,
      f
    );
  }
};


/**
 * optional uint32 mdc_code = 1;
 * @return {number}
 */
proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType.prototype.getMdcCode = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.s4.messages.interfaces.personal_health_device.UnsubscribeDeviceType.prototype.setMdcCode = function(value) {
  jspb.Message.setProto3IntField(this, 1, value);
};


goog.object.extend(exports, proto.s4.messages.interfaces.personal_health_device);
