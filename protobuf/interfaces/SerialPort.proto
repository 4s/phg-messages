syntax = "proto3";

package s4.messages.interfaces.serial_port;
option java_package = "dk.s4.phg.messages.interfaces.serial_port";
option java_multiple_files = true;
option optimize_for = LITE_RUNTIME;

/**
 * The SerialPort interface models a classical serial port
 * interface. Currently, it only supports "simple" serial ports with
 * no port settings (baud rate, data bits, stop bits, handshake etc.)
 * as the use at this point will be limited to Bluetooth and USB where
 * such settings are typically ignored. Port settings may be added in
 * a later revision of this interface, though.
 *
 * The producer of this interface offers access to one or more serial
 * ports and must also expose a DeviceControl interface through which
 * it identifies all the serial ports it exposes (as objId_t objects).
 *
 * The consumer of this interface will identify the producer using
 * service discovery on the DeviceControl interface, identifying the
 * module peer_t handler and all the objId_t handles representing the
 * serial ports provided by that device.
 *
 * To make a connection, the consumer sends the Open request to the
 * handler, stating the device handle and port number. The success
 * callback of the Open request acts also as the receiving data
 * stream. As the port is successfully opened, an OpenSuccess response
 * will be sent immediately (most likely, yet not necessarily, with no
 * message) and the callback will be kept alive. Later on, as data is
 * received, the callback is reused with the message containing the
 * received data. Finally, when the port is closed, the callback will
 * be used a last time (probably again without a message) to free the
 * callback.
 *
 * Notice that the Close (consumer to producer) and Closed (producer
 * to consumer) events are defined this way because they are truly
 * asynchronous and may happen in either order (even
 * concurrently). Both events are mandatory to properly close the
 * port. When a party wants to close the port, it must send its event
 * to the peer and wait for the corresponding event from the peer
 * before freeing the ressources. Conversely, upon receiving such an
 * event from the peer, all access to the port must stop and the
 * corresponding event returned.
 *
 * If an error occurs at the Producer end, the port must be closed
 * (closed event sent as described above) and the Open error callback
 * is used to inform about the error details. The peer intentionally
 * closing the port is NOT regarded as an error, while closing due to
 * a timeout would constitute an error that should be reported.
 */

// **********************************************************************
//
// Errors defined in this interface
//
// | Error code | Title and short description                           |
// |------------|-------------------------------------------------------|
// |     1      | Invalid or missing argument                           |
// |            | A mandatory argument is missing or not valid          |
// |------------|-------------------------------------------------------|
// |     2      | Port already open                                     |
// |            | Attempt to open a port which is already open          |
// |------------|-------------------------------------------------------|
// |     3      | Port not open                                         |
// |            | Attempt to write to a port which is not open          |
// |------------|-------------------------------------------------------|
// |     4      | Operation in progress                                 |
// |            | Attempt to write to a port while another write (or    |
// |            | the open) operation is still in progress.             |
// |------------|-------------------------------------------------------|
// |     5      | Communication error                                   |
// |            | General I/O error catch-all. The error details should |
// |            | elaborate.                                            |
// |------------|-------------------------------------------------------|
//
// **********************************************************************



message C2P {
  message Request {
    oneof request {
      Open open = 1;
      Write write = 2;
    }
  }
  message Event {
    oneof event {
      Close close = 1;
    }
  }
  oneof payload {
    Request request = 1;
    Event event = 2;
  }
}

message P2C {
  message Event {
    oneof event {
      Closed closed = 1;
    }
  }
  oneof payload {
    // No requests defined at this point
    Event event = 2;
  }
}


/**
 * Open the serial port
 *
 * The following errors may be generated:
 *  1. Invalid or missing argument - if serial_port_handle is missing
 *     or does not represent a serial port object known to this
 *     provider
 *  2. Port already open - if the requested port is currently open (or
 *     being opened by someone else)
 *  3. Port not open - if the port was closed (using the Close event
 *     or by the peer) while opening.
 *  5. Communication error - general I/O error caused the operation to
 *     fail
 */
message Open { // C2P
  
  /**
   * The port to open
   * Mandatory
   */
  /* objId_t */ int64 serial_port_handle = 1;
}

message OpenSuccess { // P2C
  
  /**
   * The message is optional as the first and last OpenSuccess might
   * not have a message.
   * Optional
   */
  bytes message = 2;
}


/**
 * Close the serial port from the consumer end. This is a no-op if the
 * serial_port_handle does not represent an open port.
 *
 * Notice that a port must be closed from both the producer and
 * consumer end to be properly closed and ready for re-opening.
 *
 * Closing a port with a pending write buffer (i.e. a previous write
 * operation that did not have the flush flag set) may abandon the
 * written data such that it is only partially transmitted. In order
 * to ensure that all data are properly transmitted before closing the
 * port, the final Write function call must have the flush flag set
 * and wait for the function to return before calling Close.
 *
 * It is allowed to use the 'Close' event to force cancel a pending
 * 'Open' or 'Write' function (even with the flush flag set). This
 * will result in the return of error code 3 (Port not open)  
 */
message Close { // C2P

  /**
   * The port to close.
   * Mandatory
   */
  /* objId_t */ int64 serial_port_handle = 1;
}


/**
 * The serial port was closed at the producer end.
 */
message Closed { // P2C

  /**
   * The port that was closed
   * Mandatory
   */
  /* objId_t */ int64 serial_port_handle = 1;
}


/**
 * Write data to the serial port. At any time, only one write can be
 * active - i.e. the consumer must wait for a WriteSuccess or error
 * response before sending a new Write request. The producer is
 * allowed to reject a "reentrant" write request with an error
 * response.
 *
 * The following errors may be generated:
 *  1. Invalid or missing argument - if serial_port_handle is missing
 *     or does not represent a serial port object known to this
 *     provider
 *  3. Port not open - The port is not open at this time (or was closed
 *     during the write, e.g. using the 'Close' event). This error may
 *     also occur if an I/O error caused the port to be closed from the
 *     producer end. In that case, a 'Closed' event should be on its
 *     way or already arrived.
 *  4. Operation in progress - another write operation (or perhaps the
 *     open operation) is in progress; the function request was sent
 *     and the producer has not yet responded
 *  5. Communication error - general I/O error caused the operation to
 *     fail. If this is a permanent error that causes a disconnect, a
 *     'Closed' event will also follow (or has been signalled
 *     already)
 */
message Write { // C2P

  /**
   * The port to write to
   * Mandatory
   */
  /* objId_t */ int64 serial_port_handle = 1;

  /**
   * The data buffer
   * Optional. Default value is an empty buffer.
   */
  bytes message = 2;

  /**
   * Set flush to true if the output buffer should be flushed at the
   * hardware level - typically used when the last fragment of a
   * command is sent. When the buffer is flushed, the producer should
   * not invoke the WriteSuccess callback until the flushing
   * succeeds. Note, however, that on most non-UART hardware
   * (e.g. Bluetooth, USB) this does NOT imply that the buffer has
   * been transmitted physically on the serial port, only that the
   * operating system / hardware driver has assumed the responsibility
   * to do so in a very near future!
   * Optional. Default value is 'false'
   */
  bool flush = 3;
}

/**
 * The message was accepted for transmission on the serial port - but
 * depending on the 'flush' flag not necessarily sent. Note that (even
 * with flush==false) the duration of the write operation may be
 * arbitrary. This is especially relevant if the hardware uses flow
 * control to pause transmission.
 * Note, that if a write operation is in progress when a 'Close' event
 * is sent, the WriteSuccess will be returned immediately, regardless
 * of the state of the transmission.
 */
message WriteSuccess { // P2C
  
}
