syntax = "proto3";

package s4.messages.interfaces.config_settings;
option java_package = "dk.s4.phg.messages.interfaces.config_settings";
option java_multiple_files = true;
option optimize_for = LITE_RUNTIME;

/**
 * The ConfigSettings interface. A provider of this interface will offer two
 * functions: to read and write a map of key-value pairs.
 *
 * As both functions will require the consumer to know the provider in
 * advance, a mechanism for service discovery is provided: the
 * provider can multicast a solicitation (SolicitStore), which shall
 * result in a multicast announcement (AnnounceStore) from the
 * provider. The provider should also announce itself after the system
 * has started (the application state changes to RUNNING). This will
 * cause most (if not all) consumers to discover the store at this
 * early stage, eliminating the need for using SolicitStore.
 *
 * The underlying store is capable of storing and retrieving named
 * key-value maps of strings (a.k.a. map<string,string>). A consumer
 * of this interface may store one or several such maps. The consumer
 * must use a unique name starting with the name of the consumer
 * module, followed by a postfix which will identify the particular
 * map.
 */

// **********************************************************************
//
// Errors defined in this interface
//
// | Error code | Title and short description                           |
// |------------|-------------------------------------------------------|
// |     1      | Invalid or missing argument                           |
// |            | A mandatory argument is missing or not valid          |
// |------------|-------------------------------------------------------|
// |     2      | Write to persistence failed                           |
// |            | The underlying persistence layer could not persist    |
// |            | the message                                           |
// |------------|-------------------------------------------------------|
//
// **********************************************************************



message C2P {
  message Request {
    oneof request {
      ReadConfiguration read_configuration = 1;
      WriteConfiguration write_configuration = 2;
    }
  }
  message Event {
    oneof event {
      SolicitStore solicit_store = 1;
    }
  }
  oneof payload {
    Request request = 1;
    Event event = 2;
  }
}

message P2C {
  message Event {
    oneof event {
      AnnounceStore announce_store = 1;
    }
  }
  oneof payload {
    // No requests defined at this point
    Event event = 2;
  }
}


message KeyValuePair {
  string key = 1;
  string value = 2;
}


/**
 * Read a named key-value map from the underlying store
 *
 * The following errors may be generated:
 *  1. Invalid or missing argument - if configurationName is missing
 */
message ReadConfiguration { // C2P

  /**
   * The name of the requested configuration settings map
   * Mandatory
   */
  string configuration_name = 1;
}

message ReadConfigurationSuccess { // P2C

  /**
   * All configuration settings stored in the ${configurationName} map.
   */
  repeated KeyValuePair configuration = 1;
}


/**
 * Add a set of key-value pairs to a map in the underlying store
 *
 * If the map named ${configurationName} does not already exist, then an empty
 * map will be created. Any existing values in the stored map matching the keys
 * provided here will be overwritten with the associated values. All other
 * key-value pairs stored in the map will not be touched.
 * 
 * The success of this function will be issued AFTER the changes have been stored
 * in persisted storage.
 *
 * The following errors may be generated:
 *  1. Invalid or missing argument - if any argument is missing
 *  2. Write to persistence failed
 */
message WriteConfiguration { // C2P

  /**
   * The name of the configuration settings map being stored
   * Mandatory
   */
  string configuration_name = 1;

  /**
   * The configuration settings to be stored in the ${configurationName} map
   * Mandatory (1..*)
   */
  repeated KeyValuePair configuration = 2;
}


message WriteConfigurationSuccess { // P2C
}


/**
 * Solicitation for ConfigSettings interface provider service discovery
 * announcements
 */
message SolicitStore { // C2P
}


/**
 * A ConfigSettings interface provider (store) announces its service to all
 * interested modules
 */
message AnnounceStore { // P2C
}
