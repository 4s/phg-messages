const path = require('path');

module.exports = {
  entry: './index.js',
  target: 'web',
  externals: [],
  output: {
    library: 'phg-messages',
    libraryTarget: 'umd',
    path: path.resolve(__dirname, 'generated_src/js_module'),
    filename: 'messages.js'
  }
};

