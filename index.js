const classes = {
    device: require('./generated_src/js/Device_pb'),
    error: require('./generated_src/js/Error_pb'),
    log: require('./generated_src/js/Log_pb')
}
const interfaces = {
    device_control: require('./generated_src/js/DeviceControl_pb'),
    bluetooth_device_control: require('./generated_src/js/BluetoothDeviceControl_pb'),
    device_domain: require('./generated_src/js/DeviceDomain_pb'),
    device_management: require('./generated_src/js/DeviceManagement_pb'),
    fhir_measurement: require('./generated_src/js/FHIRMeasurement_pb'),
    gatt: require('./generated_src/js/GATT_pb'),
    monica: require('./generated_src/js/Monica_pb'),
    serial_port: require('./generated_src/js/SerialPort_pb'),
    time: require('./generated_src/js/Time_pb'),
    personal_health_device: require('./generated_src/js/PersonalHealthDevice_pb'),
    config_settings: require('./generated_src/js/ConfigSettings_pb')
}
/*
When adding new interfaces here, then remember to also add them to the
npm/index.d.ts file.
That file defines the types.
*/
export { classes, interfaces }
