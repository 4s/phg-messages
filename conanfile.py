from conans import ConanFile, CMake


class PhgmessagesConan(ConanFile):
    name = "phg-messages"
    version = "0.2.5"
    license = "Apache-2.0"
    author = "Jacob Andersen <jacob.andersen@alexandra.dk>"
    url = "https://bitbucket.org/4s/phg-messages"
    description = "Repository for topics and messages shared across PHG modules. Messages are specified as Google Protocol Buffer proto-files."
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": True, "protobuf:shared": True, "protobuf:with_zlib": False, "protobuf:fPIC": True, "protobuf:lite": True}
    generators = "cmake"
    exports_sources = "generated_src/cpp/*"
    requires = "protobuf/3.9.1@_/_"

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="generated_src/cpp")
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="generated_src/cpp")
        self.copy("*messages.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["messages"]

